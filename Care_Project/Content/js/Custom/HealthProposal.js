﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    GetSelectedPlanDetails(GetEnquiryId());
});

GetEnquiryId = () => {
    let enquiryid = GetParameterValues('enquiryid');
    if (enquiryid != "" && enquiryid != undefined) {
        //if (enquiryid.length == 15) {
        //    return enquiryid;
        //}
        //else {
        //    window.location.href = "/health/healthrejistration";
        //}
        return enquiryid;
    }
    else {
        window.location.href = "/health/healthrejistration";
    }
}

GetSelectedPlanDetails = (enquiryid) => {
    $("#NextSelectedPlan").css("display", "none");
    $.ajax({
        type: "Post",
        url: "/Health/GetSelectedPlanDetails",
        data: '{enquiryid: ' + JSON.stringify(enquiryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null && response != "") {
                $("#divPlanSection").html("").html(response[0]);
                $("#divPlanAddon").html("").html(response[1]);
                $("#divProductDetails").html("").html(response[2]);
                $("#NextSelectedPlan").css("display", "block");
            }
        }
    });
}

GoToNextInsuredSection = () => {
    let enquiryid = GetEnquiryId();
    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if ($("#txtPMobile").hasClass("pmoberror")) { return false; }
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if ($("#txtPDOB").hasClass("errorpage")) { return false; }
    if (CheckFocusBlankValidation("txtPEmail")) return !1;
    if ($("#txtPEmail").hasClass("emailerror")) { return false; }
    if (CheckEmailValidatoin("txtPEmail")) return !1;
    if (CheckFocusBlankValidation("txtPAddress1")) return !1;
    if (CheckFocusBlankValidation("txtPAddress2")) return !1;
    if (CheckFocusBlankValidation("txtPPincode")) return !1;
    if (CheckFocusBlankValidation("txtPCity")) return !1;
    if (CheckFocusBlankValidation("txtPState")) return !1;

    let selectedamount = 0;
    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            selectedamount = $(this).data("premium");
        }
    });
    if (parseFloat(selectedamount) > 49000) {
        if (CheckFocusBlankValidation("txtPPan")) return !1;
    }
    if ($("#txtPPan").hasClass("panerror")) { return false; }
    if (CheckFocusDropDownBlankValidation("ddlAnualIncome")) return !1;
    let proposal = {};

    proposal.enquiryid = enquiryid;
    proposal.p_title = $("#ddlPTitle option:selected").val();
    proposal.p_gender = proposal.p_title == "Mr." ? "Male" : "FeMale";
    proposal.p_firstname = $("#txtPFirstName").val();
    proposal.p_lastname = $("#txtPLastName").val();
    proposal.p_mobile = $("#txtPMobile").val();
    proposal.p_dob = $("#txtPDOB").val();
    proposal.p_email = $("#txtPEmail").val();
    proposal.p_address1 = $("#txtPAddress1").val();
    proposal.p_address2 = $("#txtPAddress2").val();
    proposal.p_landmark = $("#txtPLandmark").val();
    proposal.p_pincode = $("#txtPPincode").val();
    proposal.p_cityname = $("#txtPCity").val();
    proposal.p_statename = $("#txtPState").val();
    proposal.p_pannumber = $("#txtPPan").val();
    proposal.p_aadharno = $("#txtPAadhar").val();
    proposal.p_annualincome = $("#ddlAnualIncome option:selected").val();
    proposal.p_isproposerinsured = ($("#chkIsProposerInsured").prop("checked") == true ? 1 : 0);

    $("#btnGoToNextInsured").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/InsertProposerDetails",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                $("#fdselectedplansection").css("display", "none");
                $("#fdproposerdetails").css("display", "none");
                $("#fdInsuredDetails").css("display", "block");
                $("#fdInsuredDetails").removeClass("hidden");
                $(".linext3").addClass("active");
                BindInsuredDetails(proposal.isproposerinsured);
            }
        }
    });
}

var totalperson = 1;
function BindInsuredDetails(isproposerinsured) {
    let proposal = {};
    proposal.enquiryid = GetEnquiryId();
    proposal.isproposerinsured = isproposerinsured;

    $.ajax({
        type: "Post",
        url: "/Health/BindInsuredDetails",
        async: false,
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#fdInsuredDetails").html(response[0]);
            totalperson = response[1];

            $('.DynamicCommanDateWithYr').datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                changeMonth: true,
                changeYear: true,
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true,
                //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
                //yearRange: '1961:' + (new Date).getFullYear()
                yearRange: '' + ((new Date).getFullYear() - 60) + ':' + (new Date).getFullYear()
            });

            $('.DynamicChildCommanDateWithYr').datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                changeMonth: true,
                changeYear: true,
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true,
                //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
                //yearRange: '1961:' + (new Date).getFullYear()
                yearRange: '' + ((new Date).getFullYear() - 24) + ':' + (new Date).getFullYear()
            });

            $("#btnGoToNextInsured").html("NEXT &gt;");

            $("#ddlRelation_1").attr("disabled", "disabled");
            $("#ddlTitle_1").attr("disabled", "disabled");
            $("#txtInFirstName_1").attr("disabled", "disabled");
            $("#txtInLastName_1").attr("disabled", "disabled");
            $("#txtInDOB_1").attr("disabled", "disabled");

            for (let i = 1; i <= totalperson; i++) {
                let relation = $("#ddlRelation_" + i + " option:selected").html().toLowerCase();
                if (relation != "relation") {
                    if (relation == 'son' || relation == 'daughter') {
                        CalculateNomineeAge('txtInDOB_' + i, 'notchilddobvalid' + i, 'position: absolute;top: 37px;right: 15px;font-size: 10px;');
                    }
                    else {
                        CalculateNomineeAge('txtInDOB_' + i, 'notndobvalid' + i, 'position: absolute;top: 37px;right: 15px;font-size: 10px;');
                    }
                }
            }
            CalculateNomineeAge('nDOB', 'ndobvalid', 'position: absolute;top: 37px;right: 15px;font-size: 10px;');
            //CalculateAgeWithoutVal($("#nDOB"), 'ndobvalid');
        }
    });
}

FillExistingIllness = (illnesspos) => {
    let existingillnes = $("#ddlIllness_" + illnesspos + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".ExistingIllness_" + illnesspos).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".ExistingIllness_" + illnesspos).addClass("notshow").css("display", "none");
        $("#txtExistingIllness_" + illnesspos).val("");
    }
}

FillEngageManualLabour = (mlabour) => {
    let existingillnes = $("#ddlEngageManualLabour_" + mlabour + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageManualLabour_" + mlabour).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageManualLabour_" + mlabour).addClass("notshow").css("display", "none");
        $("#txtEngageManualLabour_" + mlabour).val("");
    }
}

FillEngageWinterSports = (wsport) => {
    let existingillnes = $("#ddlEngageWinterSports_" + wsport + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageWinterSports_" + wsport).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageWinterSports_" + wsport).addClass("notshow").css("display", "none");
        $("#txtEngageWinterSports_" + wsport).val("");
    }
}

CalculateNomineeAge = (fid, errorclass, errorstyle) => {
    if (fid !== 'nDOB') {
        let index = fid.split('_')[1];
        if ($("#ddlRelation_" + index + " option:selected").val() == "") {
            $("#" + fid).addClass("errorpage");
            return $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i>Please select relation first. &nbsp;</span>");
        }
        let relation = $("#ddlRelation_" + index + " option:selected").html().toLowerCase();

        let startdate = $("#" + fid).val();
        if (startdate != "") {
            let splitstartdate = startdate.split('/');
            var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
            let age = Math.floor((new Date() - start) / 31557600000);


            //$("#" + fid).removeClass("errorpage");
            //$("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            if (relation == 'son' || relation == 'daughter') {
                let ageInDays = Math.floor((new Date() - start) / 86400000);
                if (ageInDays < 91 || age > 24) {
                    $("#" + fid).addClass("errorpage");
                    $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i>&nbsp;Age interval [91Day - 24Yr]</span>");
                } else {
                    $("#" + fid).removeClass("errorpage");
                    if (age < 1) {
                        $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + ageInDays + " Days</span>").css("color", "rgb(0, 163, 0)");
                    }
                    else {
                        $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");
                    }
                }
            }
            else {
                if (age >= 18) {
                    $("#" + fid).removeClass("errorpage");
                    $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");
                    //$("#btnNextProposerDetails").removeAttr('disabled');
                    //$("#btnNextProposerDetails").css('opacity', 1);
                }
                else {
                    $("#" + fid).addClass("errorpage");
                    $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");
                    //$("#btnNextProposerDetails").attr('disabled');
                    //$("#btnNextProposerDetails").css('opacity', 0.3);
                }
            }
        }
    }
    else {
        let startdate = $("#" + fid).val();
        if (startdate != "") {
            let splitstartdate = startdate.split('/');
            var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
            let age = Math.floor((new Date() - start) / 31557600000);

            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");
        }

    }

}

CalculateChildNomineeAge = (fid, errorclass, errorstyle) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age <= 25) {
            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must below 25 Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

CalculateAgeWithoutVal = (fid, errorclass, errorstyle) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        $("#" + fid).removeClass("errorpage");
        $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");
    }
}

function CheckAgeDifference() {
    $(".ndobvalid").html("");
    let relation = $("#ddlnRelation option:selected").html().toLowerCase().replaceAll(' ', '');
    let p_Age = 0, n_Age = 0;

    if (relation === 'father' || relation === 'mother' || relation === 'fatherinlaw' || relation === 'motherinlaw') {

        p_Age = ageCalc($("#txtInDOB_1").val());
        n_Age = ageCalc($("#nDOB").val());

        if ((p_Age - n_Age) > -18) {
            $(".ndobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Invalid dob for nominee&nbsp;</span>");
            return true;
        }
        else {
            $(".ndobvalid").html("");
            return false;
        }
    } else if (relation === 'son' || relation === 'daughter') {

        p_Age = ageCalc($("#txtInDOB_1").val());
        n_Age = ageCalc($("#nDOB").val());

        if ((p_Age - n_Age) < 18) {
            $(".ndobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Difference in age of proposer and nominee is less than 18 years&nbsp;</span>");
            return true;
        }
        else {
            $(".ndobvalid").html("");
            return false;
        }
    } else if (relation === 'grandfather' || relation === 'grandmother') {

        p_Age = ageCalc($("#txtInDOB_1").val());
        n_Age = ageCalc($("#nDOB").val());

        if ((p_Age - n_Age) > -36) {
            $(".ndobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Invalid dob for nominee&nbsp;</span>");
            return true;
        }
        else {
            $(".ndobvalid").html("");
            return false;
        }
    } else if (relation === 'grandson' || relation === 'granddaughter') {

        p_Age = ageCalc($("#txtInDOB_1").val());
        n_Age = ageCalc($("#nDOB").val());

        if ((p_Age - n_Age) < 36) {
            $(".ndobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Difference in age of proposer and nominee is less than 36 years</span>");
            return true;
        }
        else {
            $(".ndobvalid").html("");
            return false;
        }
    }
};

const ageCalc = (date) => {
    let splitstartdate = date.split('/');
    var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
    let age = Math.floor((new Date() - start) / 31557600000);
    return age;
}

function NextSelectedPlan() {
    $(".chkaddon").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            let addOnsId = $(this).data("chkaddonid");

            let addOns = $("#chkAddon_" + addOnsId).data("addons");
            let code = $("#chkAddon_" + addOnsId).data("code");
            let value = $("#chkAddon_" + addOnsId).data("value");
            let calculation = $("#chkAddon_" + addOnsId).data("calculation");
            let addOnValue = $("#chkAddon_" + addOnsId).data("addonvalue");
            let productDetailID = $("#chkAddon_" + addOnsId).data("productdetailid");
            let inquiryid = $("#chkAddon_" + addOnsId).data("inquiryid");
            let status = $("#chkAddon_" + addOnsId).prop("checked") == true ? 1 : 0;
            let period = $("#chkAddon_" + addOnsId).data("period");

            let basePremium = "";// $("#chkAddon_" + addOnsId).data("basepremium");
            let premium = "";// $("#chkAddon_" + addOnsId).data("premium");
            let discountPercent = "";// $("#chkAddon_" + addOnsId).data("discountpercent");
            let serviceTax = "";// $("#chkAddon_" + addOnsId).data("servicetax");
            let totalPremium = "";// $("#chkAddon_" + addOnsId).data("totalpremium");

            $(".chkpremiumamt").each(function (index, element) {
                if ($(this).prop("checked") == true) {
                    //proposal.periodfor = $(this).data("periodfor");
                    basePremium = premium = $(this).data("basepremium");
                    discountPercent = $(this).data("discount");
                    serviceTax = $(this).data("servicetax");
                    totalPremium = $(this).data("premium");
                }
            });

            addonlist = [addOnsId, addOns, code, value, calculation, addOnValue, productDetailID, inquiryid, status, basePremium, premium, discountPercent, serviceTax, totalPremium, period];
        }
    });
    $("#btnGoToNextProposal").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");

    $.ajax({
        type: "Post",
        url: "/Health/AddLessAddon",
        data: '{addonlist:' + JSON.stringify(addonlist) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            let proposal = {};
            proposal.enquiryid = GetParameterValues('enquiryid');
            $(".chkpremiumamt").each(function (index, element) {
                if ($(this).prop("checked") == true) {
                    proposal.period = $(this).data("periodfor");
                    proposal.selected_premium = $(this).data("premium");
                    //proposal.discount = $(this).data("discount");
                }
            });

            let addonlist = [];
            $(".chkAddon_Year" + proposal.period).each(function (index, element) {
                if ($(this).prop("checked") == true) {
                    let addon = {};
                    addon.inquiryid = $(this).data("inquiryid");
                    addon.addOnsId = $(this).data("addonsid");
                    addon.code = $(this).data("code");
                    addon.PlanAmount = $(this).data("totalpremium");
                    addon.PlanPeriod = $(this).data("period");
                    addonlist.push(addon);
                }
            });

            //$("#btnGoToNextProposal").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");

            $.ajax({
                type: "Post",
                url: "/Health/InsertSelectedPlanDetail",
                data: '{proposal:' + JSON.stringify(proposal) + ',addonlist:' + JSON.stringify(addonlist) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    setTimeout(function () {
                        GetAnnualIncomeDetails(response);
                    }, 200);
                }
            });
        }
    });
}

GetAnnualIncomeDetails = (response) => {
    if (response.enquiryid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetAnnualIncomeDetails",
            data: '{enquiryid:' + JSON.stringify(response.enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlAnualIncome").html(data);
                }
                $("#ddlPTitle").val(response.p_title);
                $("#txtPFirstName").val(response.p_firstname);
                $("#txtPLastName").val(response.p_lastname);
                $("#txtPMobile").val(response.p_mobile);
                $("#txtPEmail").val(response.p_email);
                $("#txtPPincode").val(response.p_pincode);
                $("#txtPDOB").val(response.p_dob);
                $("#txtPAddress1").val(response.p_address1);
                $("#txtPAddress2").val(response.p_address2);
                $("#txtPLandmark").val(response.p_landmark);
                //GetCityListFromStarHealth(response.pincode, response.cityid, response.areaid);
                CheckAndGetDetails(response.p_pincode);
                $("#txtPState").val(response.p_statename);
                $("#txtPCity").val(response.p_cityname);

                $("#txtPPan").val(response.p_pannumber);
                $("#txtPAadhar").val(response.p_aadharno);
                //$("#ddlAnualIncome option[value=" + response.annualincome + "]").attr("selected", "selected");  //response.annualincome
                $("#ddlAnualIncome").val(response.p_annualincome);
                //if (response.p_isproposerinsured == true) { $("#chkIsProposerInsured").prop("checked", true); } else { $("#chkIsProposerInsured").prop("checked", false); }
                $("#chkIsProposerInsured").prop("checked", true);
                //if (response.criticalillness) { $("#chkHavingCriticalIllness").prop("checked", true); } else { $("#chkHavingCriticalIllness").prop("checked", false); }
                //if (response.ispropnominee) { $("#chkIsPropNominee").prop("checked", true); } else { $("#chkIsPropNominee").prop("checked", false); }

                $(".section1").addClass("hidden");
                $(".section2").removeClass("hidden");
                $(".linext2").addClass("active");
                $("#btnGoToNextProposal").html("Next");

                CalculateAge('txtPDOB');
            }
        });
    }
}

CheckAndGetDetails = (pincode) => {
    $(".notpincodevalid").addClass("hidden").html("");

    if (pincode.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/CheckPincode",
            data: '{pincode: ' + JSON.stringify(pincode) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 0) {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> " + response);
                    $("#hdnState").val(response[0]);
                    $("#txtPPincode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("Wrong Pincode");
                    $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

CalculateAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notpdobvalid").html("<span style='position: absolute;top: 14px;right: 20px;font-size: 10px;'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notpdobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 14px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

function GoToMedicalHisSection() {

    for (var i = 1; i <= parseInt(totalperson); i++) {
        if (CheckFocusBlankValidation("ddlRelation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlTitle_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInFirstName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInLastName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInDOB_" + i)) return !1;
        if ($("#txtInDOB_" + i).hasClass("errorpage")) { return false; }

        if (CheckFocusBlankValidation("txtInHeight_" + i)) return !1;
        if ($("#txtInHeight_" + i).val() == "0") {
            $("#txtInHeight_" + i).css("border", "1px solid red");
            $("#txtInHeight_" + i).val("");
            return !1;
        }

        if (!isNaN($("#txtInHeight_" + i).val())) {
            $("#txtInHeight_" + i).css("border", "1px solid #ced4da");
        } else {
            $("#txtInHeight_" + i).css("border", "1px solid red");
            return !1;
        }

        if (CheckFocusBlankValidation("txtInWeight_" + i)) return !1;
        if ($("#txtInWeight_" + i).val() == "0") {
            $("#txtInWeight_" + i).css("border", "1px solid red");
            $("#txtInWeight_" + i).val("");
            return !1;
        }
        if (!isNaN($("#txtInWeight_" + i).val())) {
            $("#txtInWeight_" + i).css("border", "1px solid #ced4da");
        } else {
            $("#txtInWeight_" + i).css("border", "1px solid red");
            return !1;
        }

        if (CheckFocusBlankValidation("ddlOccupation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlIllness_" + i)) return !1;

        let existingillnes = $("#ddlIllness_" + i + " option:selected").val();
        if (existingillnes == "true") { if (CheckFocusBlankValidation("txtExistingIllness_" + i)) return !1; }

        let ismlabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        if (ismlabour == "true") { if (CheckFocusBlankValidation("txtEngageManualLabour_" + i)) return !1; }

        let iswsport = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        if (iswsport == "true") { if (CheckFocusBlankValidation("txtEngageWinterSports_" + i)) return !1; }

        if (CheckFocusBlankValidation("ddlEngageManualLabour_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlEngageWinterSports_" + i)) return !1;
    }

    if (CheckFocusDropDownBlankValidation("ddlnRelation")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlnTitle")) return !1;
    if (CheckFocusBlankValidation("nFirstName")) return !1;
    if (CheckFocusBlankValidation("nLastName")) return !1;
    if (CheckFocusBlankValidation("nDOB")) return !1;
    if (CheckAgeDifference()) return !1;
    if ($("#nDOB").hasClass("errorpage")) { return false; }

    let proposal = {};
    proposal.enquiryid = GetEnquiryId();

    var insuredlist = [];

    for (var i = 1; i <= parseInt(totalperson); i++) {
        let insured = {};

        insured.id = $("#hdnInsuredId_" + i).val();
        insured.enquiryid = proposal.enquiryid;
        insured.relationid = $("#ddlRelation_" + i + " option:selected").val();
        insured.relationname = $("#ddlRelation_" + i + " option:selected").text();
        insured.title = $("#ddlTitle_" + i + " option:selected").val();
        insured.firstname = $("#txtInFirstName_" + i).val();
        insured.lastname = $("#txtInLastName_" + i).val();
        insured.dob = $("#txtInDOB_" + i).val();
        insured.height = $("#txtInHeight_" + i).val();
        insured.weight = $("#txtInWeight_" + i).val();

        insured.occupationid = $("#ddlOccupation_" + i + " option:selected").val();
        insured.occupation = $("#ddlOccupation_" + i + " option:selected").text();
        insured.illness = $("#ddlIllness_" + i + " option:selected").val();
        insured.illnessdesc = $("#txtExistingIllness_" + i).val();

        insured.engageManualLabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        insured.engageManualLabourDesc = $("#txtEngageManualLabour_" + i).val();

        insured.engageWinterSports = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        insured.engageWinterSportDesc = $("#txtEngageWinterSports_" + i).val();

        //insured.isinsured = (i == 1 ? true : false);
        let ispropinsured = ($("#chkIsProposerInsured").prop("checked") == true ? 1 : 0);
        if (i == 1 && ispropinsured == 1) { insured.ispropinsured = 1; } else { insured.ispropinsured = 0; }
        insured.insuredcount = i;

        insuredlist.push(insured);
    }

    proposal.InsuredList = insuredlist;
    proposal.n_relation = $("#ddlnRelation option:selected").val();
    proposal.n_relationid = $("#ddlnRelation option:selected").val();
    proposal.n_title = $("#ddlnTitle option:selected").val();
    proposal.n_firstname = $("#nFirstName").val();
    proposal.n_lastname = $("#nLastName").val();
    proposal.n_dob = $("#nDOB").val();

    let nBdate = proposal.n_dob.split('/');
    nBdate = new Date(nBdate[2] + "-" + nBdate[1] + "-" + nBdate[0])

    proposal.n_age = Math.floor((new Date() - nBdate) / 31557600000);


    $("#btnInsuredNext").html("processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/InsertInsured_NomineeDetail",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#divMedicalHistoryDetail").html(response);
            }
            else {
                $("#divMedicalHistoryDetail").html("<span class='text-danger'>No medical history found!</span>");
            }
            $("#fdselectedplansection").css("display", "none");
            $("#fdproposerdetails").css("display", "none");
            $("#fdInsuredDetails").css("display", "none");
            $("#fdMedicalHistory").css("display", "block");
            $("#fdMedicalHistory").removeClass("hidden");
            $(".linext4").addClass("active");
            $("#btnInsuredNext").html("NEXT");
        }
    });
}

RemoveHtmlTags = (strhtml) => {
    if (strhtml != "" && strhtml != undefined) {
        var strsyntax = /<(\w+)[^>]*>.*<\/\1>/gi;
        var somtag = /\r?\n|\r/g;
        strhtml = strhtml.toString().replace(strsyntax, "").replace(somtag, "");
    }
    return strhtml;
}
CreateProposalSubmit = () => {
    let QuestionCount = $("#hdnQuestionCount").val();
    let QuestionList = [];
    if (QuestionCount != "0" && QuestionCount != undefined) {
        let tempinsuredcount = "0";
        let insuredname = [];
        $(".mainquestionar").each(function () {
            let questionstr = ""; let startquestionstr = true;

            let insuredcount = $(this).data("insuredcount");
            let quescode = RemoveHtmlTags($(this).data("quescode"));
            let repotype = $(this).data("repotype");
            let isrequired = $(this).data("isrequired");
            let quessetcode = $(this).data("quessetcode");
            let questiondesc = $("#divMainQuestion_" + insuredcount + "_" + quescode).html();
            let subquestionexist = $("#divMainQuestion_" + insuredcount + "_" + quescode).data("subquestionexist");

            //if (tempinsuredcount == insuredcount) {
            //    if (startquestionstr) {
            //        questionstr = "[";
            //        startquestionstr = false;
            //    }
            //}           

            questionstr = "[";

            if (repotype.toLowerCase() == "boolean") {
                if ($(this).prop("checked") == true) {
                    questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"true\",";
                }
                else {
                    questionstr = questionstr + "{\"questionSetCode\":\"" + quessetcode + "\",\"questionCode\":\"" + quescode + "\",\"questionDescription\":\"" + questiondesc + "\",\"response\":\"false\",";
                }

                //questionstr = questionstr + MakeSubQuestion(qscode, repotype, subexist, subloopcount);
            }
            questionstr = questionstr + "\"subQuestion\":[" + MakeSubQuestion2(quessetcode, quescode, insuredcount) + "]},";

            if (tempinsuredcount != insuredcount) {
                insuredname = $("#hdnInsuredName_" + insuredcount).val().split(',');
                let question = {};
                question.enquiryid = GetParameterValues('enquiryid');
                question.firstname = insuredname[0];
                question.lastnane = insuredname[1];

                questionstr = questionstr.slice(0, -1) + "]";
                //console.log(questionstr);
                //questionstrList.push(questionstr);
                question.questionjson = questionstr;
                QuestionList.push(question);

                tempinsuredcount = insuredcount;
                questionstr = "";
                questionstr = "[";
            }
            else {
                insuredname = $("#hdnInsuredName_" + insuredcount).val().split(',');
                let question = {};
                question.enquiryid = GetParameterValues('enquiryid');
                question.firstname = insuredname[0];
                question.lastnane = insuredname[1];

                questionstr = questionstr.slice(0, -1) + "]";
                //console.log(questionstr);
                //questionstrList.push(questionstr);
                question.questionjson = questionstr;
                QuestionList.push(question);
            }
        });

        //let question = {};
        //question.enquiryid = GetParameterValues('enquiryid');
        //question.firstname = insuredname[0];
        //question.lastnane = insuredname[1];

        //question.questionjson = questionstr.slice(0, -1) + "]";
        //QuestionList.push(question);

        //questionstrList.push(questionstr.slice(0, -1) + "]");

        //questionstr = questionstr.slice(0, -1) + "]";
        //console.log(questionstr);
        //questionstrList.push(questionstr);
    }

    //$("#btnGenrateProposal").html("<div id='model_success' style='display:none;text-align: center;'><img src='~/Content/images/care.jpg' /><p class='text-center' style='font-size: 20px!important;padding: 5%;color:#38c300;'>Please wait, we are preparing your proposal... <i class='fa fa-spinner fa-pulse'></i></p> </div>");
    $("#PMButton").click();
    $.ajax({
        type: "Post",
        url: "/Health/CreateHealthProposal",
        data: '{enquiryid:' + JSON.stringify(GetEnquiryId()) + ',medhistory:' + JSON.stringify(QuestionList) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                if (response.status.toLowerCase() === 'true') {
                    $("#modEnqId").html(response.EnquiryId.toUpperCase());
                    $("#modPremium").html(response.Premium);
                    $("#modProposalNo").html(response.ProposalNo);

                    $("#model_wait").css("display", "none");
                    $("#model_success").css("display", "block");
                    $("#model_summaryBtn").attr("href", "/health/healthproposalsummary?enquiryid=" + GetEnquiryId());
                }
                else if (response.status.toLowerCase() === 'false' || response.status.toLowerCase() === undefined || response.status.toLowerCase() === '') {
                    $("#model_wait").css("display", "none");
                    $("#model_success").css("display", "none");
                    $("#model_fail").css("display", "block");
                }
                else if (response.status.toLowerCase() === 'error') {
                    $("#model_wait").css("display", "none");
                    $("#model_success").css("display", "none");
                    $("#model_fail").html(response.message).css("display", "block").css("word-break", "break-all");
                    $("#model_fail1").css("display", "block");
                }
                //$("#divPerposalModal").html(response);
                $("#btnGenrateProposal").html("Submit");
                //$("#PMButton").click();
            }
            else {
                $("#divPerposalModal").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-exclamation-triangle' aria-hidden='true'></i>&nbsp;Error Occurred</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><p class='text-danger' style='word-break: break-word;'>There is some error occurred, Please try again later.</p></div>");
                $("#btnPerposalLastSubmit").html("Submit");
                //$("#PMButton").click();
            }
        }
    });
}

function MakeSubQuestion2(quessetcode, quescode, insuredcount) {
    let subquestionstr = "";
    $(".subquestionfield_" + insuredcount + "_" + quescode).each(function () {
        let subquestioncode = $(this).data("subquestioncode");
        let repotype = $(this).data("repotype").toLowerCase();
        let sub2exist = $(this).data("sub2exist");
        let isrequired = $(this).data("isrequired");
        let subdesc = $(this).data("subdesc");
        let subquestionsetcode = $(this).data("subquestionsetcode");

        if (repotype == "boolean") {
            if ($(this).prop("checked") == true) {
                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"true\",\"subQuestion\":[]},";
            }
            else {
                subquestionstr = subquestionstr + "{\"questionSetCode\":\"" + subquestionsetcode + "\",\"questionCode\":\"" + subquestioncode + "\",\"questionDescription\":\"" + subdesc + "\",\"response\":\"false\",\"subQuestion\":[]},";
            }
        }
    });

    return subquestionstr.slice(0, -1);
}

CheckEmailAddress = () => {
    $(".notp_email").html("").css("display", "none");
    $("#txtPEmail").removeClass("emailerror");

    let panVal = $("#txtPEmail").val();
    if (panVal != "" && panVal != undefined) {
        var regpan = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (regpan.test(panVal)) { return true; }
        else {
            $("#txtPEmail").addClass("emailerror");
            $(".notp_email").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Email address is not yet valid.</span>").css("display", "block");
            return false;
        }
    }
}

CheckPanNumber = () => {
    $(".notp_pan").html("").css("display", "none");
    $("#txtPPan").removeClass("panerror");

    let panVal = $("#txtPPan").val().toUpperCase();
    if (panVal != "" && panVal != undefined && panVal.length == 10) {
        //var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/;
        //var regpan = /[a-zA-Z]{3}[PCHFATBLJG]{1}[a-zA-Z]{1}[0-9]{4}[a-zA-Z]{1}$/;
        //var regpan = /([A-Z]){5}([0-9]){4}([A-Z]){1}$/;
        var regpan = /[A-Z]{3}[PCHFATBLJG]{1}[A-Z]{1}[0-9]{4}[A-Z]{1}$/;
        if (regpan.test(panVal)) {
            $(".notp_pan").html("<span class='text-success pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-check-circle'></i> Permanent Account Number is valid.</span>").css("display", "block");
            return true;
        }
        else {
            $("#txtPPan").addClass("panerror");
            $(".notp_pan").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Permanent Account Number is not yet valid.</span>").css("display", "block");
            return false;
        }
    }
    else if (panVal.length > 0 && panVal.length < 10) {
        $("#txtPPan").addClass("panerror");
        $(".notp_pan").html("<span class='text-danger pull-right' style='position: absolute;top: 40px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Permanent Account Number is not yet valid.</span>").css("display", "block");
        return false;
    }
}

CheckMobileNumber = (mid, errorclas) => {
    let mobno = $("#" + mid).val();
    $("#" + mid).removeClass("pmoberror");
    if (mobno.length == 10) {
        let regx = /^[6-9]{1}[0-9]{9}$/;
        var valid = regx.test(mobno);
        if (valid) {
            $("." + errorclas).html("<span style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-check-circle'></i> Valid</span>").css("color", "rgb(0, 163, 0)");
        }
        else {
            $("#" + mid).addClass("pmoberror");
            $("." + errorclas).html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Mobile number not valid.</span>");
        }
    }
    else {
        $("#" + mid).addClass("pmoberror");
        $("." + errorclas).html("<span class='text-danger pull-right' style='position: absolute;top: 10px;right: 15px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Mobile number not valid.</span>");
    }
}

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

function Backform2Section1() {
    $(".section2").addClass("hidden");
    $(".linext2").removeClass("active");
    $(".section1").removeClass("hidden");
    $(".section1").css("display", "block");
}

function Backform2Section2() {
    $(".section3").addClass("hidden");
    $(".linext3").removeClass("active");
    $(".section2").removeClass("hidden");
    $(".section2").css("display", "block");
}

function Backform2Section3() {
    $(".section4").addClass("hidden");
    $(".linext4").removeClass("active");
    $(".section3").removeClass("hidden");
    $(".section3").css("display", "block");
}

CheckWeight = (weightid) => {
    $(".notweightvalid" + weightid).html("");
    let weightVal = $("#txtInWeight_" + weightid).val();
    if (weightVal.length == 1 && (weightVal == "0" || weightVal == "00" || weightVal == "000")) {
        $(".notweightvalid" + weightid).html("<span class='text-danger pull-right' style='position: absolute;top: 37px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Invalid weight!</span>");
        $("#txtInWeight_" + weightid).val("");
        return false;
    }
    return true;
}

CheckHeight = (heighttid) => {
    $(".notheightvalid" + heighttid).html("");
    let heightVal = $("#txtInHeight_" + heighttid).val();
    if (heightVal.length == 1 && (heightVal == "0" || heightVal == "00" || heightVal == "000")) {
        $(".notheightvalid" + heighttid).html("<span class='text-danger pull-right' style='position: absolute;top: 37px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Invalid height!</span>");
        $("#txtInHeight_" + heighttid).val("");
        return false;
    }
    return true;
}

AddLessAddon = (addOnsId) => {
    $(".chkaddon").each(function (index, element) { $(this).prop("checked", false); });
    $("#chkAddon_" + addOnsId).prop("checked", true);
}