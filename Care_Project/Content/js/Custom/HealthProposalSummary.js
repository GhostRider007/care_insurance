﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    BindHealthProposalSummary(GetEnquiryId());
    $("#enqId_label").html("Enquiry Id : " + GetEnquiryId().toUpperCase());
    $("#proposalNo_label").html("Proposal No. : " + $("#propNoValue").val().toUpperCase())
});

GetEnquiryId = () => {
    let enquiryid = GetParameterValues('enquiryid');
    if (enquiryid != "" && enquiryid != undefined) {
        //if (enquiryid.length == 15) {
        //    return enquiryid;
        //}
        //else {
        //    window.location.href = "/health/healthrejistration";
        //}
        return enquiryid;
    }
    else {
        window.location.href = "/health/healthrejistration";
    }
}

BindHealthProposalSummary = (enquiryid) => {
    $.ajax({
        type: "Post",
        url: "/Health/BindProposalSummaryDetails",
        async: false,
        data: '{enquiryid: ' + JSON.stringify(enquiryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null && response != "") {
                $("#divProductSummPremiumBreakup").html("").html(response[0]);
                $("#divProposerDetail").html("").html(response[1]);
                $("#divInsured").html("").html(response[2]);
                $("#divMedical").html("").html(response[3]);
                $("#divNominee").html("").html(response[4]);
                $("#divAddon").html("").html(response[5]);
            }
        }
    });
}

ChangeInsuredMedicalData = (index, count) => {
    for (let i = 1; i <= count; i++) {
        $("#insuredMedicalPerson_" + i).removeClass("ui-tabs-active ui-state-active");
        $("#insuredMedicalData_" + i).addClass("hidden");
    }
    $("#insuredMedicalPerson_" + index).addClass("ui-tabs-active ui-state-active");
    $("#insuredMedicalData_" + index).removeClass("hidden");
}

RedirectPaymentUrl = () => {
    let enquiryid = GetEnquiryId();
    if (enquiryid != "" && enquiryid != undefined) {
        //$(".fapayment").removeClass("fa-credit-card").addClass("fa-pulse fa-spinner");
        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');

        $.ajax({
            type: "Post",
            url: "/Health/GenratePaymentUrl",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response == "paid") {
                    setTimeout(function () {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-success'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-success'>This payment has already been paid.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }, 1000);
                }
                else if (response != "" && response != "paid" && response != undefined) {
                    setTimeout(function () {
                        window.open(response);
                        $(".close").click();
                        //window.location.href = response;
                    }, 1500);
                }
                else {
                    alert("Unable to generate payment url, please contect to administrator.");
                }
                //$(".fapayment").addClass("fa-credit-card").removeClass("fa-pulse fa-spinner");
            }
        });
    }
}

CheckPaymentStatusDetails = () => {
    let enquiryid = GetEnquiryId();
    if (enquiryid != "" && enquiryid != undefined) {
        $(".fapaymentstatus").removeClass("fa-rupee").addClass("fa-pulse fa-spinner");

        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');
        $.ajax({
            type: "Post",
            url: "/Health/CheckHealthPaymentDetails",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                setTimeout(function () {
                    if (response == "notpay") {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You haven't payment yet, Please payment first.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                    else if (response != "failed" && response != "notpay") {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-success'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Payment Success</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'>" + response + "</div>");
                        //$("#PStatusModal").modal('show');
                    }
                    else {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                }, 1000);
                $(".fapaymentstatus").addClass("fa fa-hand-o-right").removeClass("fa-pulse fa-spinner");
            }
        });
    }
}

GetHealthPdfDocument = () => {
    let enquiryid = GetEnquiryId();
    if (enquiryid != "" && enquiryid != undefined) {
        $(".fadownloadpdf").removeClass("fa-download").addClass("fa-pulse fa-spinner");
        //&times;
        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title' style='color: #979494'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'></span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 style='color: #979494'>Please wait...<span class='fa fa-pulse fa-spinner'></span><br/> We are featching details.</h6></div>");
        $("#PStatusModal").modal('show');

        $.ajax({
            type: "Post",
            url: "/Health/GetHealthPdfDocument",
            data: '{enquiryid:' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $(".fadownloadpdf").addClass("fa-download").removeClass("fa-pulse fa-spinner");
                setTimeout(function () {
                    if (data != null) {
                        if (data == "notpay") {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>You haven't payment yet, Please payment first.</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                        else if (data == "failed") {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Failed to generate Policy pdf, Please contact to administrator</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                        else if (data != "" && data != "notpay" && data != "failed") {
                            $(".close").click();

                            var a = $("<a/>");
                            a.attr("download", (enquiryid + ".pdf"));
                            a.attr("href", data);
                            $("body").append(a);
                            a[0].click();
                            $("body").remove(a);
                        }
                        else {
                            $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                            //$("#PStatusModal").modal('show');
                        }
                    }
                    else {
                        $("#divPerposalModal").html("").html("<div class='modal-header'><h6 class='modal-title text-danger'><i class='fa fa-info-circle' aria-hidden='true'></i>&nbsp;Information</h6><button type='button' class='close text-danger' data-dismiss='modal' aria-label='Close' ><span aria-hidden='true'>&times;</span></button></div><div class='modal-body text-center' style='padding: 40px;'><h6 class='text-danger'>Your policy is being prepared. Kindly try after few minutes.</h6></div>");
                        //$("#PStatusModal").modal('show');
                    }
                }, 1000);
            }
        });
    }
}

$('#tabs, #fragment-1').tabs({
    select: function (event, ui) {
        var tabNumber = ui.index;
        var tabName = $(ui.tab).text();

        console.log('Tab number ' + tabNumber + ' - ' + tabName + ' - clicked');
    }
});