﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}
$(function () {
    FetchHealthListing(GetParameterValues('enquiryid'));
    healthList_Count = $("#divHealth_SearchList").children().length;
    $("#lblProductCount").html(healthList_Count + ' Record(s)');
});
//------------------------------------------------------------------------------------
//Variables
let healthList_Count = 0;

//------------------------------------------------------------------------------------
FetchHealthListing = (enqid) => {
    if (enqid !== "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetHealthSearchList",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response.length > 1) {
                    $("#divHealth_SearchList").html("").html(response[0]);
                    $("#divPolicyType").html("").html(response[6]);
                    $("#hdnMinRange").val(response[1]);
                    $("#hdnMaxRange").val(response[2]);
                    BindPriceRangeSection(parseInt(response[1]), parseInt(response[2]));
                    BindTenureRangeSection("ddlTenure", response[3]);
                    //BindSumInsured("ddlSumInsured", response[4]);
                    $("#ddlSumInsured").html("").html(response[4]);
                    $("#divFilterSumInsured").removeClass("hidden");
                    $("#divResetSection").css("display", "block");
                    $("#divStarterSection").html("");
                }
                else {
                    $("#divStarterSection").html("");
                    $("#divPolicyType").html("").html(response[0]);
                    $("#divHealth_SearchList").html("<div class='col-sm-12 text-center text-danger'><p style='font-size: 25px;margin: 60px;'>Sorry, we don't have plans based on the information you have provided. </p><a class='btn btn-primary' href='/health/healthrejistration' style='background: #09a9e5;color: #fff;border-color: #09a9e5;'>Search Again</a></div>");
                }
            }
        });
    }
}

function BindSumInsured(dropdownid, suminsured) {
    $("#" + dropdownid).html("");
    var splitsuminsured = suminsured.split(',');
    $("#" + dropdownid).append("<option value=''>Select</option>");
    for (var i = 0; i < splitsuminsured.length; i++) {
        $("#" + dropdownid).append("<option value='" + splitsuminsured[i] + "'>₹ " + splitsuminsured[i] + " </option>");
    }
    $("#divFilterSumInsured").removeClass("hidden");
}

function BindTenureRangeSection(dropdownid, tenure) {
    if (tenure != null && tenure != "") {
        var tenureSplt = tenure.split('-');
        for (var i = 1; i <= parseInt(tenureSplt[1]); i++) {
            $("#" + dropdownid).append("<option value=" + i + ">" + i + " Year</option>");
        }
        $("#divFilterTenure").removeClass("hidden");
    }
}

function BindPriceRangeSection(minval, maxval) {
    $("#minRangeSlide").html("₹ " + minval);
    $("#maxRangeSlide").html("₹ " + maxval);

    var minRangeSlide = $("#minRangeSlide");
    var maxRangeSlide = $("#maxRangeSlide");

    $("#sliderprice").slider({
        range: true,
        orientation: "horizontal",
        min: minval,
        max: maxval,
        values: [minval, maxval],
        step: 100,
        slide: function (event, ui) {
            if (ui.values[0] == ui.values[1]) {
                return false;
            }
            minRangeSlide.html("").html('₹ ' + $("#sliderprice").slider("values", 0));
            maxRangeSlide.html("").html('₹ ' + $("#sliderprice").slider("values", 1));
        },
        change: function (event, ui) {
            PriceFilter(ui.values[0], ui.values[1]);
        }
    });

    $("#divFilterPremium").removeClass("hidden");
}

$(document.body).on('click', ".btnselectedplan", function (e) {
    let productid = $(this).data("productid");
    let suminsured = $(this).data("suminsured");
    let suminsuredid = $(this).data("suminsuredid");
    let companyid = $(this).data("companyid");
    let adult = $(this).data("adult");
    let child = $(this).data("child");
    let planname = $(this).data("planname");
    let totalamt = $(this).data("totalamt");
    let thiscount = $(this).data("thiscount");
    let tenure = $(this).data("tenure");
    let inramount = $(this).data("inramount");
    if (thiscount != "") {
        $(".btnselectedplan").each(function () { $(this).attr("disabled", true); });
        $("#btnPremiumPlan" + thiscount).html("Wait... <i class='fa fa-spinner fa-pulse'></i>").css({ "background": "#262566" });
        let urlpara = "enquiry_id=" + GetParameterValues('enquiryid') + "&product=" + productid + "&companyid=" + companyid + "&suminsuredid=" + suminsuredid + "&suminsured=" + suminsured + "&adult=" + adult + "&child=" + child + "&planname=" + planname + "&totalamt=" + totalamt + "&tenure=" + tenure;
        $.ajax({
            type: "Post",
            url: "/Health/InsertSearchUrlParaDetail",
            data: '{enquiryid: ' + JSON.stringify(GetParameterValues('enquiryid')) + ',urlpara:' + JSON.stringify(urlpara) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response == true) {
                    window.location.href = "/health/healthproposal?enquiryid=" + GetParameterValues('enquiryid');
                }
                else {
                    $("#btnPremiumPlan" + thiscount).html(inramount).css({ "background": "#09a9e5" });
                    $(".btnselectedplan").each(function () { $(this).attr("disabled", false); });
                }
            }
        });
    }
});

var model = {}
model.EnquiryId = GetParameterValues('enquiryid');
function PriceFilter(min, max) {
    model.MinPrice = min;
    model.MaxPrice = max;
    //model.Type = "price";
    FinalFilterAllCategory(model);
}

$(document.body).on('change', "#ddlTenure", function (e) {
    model.Tenure = $(this).val();
    FinalFilterAllCategory(model);
});

$(document.body).on('change', "#ddlSumInsured", function (e) {
    model.SumInsured = $(this).val();
    FinalFilterAllCategory(model);
});

function FinalFilterAllCategory(paramodel) {
    $.ajax({
        type: "Post",
        url: "/Health/FilterSection",
        data: '{filter: ' + JSON.stringify(paramodel) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#divHealth_SearchList").html("").html(response[0]);
                $("#lblProductCount").html(response[5] + " Records");
            }
            else {
                $("#divHealth_SearchList").html("").html("<h2 style='text-align: center;margin-top: 10%;'>No matching results found !</h2>");
                $("#lblProductCount").html("0 Records");
            }
            $(window).scrollTop(0);
        }
    });
}

var checked_HIP = 0;
var compDetailCount = [];

function CompareCheck(element, dataCount) {

    if ($(element).prop("checked") === true) {
        checked_HIP += 1;
        if (checked_HIP > 3) {
            checked_HIP = 3;
            $(element).prop("checked", false);
            return alert("You cannot select more than 3 products.\nPlease clear all or deselect one of them");
        }
        if (checked_HIP > 1) {
            $("#compare_div").removeClass("hidden");
        }

        compDetailCount.push(dataCount);
    }
    else {
        checked_HIP -= 1;
        if (checked_HIP === 0) {
            $("#compare_div").addClass("hidden");
        }
        compDetailCount = compDetailCount.filter(item => item !== dataCount)
    }
    $("#compCount").html(checked_HIP);
}

let clearCompSelection = () => {
    $(".healthInsuranceProducts").each(function () {
        $(this).prop("checked", false);
    });
    checked_HIP = 0;
    $("#compCount").html(checked_HIP);
    $("#compare_div").addClass("hidden");
    compDetailCount = [];
}

$("#btn_compareProducts").click(function () {

    if (checked_HIP === 1) {
        alert("Please select atleast two products.");
        return false;
    }
    else {
        $("#comp-modalContent").html("<p class='text-center' style='font-size: 25px;margin: 60px;'>We are comparing products based on the information you have selected. <i class='fa fa-spinner fa-pulse'></i></p>");
        let compDetailArray = [];
        for (let i = 0; i < compDetailCount.length; i++) {
            var compDetail = {};
            compDetail.sumInsured = $("#compData" + compDetailCount[i]).data("sum_insured");
            compDetail.plan = ($("#compData" + compDetailCount[i]).data("plan") == undefined || $("#compData" + compDetailCount[i]).data("plan") == "") ? "" : $("#compData" + compDetailCount[i]).data("plan");
            compDetail.companyId = $("#compData" + compDetailCount[i]).data("companyid");
            compDetail.productid = $("#compData" + compDetailCount[i]).data("productid");
            compDetail.cover = $("#coverCount" + compDetailCount[i]).html();
            compDetail.premium = $("#premiumCount" + compDetailCount[i]).html();
            compDetailArray.push(compDetail);
        }

        $.ajax({
            type: "Post",
            url: "/Health/CompareInsuranceProducts",
            data: '{compreqarray: ' + JSON.stringify(compDetailArray) + ',enqid: ' + JSON.stringify(GetParameterValues("enquiryid")) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                $("#comp-modalContent").html(data);
            }
        });

    }



});

$("#resetFilter").click(function () {
    $("#ddlSumInsured").val('');
    $("#ddlTenure").val('');
    model.SumInsured = "";
    model.Tenure = "";
    BindPriceRangeSection(parseInt($("#hdnMinRange").val()), parseInt($("#hdnMaxRange").val()));

    //FinalFilterAllCategory(model);
});
