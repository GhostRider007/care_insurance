﻿$(function () {
    var enquiry_id = GetParameterValues('enquiryid');
    if (enquiry_id == null || enquiry_id == '') { GenrateEnquiry_Id(); }
    BindSumInsured("ddlSumInsured");
});

function GenrateEnquiry_Id() {
    $.ajax({
        type: "Post",
        url: "/Health/GenrateEnquiryId",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response !== "") {
                window.location.href = response;
            }
        }
    });
}

function BindSumInsured(dropdownid) {
    $("#" + dropdownid).append("<option value=''>Please Wait...</option>");
    $.ajax({
        type: "Post",
        url: "/Health/GetSumInsuredPriceList",
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#" + dropdownid).html("");
                $("#" + dropdownid).append("<option value=''>Select</option>");
                $.each(response, function (i, item) { $("#" + dropdownid).append("<option value='" + item.SumInsuredValue + "' data-minprice='" + item.MinSumInsured + "' data-maxprice='" + item.MaxSumInsured + "'> " + item.SumInsuredText + "</option>"); });
            }
        }
    });
}

function CheckPincode() {
    $("#notpincodevalid").addClass("hidden").html("");
    var txtPinCodeVal = $("#txtPinCode").val();

    if (txtPinCodeVal.length != 6) {
        $("#notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $("#notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/CheckPincode",
            data: '{pincode: ' + JSON.stringify(txtPinCodeVal) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response != "") {
                    $("#notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> " + response);
                    $("#txtPinCode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $("#notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("<i class='fa fa-times-circle'></i> Wrong Pincode");
                    $("#txtPinCode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

ResetMembers = () => {
    $(".chkmember").each(function () {
        let objmember = $(this).data("member");
        if ($(".chkbox_" + objmember).prop("checked") == true) {
            $(".chkbox_" + objmember).click();
        }
    });
}

function OnOffMember(membertype) {
    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") {
        $(".chkmember").each(function () {
            let objmember = $(this).data("member");

            if (objmember == membertype) {
                if ($(".chkbox_" + membertype).prop("checked") == true) {
                    $(".chkbox_" + membertype).click();
                }
            }
            else {
                if ($(".chkbox_" + objmember).prop("checked") == true) {
                    $(".chkbox_" + objmember).click();
                }
            }
        });
    }
    else {
        if ($(".FamilySection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "father" || objmember == "mother") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
        if ($("#ParentsSection input:checkbox:checked").length > 0) {
            $(".chkmember").each(function () {
                let objmember = $(this).data("member");
                if (objmember == "self" || objmember == "spouse" || objmember == "son" || objmember == "daughter") {
                    if ($(".chkbox_" + objmember).prop("checked") == true) {
                        $(".chkbox_" + objmember).click();
                    }
                }
            });
        }
    }
}

function increment(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count < 2) {
        $(obj).parent().parent().find('input').val(count + 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count + 1, ("#" + appdiv), name);
    }
}
function decrement(obj) {
    var count = parseInt($(obj).parent().parent().find('input').val());
    if (count != 1) {
        $(obj).parent().parent().find('input').val(count - 1);
        var appdiv = $(obj).data("appdiv");
        var name = $(obj).data("name");
        SonDautAgeSectionPlusMinus("plus", count - 1, ("#" + appdiv), name);
    }
}

$("#chkSon").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divSonAgeSection"), "Son"); });
$("#chkDaughter").click(function () { SonDautAgeSectionPlusMinus("plus", 1, ("#divDaughterAgeSection"), "Daughter"); });

function SonDautAgeSectionPlusMinus(type, count, appentdiv, name) {
    if (type == "plus") {
        $(appentdiv).html("");
        var i; var bindagedel = "";
        for (i = 1; i <= parseInt(count); i++) {
            bindagedel += "<div class='col-sm-12'><div class='row'>"
                + "<div class='col-sm-6'> <label class='pull-right' style='color: #e71820;'>" + name + "</label></div>"
                + "<div class='col-sm-6 form-validation'><h6>"
                + "<span class='ageleft'>Age</span>"
                + "<input type='text' class='form-control agetextbox' id='txt" + name + i + "Age' name='txt" + name + i + "Age' maxlength='2' onkeypress='return isNumberOnlyKeyNoDot(event)' onchange='return CalculateChildAge(\"txt" + name + i + "Age\",\"childageerror_" + name + i + "\")' /> <span class='yrposion'>yr</span>"
                + "</h6><span id='childageerror_" + name + i + "' style='float: right;margin: 0px 0px 0px 0px;color: #e71820;font-size: 9px;'></span></div>"
                + "</div></div>";
        }
        $(appentdiv).html(bindagedel);
    }
    else {
        $(appentdiv).html("");
    }
}

$("#CheSelf").click(function () {
    $("#txtSelfAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SelfAge").removeClass("hidden");
    }
    else {
        $(".SelfAge").addClass("hidden");
    }
})

$("#CheSpouse").click(function () {
    $("#txtSpouseAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".SpouseAge").removeClass("hidden");
    }
    else {
        $(".SpouseAge").addClass("hidden");
    }
})

$("#CheFather").click(function () {
    $("#txtFatherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".FatherAge").removeClass("hidden");
    }
    else {
        $(".FatherAge").addClass("hidden");
    }
})

$("#CheMother").click(function () {
    $("#txtMotherAge").val("");
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $(".MotherAge").removeClass("hidden");
    }
    else {
        $(".MotherAge").addClass("hidden");
    }
})

$(".clickcheckbox").click(function () {
    var selectedId = $(this).attr("id");
    if ($("#" + selectedId).prop("checked") == true) {
        $("#" + selectedId + "Input").val("1");
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").removeClass("hidden");
        SonDautAgeSectionPlusMinus("plus", 1, "#div" + thisname + "AgeSection", thisname);
    }
    else {
        var thisname = $("#" + selectedId).data("name");
        $("#" + selectedId + "Age").addClass("hidden");
        SonDautAgeSectionPlusMinus("minus", 1, "#div" + thisname + "AgeSection", thisname);
    }
});

CalculateChildAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) <= 24) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age range : 91 Days-24 Y&nbsp;</span>");
        }
    }
}

const ageCalcChildDays = (date) => {
    let splitstartdate = date.split('/');
    var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
    let age = Math.floor((new Date() - start) / 86400000);
    return age;
}

CalculateAdultAge = (fid, errorclass) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        if (parseInt(startdate) >= 18 && parseInt(startdate) <= 60) {
            $("#" + fid).removeClass("errorpage");
            $("#" + errorclass).html("");
        }
        else {
            $("#" + fid).val("");
            $("#" + fid).addClass("errorpage");
            $("#" + errorclass).html("<i class='fa fa-exclamation-triangle'></i> Age range : 18-60 Y&nbsp;</span>");
        }
    }
}

function GetProductList() {
    var isfieldapplied = false;
    let membercount = 0;
    let tchildcount = 0;
    let parentcount = 0;

    if (CheckFocusDropDownBlankValidation("ddlPolicyType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlSumInsured")) return !1;
    if (CheckFocusBlankValidation("txtPinCode")) return !1;
    if ($("#CheSelf").is(":checked")) { if (CheckFocusBlankValidation("txtSelfAge")) return !1; isfieldapplied = true; membercount = membercount + 1; parentcount = parentcount + 1; }
    if ($("#CheSpouse").is(":checked")) { if (CheckFocusBlankValidation("txtSpouseAge")) return !1; isfieldapplied = true; membercount = membercount + 1; parentcount = parentcount + 1; }
    if ($("#chkSon").is(":checked")) {
        var noofson = $("#chkSonInput").val(); var i;
        for (i = 1; i <= noofson; i++) {
            if (CheckFocusBlankValidation("txtSon" + i + "Age")) return !1;
        }
        isfieldapplied = true;
        membercount = membercount + parseInt(noofson);
        tchildcount = tchildcount + parseInt(noofson);
    }
    if ($("#chkDaughter").is(":checked")) {
        var noofdaughter = $("#chkDaughterInput").val(); var i;
        for (i = 1; i <= noofdaughter; i++) {
            if (CheckFocusBlankValidation("txtDaughter" + i + "Age")) return !1;
        }
        isfieldapplied = true;
        membercount = membercount + parseInt(noofdaughter);
        tchildcount = tchildcount + parseInt(noofdaughter);
    }
    //if ($("#CheFather").is(":checked")) { if (CheckFocusBlankValidation("txtFatherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }
    //if ($("#CheMother").is(":checked")) { if (CheckFocusBlankValidation("txtMotherAge")) return !1; isfieldapplied = true; membercount = membercount + 1; }

    var slectedOpt = $("#ddlPolicyType option:selected").val().toLowerCase();
    if (slectedOpt == "individual") {
        if (membercount < 1) {
            ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Please select at least one member for genrate quote.", "#ff414d"); return false;
        }
    }
    else {
        if (tchildcount > 0) {
            if (parentcount != 2) {
                ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Both parents should be selected for child insurance.", "#ff414d"); return false;
            }
        }
        if (tchildcount > 2) {
            ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Total Children should not be more than 2.", "#ff414d"); return false;
        }
        if (membercount > 4) {
            ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Total members should not be more than 4.", "#ff414d"); return false;
        }

        //if (membercount < 2) {
        //    ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Please select minimum two members for genrate quote.", "#ff414d"); return false;
        //}
        //else if (tchildcount > 2) {
        //    ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Total Children should not be more than 2.", "#ff414d"); return false;
        //}
        //else if (membercount > 4) {
        //    ShowMessagePopup("<i class='fa fa-info-circle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Information !", "#ff414d", "Total members should not be more than 4.", "#ff414d"); return false;
        //}
    }
    if (CheckFocusBlankValidation("txtPinCode")) return !1;

    var quotesVal = {};
    quotesVal.Policy_Type = $("#ddlPolicyType option:selected").val();
    quotesVal.Policy_Type_Text = $("#ddlPolicyType option:selected").text();
    quotesVal.Sum_Insured = $("#ddlSumInsured option:selected").val();
    quotesVal.Self = $('#CheSelf').prop('checked');
    quotesVal.SelfAge = $("#txtSelfAge").val();
    quotesVal.Spouse = $('#CheSpouse').prop('checked');
    quotesVal.SpouseAge = $("#txtSpouseAge").val();
    quotesVal.Son = $('#chkSon').prop('checked');
    quotesVal.Son1Age = $("#txtSon1Age").val();
    quotesVal.Son2Age = $("#txtSon2Age").val();
    //quotesVal.Son3Age = $("#txtSon3Age").val();
    //quotesVal.Son4Age = $("#txtSon4Age").val();
    quotesVal.Daughter = $('#chkDaughter').prop('checked');
    quotesVal.Daughter1Age = $("#txtDaughter1Age").val();
    quotesVal.Daughter2Age = $("#txtDaughter2Age").val();
    //quotesVal.Daughter3Age = $("#txtDaughter3Age").val();
    //quotesVal.Daughter4Age = $("#txtDaughter4Age").val();
    //quotesVal.Father = $('#CheFather').prop('checked');
    //quotesVal.FatherAge = $("#txtFatherAge").val();
    //quotesVal.Mother = $('#CheMother').prop('checked');
    //quotesVal.MotherAge = $("#txtMotherAge").val();
    quotesVal.Pin_Code = $("#txtPinCode").val();
    quotesVal.Enquiry_Id = GetParameterValues('enquiryid');//GetEnquiryId();

    if (quotesVal.Pin_Code.length == 6) {
        if ($("#txtPinCode").hasClass("successpincode")) {
            $("#btnGetQuotes").html("Please Wait... <i class='fa fa-spin fa-spinner'></i>");
            $.ajax({
                type: "Post",
                url: "/Health/SaveQuoteDetails",
                data: '{quote: ' + JSON.stringify(quotesVal) + '}',
                contentType: "application/json; charset=utf-8",
                datatype: "json",
                success: function (response) {
                    if (response != "") {
                        if (response == "notredirect") {
                            window.location.href = "/health/helthsearch";
                        }
                        else {
                            window.location.href = response;
                        }
                    }
                    else {
                        $("#btnGetQuotes").html("GET QUOTES");
                    }
                }
            });
        }
        else {
            if (CheckFocusBlankValidation("txtPinCode")) return !1;
        }
    }
    else {
        if (NotExistFocusBlankValidation("txtPinCode")) return !1;
        $(".notpincodevalid").removeClass("hidden").html("provide valid pincode");
    }
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    //$(".modaldioclass").css("width", "500px");
    $("#commonheading").css("color", headcolor).html(headerHeading);
    $("#commoncontent").css("color", bodycolor).html(bodyMsg);
    $(".compopupopenclass").click();
}
//ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Error !", "#ff414d", data.d[1], "#ff414d");
//ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Remitter registration has been successfully completed.", "#28a745");

function checkSection1RegForm() {
    if (CheckFocusDropDownBlankValidation("ddlPolicyType")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlSumInsured")) return !1;
    if (CheckFocusBlankValidation("txtPinCode")) return !1;
    if (!$("#txtPinCode").hasClass("errorpincode")) {
        $(".section1").addClass("hidden");
        $(".section2").removeClass("hidden");
        $(".linext2").addClass("active");
    }
}
function Backform() {
    $(".section2").addClass("hidden");
    $(".linext2").removeClass("active");
    $(".section1").removeClass("hidden");
}

GetEnquiryId = () => {
    let enquiryid = GetParameterValues('enquiryid');
    if (enquiryid != "" && enquiryid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Health/DecodeGenrateEnquiryId",
            data: '{enquiryid: ' + JSON.stringify(enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                debugger;
                if (response.length == 15) {
                    return response;
                }
                else {
                    window.location.href = "/health/healthrejistration";
                }
            }
        });
    }
    else {
        window.location.href = "/health/healthrejistration";
    }
}