﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    /*localStorage.removeItem("autologin");
    if (localStorage.getItem("autologin") == null)
    {
        AutoLogin();
    }
    */
    let agencyid = GetParameterValues("agencyid");
    let type = GetParameterValues("type");
    if (type != "" && type != undefined) { AutoLogin(agencyid); }
});

function AutoLogin(agencyid) {
    if (agencyid != "" && agencyid != undefined) {
        $.ajax({
            type: "Post",
            url: "/Home/Agency_AutoLogin",
            data: '{userid: ' + JSON.stringify(agencyid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response[0] == "success") {
                    localStorage.setItem("autologin", true);
                    window.location.href = "/";
                }
                else {
                    localStorage.setItem("autologin", false);
                }
            }
        });
    }
}

function GetQueryStringParams(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}