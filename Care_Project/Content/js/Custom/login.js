﻿UserLogin = () => {
    $("#LoginErrorMsg").html("");
    if (CheckFocusBlankValidation("txtUserId")) return !1;
    if (CheckFocusBlankValidation("txtPassword")) return !1;

    let userid = $("#txtUserId").val();
    let password = $("#txtPassword").val();
    $("#btnLogin").html("Processing...<i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Home/Agency_Login",
        data: '{userid: ' + JSON.stringify(userid) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response[0] == "failed") {
                // $(".close").click();
                $("#LoginErrorMsg").html(response[1]);
                $("#btnLogin").html("<i class='fa fa-unlock-alt'></i>&nbsp;Sign In");
            }
            else if (response[0] == "success") {
                location.reload();
            }
        }
    });
}

LogoutEvnt = () => {
    $.ajax({
        type: "Post",
        url: "/Home/Agency_Logout",
        //data: '{userid: ' + JSON.stringify(userid) + ',password: ' + JSON.stringify(password) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            //location.reload();
            window.location.href = "/";
        }
    });
}