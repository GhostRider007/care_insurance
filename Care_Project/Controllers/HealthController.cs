﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Care_Project.Methods.CommonClass;
using Care_Project.Methods.Service;
using Care_Project.Methods.Utility;
using Care_Project.Models.Health;
using static Care_Project.Models.Health.HealthComparisonModel;
using static Care_Project.Models.Health.HealthModel;

namespace Care_Project.Controllers
{
    public class HealthController : Controller
    {
        #region[ActionResults]
        public ActionResult HealthRejistration()
        {
            Health_Service.RemoveHealth_RespoFromCache();
            return View();
        }
        public ActionResult HealthQuotelist()
        {
            return View();
        }
        public ActionResult HealthProposal()
        {
            return View();
        }
        public ActionResult HealthProposalSummary()
        {
            return View();
        }

        [AcceptVerbs(WebRequestMethods.Http.Get, WebRequestMethods.Http.Post)]
        public ActionResult HealthPaymentSuccess(PaymentModel payment, string inquiryid)
        {
            try
            {
                //payment.transactionRefNum = "403993715523424457";
                //payment.policyNumber = "10474059";
                //payment.uwDecision = "INFORCE";

                if (!string.IsNullOrEmpty(inquiryid))
                {
                    payment.enquiryid = inquiryid;
                    if (!string.IsNullOrEmpty(payment.uwDecision))
                    {
                        if (payment.uwDecision.ToLower().Trim().Contains("inforce"))
                        {
                            payment.Status = "Successful";
                        }
                        else if (payment.uwDecision.ToLower().Trim().Contains("pending"))
                        {
                            payment.Status = "Successful with Underwriting Scenario";
                        }
                        else
                        {
                            payment.Status = "Failure";
                        }
                    }
                    else
                    {
                        payment.Status = "N/A";
                    }

                    if (!string.IsNullOrEmpty(payment.enquiryid))
                    {
                        bool issuccess = Health_Service.UpdateCarePaymentDetails(payment);
                        payment = Health_Service.GetPaymentSuccessPageData(payment);
                    }
                }
                else
                {
                    payment.Status = "Failure";
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return View(payment);
        }
        #endregion

        #region [Json Section]

        #region[HealthRejistration]
        public JsonResult GenrateEnquiryId()
        {
            return Json("/health/healthrejistration?enquiryid=" + Health_Service.EncodeGenrateEnquiryId());
        }
        public JsonResult GetSumInsuredPriceList()
        {
            return Json(Health_Service.GetSumInsuredPriceList());
        }
        public JsonResult CheckPincode(string pincode)
        {
            string result = string.Empty;

            try
            {
                string city = Health_Service.CheckPinCodeExist(pincode);
                if (!string.IsNullOrEmpty(city))
                {
                    result = city;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(result);
        }
        public JsonResult SaveQuoteDetails(HealthEnquiry quote)
        {
            string result = string.Empty;
            if (Health_Service.SaveQuoteDetails(quote))
            {
                result = "/health/healthquotelist?enquiryid=" + quote.Enquiry_Id;
            }
            return Json(result.ToLower());
        }
        #endregion

        #region [Health Listing Section]
        public JsonResult GetHealthSearchList(string enquiryid)
        {
            return Json(Health_Service.GetHealthApiResponse(enquiryid, string.Empty));
        }
        public JsonResult InsertSearchUrlParaDetail(string enquiryid, string urlpara)
        {
            return Json(Health_Service.InsertSearchUrlParaDetail(enquiryid, urlpara));
        }
        public JsonResult FilterSection(FilterSection filter)
        {
            return Json(Health_Service.GetFilterHealthApiResponse(filter));
        }
        public JsonResult CompareInsuranceProducts(List<CompareRequest> compreqarray, string enqid)
        {
            return Json(Health_Service.CompareInsuranceProducts(compreqarray, enqid, string.Empty));
        }
        #endregion

        #region [Proposal Section]
        public JsonResult GetSelectedPlanDetails(string enquiryid)
        {
            return Json(Health_Service.GetSelectedPlanDetails(enquiryid));
        }
        public JsonResult InsertSelectedPlanDetail(HealthProposal proposal, List<AddonChecked> addonlist)
        {
            return Json(Health_Service.InsertSelectedPlanDetail(proposal, addonlist));
        }
        public JsonResult GetAnnualIncomeDetails(string enquiryid)
        {
            return Json(Health_Service.GetAnnualIncomeDetails(enquiryid));
        }
        public JsonResult InsertProposerDetails(HealthProposal proposal)
        {
            return Json(Health_Service.InsertProposerDetails(proposal));
        }
        public JsonResult BindInsuredDetails(HealthProposal proposal)
        {
            return Json(Health_Service.GetInsuredDetails(proposal));
        }
        public JsonResult InsertInsured_NomineeDetail(HealthProposal proposal)
        {
            return Json(Health_Service.InsertInsured_NomineeDetail(proposal));
        }
        public JsonResult CreateHealthProposal(string enquiryid, List<HealthPerposalQuestion> medhistory)
        {
            return Json(Health_Service.GenrateCareProposal(enquiryid, string.Empty, medhistory));
        }
        public JsonResult AddLessAddon(string[] addonlist)
        {
            bool issuccess = Health_Service.AddLessAddon(addonlist);
            //return Json(Health_Service.SetPremiumBreakup(addonlist[9], addonlist[10], addonlist[11], addonlist[12], addonlist[13], addonlist[14], addonlist[7]));
            return Json("");
        }
        #endregion

        #region  [HealthProposalSummary Section]
        public JsonResult BindProposalSummaryDetails(string enquiryid)
        {
            return Json(Health_Service.BindProposalSummaryDetails(enquiryid));
        }
        public JsonResult GenratePaymentUrl(string enquiryid)
        {
            return Json(Health_Service.GetHealthCarePaymentUrl(enquiryid));
        }
        public JsonResult CheckHealthPaymentDetails(string enquiryid)
        {
            return Json(Health_Service.CheckHealthPaymentDetails(enquiryid));
        }
        public JsonResult GetHealthPdfDocument(string enquiryid)
        {
            string docurl = string.Empty;
            try
            {
                List<string> policyProDetails = Health_Service.GetHealthPdfDocument(enquiryid);

                if (!string.IsNullOrEmpty(policyProDetails[0]) && !string.IsNullOrEmpty(policyProDetails[1]))
                {
                    var request = System.Net.WebRequest.Create(Config.ApiUrl + "Care/PolicyDocument/" + policyProDetails[0] + "/" + policyProDetails[1] + "/POLSCHD");
                    request.Method = "GET";

                    using (var response = request.GetResponse())
                    {
                        using (var stream = response.GetResponseStream())
                        {
                            string path = Server.MapPath("~/health_pdf/" + enquiryid + ".pdf");
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                stream.CopyTo(fileStream);
                                bool ispdflink = Health_Service.UpdateHealthPolicyPdfLink(enquiryid, "health_pdf/" + enquiryid + ".pdf");
                                if (ispdflink)
                                {
                                    docurl = Config.WebsiteUrl + ("health_pdf/" + enquiryid + ".pdf");
                                }
                            }
                        }
                    }
                }
                else
                {
                    docurl = policyProDetails[0];
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return Json(docurl);
        }
        #endregion

        #region[Common]
        public JsonResult DecodeGenrateEnquiryId(string enquiryid)
        {
            //string enqid = Health_Service.DecodeGenrateEnquiryId(enquiryid);
            //return Json(enqid);
            return Json(enquiryid);
        }
        //public JsonResult IndianMoneyFormat(string amount)
        //{
        //    return Json(UtilityClass.IndianMoneyFormat(amount));
        //}
        #endregion

        #endregion
    }
}