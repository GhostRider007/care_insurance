﻿using Care_Project.Methods.Service;
using Care_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace Care_Project.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index(string type, string agencyid)
        {
            if (!string.IsNullOrEmpty(type) && !string.IsNullOrEmpty(agencyid))
            {
                Administrator_Service.LogoutAgency();
                List<string> isAgencyLogin = Administrator_Service.GetAgencyById(agencyid);
            }
            return View();
        }
        public ActionResult Health()
        {
            return View();
        }

        #region [Json Section]
        public JsonResult Agency_Login(string userid, string password)
        {
            return Json(Administrator_Service.Agency_Login(userid, password));
        }

        public JsonResult Agency_AutoLogin(string userid)
        {
            return Json(Administrator_Service.GetAgencyById(userid));
        }
        public JsonResult Agency_Logout()
        {
            return Json(Administrator_Service.LogoutAgency());
        }
        #endregion
    }
}