using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Care_Project
{
    public class MvcApplication : System.Web.HttpApplication
    {
        //protected void Application_BeginRequest()
        //{
        //    if (FormsAuthentication.RequireSSL && !Request.IsSecureConnection)
        //    {
        //        Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
        //    }
        //}
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
