﻿using Care_Project.Methods.CommonClass;
using Care_Project.Methods.Utility;
using Care_Project.Models.Health;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;

namespace Care_Project.Methods.API_Post
{
    public static class Post_Utility
    {
        public static string GetCommonUrl(string url)
        {
            return Config.ApiUrl + url;
        }
        public static string GetrAuthenticateToken(string agencyId)
        {
            string result = string.Empty;
            if (IsAccessTokenExist(ref result))
            {
                return result;
            }
            else
            {
                string trackId = UtilityClass.GenrateRandomTransactionId("INS", 7, null);
                string url = GetCommonUrl("Authenticate/Login");
                string requestJson = "{\"username\": \"" + Config.ApiUserId + "\",\"password\": \"" + Config.ApiPassword + "\"}";
                string response = PostHealthAPI("POST", url, requestJson, "Get_Token", agencyId, trackId);
                if (!string.IsNullOrEmpty(response))
                {
                    Health_Auth hAuth = JsonConvert.DeserializeObject<Health_Auth>(response);
                    if (hAuth.success)
                    {
                        result = hAuth.response.tokenvalue;
                        InitializeAccessTokenSession(result);
                    }
                }
            }
            return result;
        }

        #region [Connect To API Swagger Url]
        public static string GetHealthSearchListing(string requestJson, string agencyId, string tokenKey)
        {
            return PostHealthAPI("POST", GetCommonUrl("SearchProduct/searchProduct"), requestJson, "Get_Health_Listing", agencyId, UtilityClass.GenrateTrackId(), tokenKey);
        }
        public static string GenrateCareProposal(string requestJson, string agencyId, string tokenKey)
        {
            return PostHealthAPI("POST", GetCommonUrl("CreateProposal/CreateProposal"), requestJson, "Create_Proposal", agencyId, UtilityClass.GenrateTrackId(), tokenKey);
        }
        public static string PostHealthAPI(string postType, string postUrl, string requestJson, string actionType, string agencyId, string trackId, string tokenKey = null)
        {
            string ReturnValue = string.Empty;
            //ConnectToDataBase.Logger("Information", postUrl, "Request captured in post Method : " + postUrl + "", requestJson, actionType, "Health", agencyId, trackId, "");

            StringBuilder sbResult = new StringBuilder();

            //ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(postUrl);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(requestJson);
                if (!string.IsNullOrEmpty(requestJson))
                {
                    Http.ContentLength = lbPostBuffer.Length;
                }
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";
                if (!string.IsNullOrEmpty(tokenKey))
                {
                    Http.Headers["Authorization"] = "Bearer " + tokenKey;
                }

                if (!string.IsNullOrEmpty(requestJson))
                {
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();

                        //ConnectToDataBase.Logger("Success", postUrl, "Request captured in post Method : " + postUrl + "", requestJson, ReturnValue, actionType, agencyId, trackId, "");
                    }
                }
            }
            catch (WebException wex)
            {
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        ReturnValue = GetWebResponseString(httpResponse);
                    }
                }

                //ConnectToDataBase.Logger("Error", postUrl, "Error : Request captured in post Method : " + postUrl + "", requestJson, "", actionType, agencyId, trackId, httpResponse.StatusDescription);
                //ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
            }

            return ReturnValue;
        }
        public static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            string aa = "";
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            Stream streamResponse = responseStream;
            using (responseStream)
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip") || myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                {
                    if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                    }
                    else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                    }
                    using (StreamReader streamRead = new StreamReader(streamResponse))
                    {
                        //char[] readBuffer = new char[checked((IntPtr)myHttpWebResponse.ContentLength)];

                        //for (int count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)); count > 0; count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)))
                        //{
                        //    rawResponse.Append(new string(readBuffer, 0, count));
                        //}
                    }
                    aa = rawResponse.ToString();
                }
                else
                {
                    aa = (new StreamReader(streamResponse)).ReadToEnd().Trim();
                }
            }
            return aa;
        }
        public static string PostHealtApiWithoutBody(string postUrl, string tokenKey)
        {
            var client = new RestClient(postUrl);
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            request.AddHeader("Authorization", "Bearer " + tokenKey);
            IRestResponse response = client.Execute(request);
            return response.Content;
        }
        #endregion

        #region [Initialization Session]
        public static bool IsAccessTokenExist(ref string token)
        {
            if (HttpContext.Current.Session["accessToken"] != null)
            {
                List<string> objAccessToken = (List<string>)HttpContext.Current.Session["accessToken"];
                TimeSpan TS = DateTime.Now - Convert.ToDateTime(objAccessToken[1]);
                if (TS.Minutes < 59)
                {
                    token = objAccessToken[0];
                    return true;
                }
                else
                {
                    HttpContext.Current.Session["accessToken"] = null;
                }
            }

            return false;
        }
        public static void InitializeAccessTokenSession(string accessToken)
        {
            if (!string.IsNullOrEmpty(accessToken))
            {
                List<string> objAccessToken = new List<string>();
                objAccessToken.Add(accessToken);
                objAccessToken.Add(DateTime.Now.ToString());
                objAccessToken.Add("exist");
                HttpContext.Current.Session["accessToken"] = objAccessToken;
            }
        }
        #endregion
    }
}