﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Methods.CommonClass
{
    public class CommonClass
    {
        public class PinCodeMaster
        {
            public int ID { get; set; }
            public string PINCODE { get; set; }
            public string CITYCODE { get; set; }
            public string CITYDESC { get; set; }
            public string STATECODE { get; set; }
            public string VERSIONID { get; set; }
            public string GUID { get; set; }
            public string ZONECODE { get; set; }
            public string DISTRICTCODE { get; set; }
        }
    }
}