﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Care_Project.Methods.CommonClass
{
    public static class Config
    {
        public static string ApiUserId
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ApiUserId"]); }
        }
        public static string ApiPassword
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["ApiPassword"]); }
        }
        public static string ConnectionString_GroupCare
        {
            get { return ConfigurationManager.ConnectionStrings["GroupCare"].ConnectionString; }
        }
        public static string ConnectionString_Richa
        {
            get { return ConfigurationManager.ConnectionStrings["MyAmdDB"].ConnectionString; }
        }
        public static string ApiUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["helthapiurl"]); }
        }
        public static string BaseApiUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["helthapiurl2"]); }
        }
        public static string GetLogoLink
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["weblogo"]); }
        }
        public static string WebsiteName
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websitename"]); }
        }
        public static string WebsiteUrl
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["websiteurl"]); }
        }
        public static string AdminEmail
        {
            get { return Convert.ToString(ConfigurationManager.AppSettings["adminemail"]); }
        }
    }
}