﻿using Care_Project.Methods.CommonClass;
using Care_Project.Models.Health;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Care_Project.Methods.DataBase
{
    public static class ConnectToDataBase
    {
        #region [Common Database setup]
        private static SqlConnection sqlCon = new SqlConnection(Config.ConnectionString_GroupCare);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }

        #region[Common CRUD]
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("usp_GetRecordFromTable", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("tablename", tablename);
                Command.Parameters.AddWithValue("where", whereCondition);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static bool InsertRecordToTable(string fileds, string fieldValue, string tableName)
        {
            try
            {
                Command = new SqlCommand("usp_InsertRecordToTable", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("values", fieldValue);
                Command.Parameters.AddWithValue("tablename", tableName);

                OpenConnection(sqlCon);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(sqlCon);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool UpdateRecordIntoAnyTable(string tableName, string fieldsWithValue, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("ups_UpdateRecordToTable", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("TableName", tableName);
                Command.Parameters.AddWithValue("FieldsWithValue", fieldsWithValue);
                Command.Parameters.AddWithValue("WhereCondition", whereCondition);

                OpenConnection(sqlCon);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(sqlCon);

                if (isSuccess > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion

        #endregion

        #region[GET DETAILS]
        public static DataTable GetPincodeDetail(string pincode)
        {
            try
            {
                Command = new SqlCommand("sp_GetPincodeDetails", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("pincode", pincode);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "PinCodeCityStateMap");
                ObjDataTable = ObjDataSet.Tables["PinCodeCityStateMap"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static DataTable GetSumInsuredList(string sumInsuredId = "")
        {
            try
            {
                Command = new SqlCommand("sp_GetSumInsuredDetail", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("SumInsuredId", sumInsuredId);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "SumInsuredMaster");
                ObjDataTable = ObjDataSet.Tables["SumInsuredMaster"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static DataTable GetHealth_ResponseDetail(string enquiryId)
        {
            return GetRecordFromTable("*", "T_Health_Response", "enquiryid='" + enquiryId + "'");
        }
        public static DataTable GetHealthEnquiryDetail(string enquiryId)
        {
            return GetRecordFromTable("*", "T_Enquiry_Health", "enquiry_id='" + enquiryId + "'");
        }
        public static DataTable GetHealthProposalDetail(string enquiryid)
        {
            return GetRecordFromTable("*", "T_HealthProposal", "enquiryid='" + enquiryid + "'");
        }
        public static DataTable GetAnnualIncomeDetails(string enquiryid, string companyid = "")
        {
            string wherecon = "";
            if (!string.IsNullOrEmpty(companyid))
            {
                wherecon = "CompanyId='" + companyid + "'";
            }
            else
            {
                wherecon = "CompanyId in (select companyid from T_HealthProposal where enquiryid='" + enquiryid + "')";
            }
            return GetRecordFromTable("*", "T_AnnualIncome", wherecon);
        }
        public static DataTable GetRelationShipDetail()
        {
            return GetRecordFromTable("*", "T_Relationship_Common", "CompanyId='1'");
        }
        public static DataTable GetInsuredDetails(string enquiryid)
        {
            return GetRecordFromTable("*", "T_HealthInsured", "enquiryid='" + enquiryid + "' order by insuredcount");
        }
        public static DataTable GetNomineeDetails()
        {
            return GetRecordFromTable("*", "T_Nominee", string.Empty);
        }
        public static DataTable GetOccupation()
        {
            return GetRecordFromTable("*", "T_Occupation", string.Empty);
        }
        #endregion

        #region[INSERT/UPDATE DETAILS]
        public static bool InsertEnqueryDetail(string agencyId, string agencyName, string pType, string pTypeText, string sumInsured, string self, string selfAge, string spouse, string spouseAge, string son, string son1Age, string son2Age, string son3Age, string son4Age, string daughter, string daughter1Age, string daughter2Age, string daughter3Age, string daughter4Age, string father, string fatherAge, string mother, string motherAge, string pincode, string enquiryId)
        {
            try
            {
                Command = new SqlCommand("sp_InsertNewQuoteDetail", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@AgencyId", agencyId));
                Command.Parameters.Add(new SqlParameter("@AgencyName", agencyName));
                Command.Parameters.Add(new SqlParameter("@Policy_Type", pType));
                Command.Parameters.Add(new SqlParameter("@Policy_Type_Text", pTypeText));
                Command.Parameters.Add(new SqlParameter("@Sum_Insured", sumInsured));
                Command.Parameters.Add(new SqlParameter("@Self", self));
                Command.Parameters.Add(new SqlParameter("@SelfAge", selfAge));
                Command.Parameters.Add(new SqlParameter("@Spouse", spouse));
                Command.Parameters.Add(new SqlParameter("@SpouseAge", spouseAge));
                Command.Parameters.Add(new SqlParameter("@Son", son));
                Command.Parameters.Add(new SqlParameter("@Son1Age", son1Age));
                Command.Parameters.Add(new SqlParameter("@Son2Age", son2Age));
                Command.Parameters.Add(new SqlParameter("@Son3Age", son3Age));
                Command.Parameters.Add(new SqlParameter("@Son4Age", son4Age));
                Command.Parameters.Add(new SqlParameter("@Daughter", daughter));
                Command.Parameters.Add(new SqlParameter("@Daughter1Age", daughter1Age));
                Command.Parameters.Add(new SqlParameter("@Daughter2Age", daughter2Age));
                Command.Parameters.Add(new SqlParameter("@Daughter3Age", daughter3Age));
                Command.Parameters.Add(new SqlParameter("@Daughter4Age", daughter4Age));
                Command.Parameters.Add(new SqlParameter("@Father", father));
                Command.Parameters.Add(new SqlParameter("@FatherAge", fatherAge));
                Command.Parameters.Add(new SqlParameter("@Mother", mother));
                Command.Parameters.Add(new SqlParameter("@MotherAge", motherAge));
                Command.Parameters.Add(new SqlParameter("@Pin_Code", pincode));
                Command.Parameters.Add(new SqlParameter("@Enquiry_Id", enquiryId));

                OpenConnection(sqlCon);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(sqlCon);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertHealthRequestData(string enquiryId, string reqData)
        {
            bool isSuccess = false;
            try
            {
                DataTable dtRespoDetail = GetHealth_ResponseDetail(enquiryId);
                if (dtRespoDetail != null && dtRespoDetail.Rows.Count > 0)
                {
                    isSuccess = UpdateRecordIntoAnyTable("T_Health_Response", "searchrequest='" + reqData + "'", "enquiryid='" + enquiryId + "'");
                }
                else
                {
                    string fieldValue = "'" + enquiryId + "','" + reqData + "'";
                    isSuccess = InsertRecordToTable("enquiryid,searchrequest", fieldValue, "T_Health_Response");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return isSuccess;
        }
        public static bool UpdateHealthResponseData(string enquiryId, string respoData)
        {
            return UpdateRecordIntoAnyTable("T_Health_Response", "searchresponse='" + respoData + "'", "enquiryId='" + enquiryId + "'");
        }

        public static bool UpdateSearchUrlDetail(string enquiryId, string urlpara)
        {
            return UpdateRecordIntoAnyTable("T_Enquiry_Health", "searchurl='" + urlpara + "'", "Enquiry_Id='" + enquiryId + "'");
        }
        public static bool UpdateSelectedPlanDetail(string enquiryId, string selectedRespo)
        {
            return UpdateRecordIntoAnyTable("T_Enquiry_Health", "selectedproductjson='" + selectedRespo + "'", "Enquiry_Id='" + enquiryId + "'");
        }
        public static bool InsertUpdateHealthProposal(HealthProposal proposal)
        {
            try
            {
                Command = new SqlCommand("sp_InsertUpdateHealthProposal", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@enquiryid", proposal.enquiryid));
                Command.Parameters.Add(new SqlParameter("@productid", proposal.productid));
                Command.Parameters.Add(new SqlParameter("@companyid", proposal.companyid));
                Command.Parameters.Add(new SqlParameter("@suminsured_amount", proposal.suminsured_amount));
                Command.Parameters.Add(new SqlParameter("@period", proposal.period));
                Command.Parameters.Add(new SqlParameter("@selected_premium", proposal.selected_premium));
                Command.Parameters.Add(new SqlParameter("@p_title", proposal.p_title));
                Command.Parameters.Add(new SqlParameter("@p_firstname", proposal.p_firstname));
                Command.Parameters.Add(new SqlParameter("@p_lastname", proposal.p_lastname));
                Command.Parameters.Add(new SqlParameter("@p_mobile", proposal.p_mobile));
                Command.Parameters.Add(new SqlParameter("@p_dob", proposal.p_dob));
                Command.Parameters.Add(new SqlParameter("@p_email", proposal.p_email));
                Command.Parameters.Add(new SqlParameter("@p_address1", proposal.p_address1));
                Command.Parameters.Add(new SqlParameter("@p_address2", proposal.p_address2));
                Command.Parameters.Add(new SqlParameter("@p_landmark", proposal.p_landmark));
                Command.Parameters.Add(new SqlParameter("@p_pincode", proposal.p_pincode));
                Command.Parameters.Add(new SqlParameter("@p_cityname", proposal.p_cityname));
                Command.Parameters.Add(new SqlParameter("@p_statename", proposal.p_statename));
                Command.Parameters.Add(new SqlParameter("@p_pannumber", proposal.p_pannumber));
                Command.Parameters.Add(new SqlParameter("@p_aadharno", proposal.p_aadharno));
                Command.Parameters.Add(new SqlParameter("@p_annualincome", proposal.p_annualincome));
                Command.Parameters.Add(new SqlParameter("@p_isproposerinsured", proposal.p_isproposerinsured));
                Command.Parameters.Add(new SqlParameter("@n_title", proposal.n_title));
                Command.Parameters.Add(new SqlParameter("@n_firstname", proposal.n_firstname));
                Command.Parameters.Add(new SqlParameter("@n_lastname", proposal.n_lastname));
                Command.Parameters.Add(new SqlParameter("@n_dob", proposal.n_dob));
                Command.Parameters.Add(new SqlParameter("@n_age", proposal.n_age));
                Command.Parameters.Add(new SqlParameter("@n_relation", proposal.n_relationid));
                Command.Parameters.Add(new SqlParameter("@n_relationid", proposal.n_relationid));
                Command.Parameters.Add(new SqlParameter("@row_premium", proposal.row_premium));
                Command.Parameters.Add(new SqlParameter("@row_basePremium", proposal.row_basePremium));
                Command.Parameters.Add(new SqlParameter("@row_serviceTax", proposal.row_serviceTax));
                Command.Parameters.Add(new SqlParameter("@row_totalPremium", proposal.row_totalPremium));
                Command.Parameters.Add(new SqlParameter("@row_discountPercent", proposal.row_discountPercent));
                Command.Parameters.Add(new SqlParameter("@row_insuranceCompany", proposal.row_insuranceCompany));
                Command.Parameters.Add(new SqlParameter("@row_policyType", proposal.row_policyType));
                Command.Parameters.Add(new SqlParameter("@row_planname", proposal.row_planname));
                Command.Parameters.Add(new SqlParameter("@row_policyTypeName", proposal.row_policyTypeName));
                Command.Parameters.Add(new SqlParameter("@row_policyName", proposal.row_policyName));
                Command.Parameters.Add(new SqlParameter("@policypdflink", proposal.policypdflink));
                Command.Parameters.Add(new SqlParameter("@referenceId", proposal.referenceId));
                Command.Parameters.Add(new SqlParameter("@proposalNumber", proposal.proposalNumber));
                Command.Parameters.Add(new SqlParameter("@policyNumber", proposal.policyNumber));
                Command.Parameters.Add(new SqlParameter("@paymenturl", proposal.paymenturl));
                Command.Parameters.Add(new SqlParameter("@p_gender", proposal.p_gender));
                Command.Parameters.Add(new SqlParameter("@actionType", proposal.actionType));

                OpenConnection(sqlCon);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(sqlCon);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertPaymentDetail(HealthProposal proposal)
        {
            bool issuccess = false;
            DataTable dtPayment = GetRecordFromTable("*", "T_HealthPayment", "enquiryid ='" + proposal.enquiryid + "'");
            if (dtPayment != null && dtPayment.Rows.Count > 0)
            {
                issuccess = UpdateRecordIntoAnyTable("T_HealthPayment", "companyid='" + proposal.companyid + "',productId='" + proposal.productid + "'", "enquiryid ='" + proposal.enquiryid + "'");
            }
            else
            {
                string fieldValue = "'" + proposal.enquiryid + "'," + proposal.companyid + ",'Care Health','" + proposal.productid + "'";
                issuccess = InsertRecordToTable("enquiryid,companyid,companyname,productId", fieldValue, "T_HealthPayment");
            }
            return issuccess;
        }
        public static bool InsertUpdate_InsuredDetail(HealthInsured insured)
        {
            try
            {
                Command = new SqlCommand("sp_InsertUpdate_InsuredDetail", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.Add(new SqlParameter("@enquiryid", insured.enquiryid));
                Command.Parameters.Add(new SqlParameter("@productid", !string.IsNullOrEmpty(insured.productid) ? insured.productid : "0"));
                Command.Parameters.Add(new SqlParameter("@companyid", !string.IsNullOrEmpty(insured.companyid) ? insured.companyid : "0"));
                Command.Parameters.Add(new SqlParameter("@relationid", insured.relationid));
                Command.Parameters.Add(new SqlParameter("@relationname", insured.relationname));
                Command.Parameters.Add(new SqlParameter("@title", insured.title));
                Command.Parameters.Add(new SqlParameter("@firstname", insured.firstname));
                Command.Parameters.Add(new SqlParameter("@lastname", insured.lastname));
                Command.Parameters.Add(new SqlParameter("@dob", insured.dob));
                Command.Parameters.Add(new SqlParameter("@height", insured.height));
                Command.Parameters.Add(new SqlParameter("@weight", insured.weight));
                Command.Parameters.Add(new SqlParameter("@occupationId", insured.occupationId));
                Command.Parameters.Add(new SqlParameter("@occupation", insured.occupation));
                Command.Parameters.Add(new SqlParameter("@illness", insured.illness));
                Command.Parameters.Add(new SqlParameter("@illnessdesc", !string.IsNullOrEmpty(insured.illnessdesc) ? insured.illnessdesc : string.Empty));
                Command.Parameters.Add(new SqlParameter("@engageManualLabour", insured.engageManualLabour));
                Command.Parameters.Add(new SqlParameter("@engageManualLabourDesc", !string.IsNullOrEmpty(insured.engageManualLabourDesc) ? insured.engageManualLabourDesc : string.Empty));
                Command.Parameters.Add(new SqlParameter("@engageWinterSports", insured.engageWinterSports));
                Command.Parameters.Add(new SqlParameter("@engageWinterSportDesc", !string.IsNullOrEmpty(insured.engageWinterSportDesc) ? insured.engageWinterSportDesc : string.Empty));
                Command.Parameters.Add(new SqlParameter("@ispropinsured", insured.ispropinsured));
                Command.Parameters.Add(new SqlParameter("@insuredcount", insured.insuredcount));

                OpenConnection(sqlCon);
                int isSuccess = Command.ExecuteNonQuery();
                CloseConnection(sqlCon);

                if (isSuccess > 0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        public static bool InsertHealthQuestionary(string enquiryid, string reqData, string type)
        {
            string query = (type == "request" ? "questionaryrequest='" + reqData + "'" : "questionaryresponse='" + reqData + "'");
            return UpdateRecordIntoAnyTable("T_Health_Response", query, "enquiryid='" + enquiryid + "'");
        }
        public static bool InsertProposalRequestData(string prequest, string enquiryid, string type)
        {
            string query = (type == "request" ? "perposalrequest='" + prequest + "'" : "perposalresponse='" + prequest + "'");
            return UpdateRecordIntoAnyTable("T_Health_Response", query, "enquiryid='" + enquiryid + "'");
        }
        public static bool UpdateHealthMedicalHistory(string enquiryid, string medicalhistory, string firstname, string lastnane)
        {
            string wherecon = "enquiryid='" + enquiryid + "' and firstname='" + firstname + "' and lastname='" + lastnane + "'";
            return UpdateRecordIntoAnyTable("T_HealthInsured", "medicalhistory='" + medicalhistory + "'", wherecon);
        }
        public static bool UpdateProposalRespoData(string referenceId, string proposalNum, string enquiryid)
        {
            return UpdateRecordIntoAnyTable("T_HealthProposal", "referenceId='" + referenceId + "',proposalNumber='" + proposalNum + "',paymenturl='" + Config.BaseApiUrl + "CarePayment/" + proposalNum + "/" + enquiryid + "',updateddate=getdate()", "EnquiryId='" + enquiryid + "'");
        }
        public static bool UpdateStartEndPolicyDate(string enquiryid, string updateQuery)
        {
            return UpdateRecordIntoAnyTable("T_HealthProposal", updateQuery, "enquiryid='" + enquiryid + "'");
        }
        public static bool UpdatePaymentTabData(string referenceId, string proposalNum, string paymenturl, string premium, string serviceTax, string totalPremium, string enquiryid)
        {
            return UpdateRecordIntoAnyTable("T_HealthPayment", "referenceId='" + referenceId + "',proposalNum='" + proposalNum + "',paymenturl='" + paymenturl + "',premium='" + premium + "', serviceTax='" + serviceTax + "', totalPremium='" + totalPremium + "'", "enquiryid ='" + enquiryid + "'");
        }
        public static DataTable GetHealthPaymentDetail(string enquiryid)
        {
            return GetRecordFromTable("*", "T_HealthPayment", "enquiryid ='" + enquiryid + "'");
        }
        public static bool UpdateCarePaymentDetails(string policyNumber, string transactionRefNum, string uwDecision, string errorFlag, string errorMsg, string status, string enquiryid)
        {
            try
            {
                bool issuccess = UpdateRecordIntoAnyTable("T_HealthProposal", "policyNumber='" + policyNumber + "'", ("enquiryid='" + enquiryid + "'"));

                string fieldsWithValue = "cc_policyNumber='" + policyNumber + "',cc_transactionRefNum='" + transactionRefNum + "',cc_uwDecision='" + uwDecision + "',cc_errorFlag='" + errorFlag + "',cc_errorMsg='" + errorMsg + "',status='" + status + "',paymentstatus='1',paymentdate=getdate()";

                return UpdateRecordIntoAnyTable("T_HealthPayment", fieldsWithValue, ("enquiryid='" + enquiryid + "'"));
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }
        #endregion
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string pdfurl)
        {
            return UpdateRecordIntoAnyTable("T_HealthProposal", "policypdflink='" + pdfurl + "'", ("enquiryid='" + enquiryid + "'"));
        }

        public static DataTable GetAddLessAddon(string enquiryid, string addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID, string planAmount, string planPeriod)
        {
            string p_wherecon = "inquiryid='" + enquiryid + "' and addOnsId='" + code + "' and addOns='" + addOns + "' and code='" + code + "' and value='" + value + "' and calculation='" + calculation + "' and addOnValue='" + addOnValue + "' and productDetailID='" + productDetailID + "' and status=1 and PlanAmount='" + planAmount + "' and PlanPeriod='" + planPeriod + "'";
            return GetRecordFromTable("*", "T_HealthAddon", p_wherecon);
        }
        public static bool AddLessAddon(string[] addonlist)
        {
            bool issuccess = false;
            try
            {
                string p_wherecon = "inquiryid='" + addonlist[7] + "' and addOnsId='" + addonlist[2] + "' and addOns='" + addonlist[1] + "' and code='" + addonlist[2] + "' and value='" + addonlist[3] + "' and calculation='" + addonlist[4] + "' and addOnValue='" + addonlist[5] + "' and productDetailID='" + addonlist[6] + "' and PlanAmount='" + addonlist[13] + "' and PlanPeriod='" + addonlist[14] + "'";
                DataTable dtAddonExist = GetRecordFromTable("*", "T_HealthAddon", p_wherecon);
                if (dtAddonExist != null && dtAddonExist.Rows.Count > 0)
                {
                    issuccess = UpdateRecordIntoAnyTable("T_HealthAddon", "updateddate=getdate(),status=" + addonlist[8], p_wherecon);
                }
                else
                {
                    string fileds = "inquiryid,addOnsId,addOns,code,value,calculation,addOnValue,productDetailID,status,PlanAmount,PlanPeriod,BasePremium";
                    string fieldValue = "'" + addonlist[7] + "','" + addonlist[2] + "','" + addonlist[1] + "','" + addonlist[2] + "','" + addonlist[3] + "','" + addonlist[4] + "'" +
                        ",'" + addonlist[5] + "','" + addonlist[6] + "'," + addonlist[8] + ",'" + addonlist[13] + "','" + addonlist[14] + "','" + addonlist[9] + "'";
                    issuccess = InsertRecordToTable(fileds, fieldValue, "T_HealthAddon");
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return issuccess;
        }
        public static bool UpdateSelectedAddons(List<AddonChecked> addonlist)
        {
            bool isssucess = false;
            try
            {
                string inquiryid = addonlist[0].inquiryid;
                bool isUncheckedAll = UpdateRecordIntoAnyTable("T_HealthAddon", "isselected=0", "inquiryid='" + inquiryid + "'");
                if (addonlist != null && addonlist.Count > 0)
                {
                    foreach (var addon in addonlist)
                    {
                        string whereCondition = "addOnsId='" + addon.code + "' and code='" + addon.code + "' and PlanAmount='" + addon.PlanAmount + "' and PlanPeriod='" + addon.PlanPeriod + "' and inquiryid='" + addon.inquiryid + "'";
                        isssucess = UpdateRecordIntoAnyTable("T_HealthAddon", "isselected=1", whereCondition);
                    }

                    if (isssucess)
                    {
                        UpdateRecordIntoAnyTable("T_HealthAddon", "status=0", "isselected=0 and inquiryid='" + inquiryid + "'");
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return isssucess;
        }
        public static DataTable GetAddLessAddon(string enquiryid, string isselected = "")
        {
            return GetRecordFromTable("*", "T_HealthAddon", "inquiryid='" + enquiryid + "' and status=1" + (!string.IsNullOrEmpty(isselected) ? " and isselected='" + isselected + "'" : ""));
        }
    }
}