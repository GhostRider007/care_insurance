﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Care_Project.Methods.CommonClass;
using Care_Project.Methods.Utility;

namespace Care_Project.Methods.DataBase
{
    public static class Richa_ConnectToDataBase
    {
        #region [Common Database setup]
        private static SqlConnection sqlCon = new SqlConnection(Config.ConnectionString_Richa);
        private static SqlCommand Command { get; set; }
        private static SqlDataAdapter Adapter { get; set; }
        private static DataSet ObjDataSet { get; set; }
        private static DataTable ObjDataTable { get; set; }
        private static void OpenConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Closed)
            {
                constr.Open();
            }
        }
        public static void CloseConnection(SqlConnection constr)
        {
            if (constr.State == ConnectionState.Open)
            {
                constr.Close();
            }
        }
        #endregion

        #region[Common CRUD]
        public static DataTable GetRecordFromTable(string fileds, string tablename, string whereCondition)
        {
            try
            {
                Command = new SqlCommand("usp_GetRecordFromTable", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("fileds", fileds);
                Command.Parameters.AddWithValue("tablename", tablename);
                Command.Parameters.AddWithValue("where", whereCondition);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "CommonTable");
                ObjDataTable = ObjDataSet.Tables["CommonTable"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        #endregion
        public static DataTable Get_AgencyDetail(string userid)
        {
            try
            {
                Command = new SqlCommand("AgencyDetails", sqlCon);
                Command.CommandType = CommandType.StoredProcedure;
                Command.Parameters.AddWithValue("UserId", userid);

                Adapter = new SqlDataAdapter();
                Adapter.SelectCommand = Command;
                ObjDataSet = new DataSet();

                ObjDataTable = new DataTable();
                Adapter.Fill(ObjDataSet, "AgencyDetails");
                ObjDataTable = ObjDataSet.Tables["AgencyDetails"];
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return ObjDataTable;
        }
        public static DataTable AgentStaffLogin(string userid, string password)
        {
            Command = new SqlCommand("UserLoginNew", sqlCon);
            Command.CommandType = CommandType.StoredProcedure;
            Command.Parameters.AddWithValue("uid", userid);
            Command.Parameters.AddWithValue("pwd", password);

            Adapter = new SqlDataAdapter();
            Adapter.SelectCommand = Command;
            ObjDataSet = new DataSet();

            ObjDataTable = new DataTable();
            Adapter.Fill(ObjDataSet, "AgencyDetails");
            ObjDataTable = ObjDataSet.Tables["AgencyDetails"];
            return ObjDataTable;
        }
    }
}