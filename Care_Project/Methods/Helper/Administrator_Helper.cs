﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Care_Project.Methods.DataBase;

namespace Care_Project.Methods.Helper
{
    public static class Administrator_Helper
    {
        public static DataTable Agency_Login(string userid, string password)
        {
            return Richa_ConnectToDataBase.AgentStaffLogin(userid, password);
        }
        public static DataTable Get_AgencyDetail(string userid)
        {
            return Richa_ConnectToDataBase.Get_AgencyDetail(userid);
        }
    }
}