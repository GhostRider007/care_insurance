﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Web;
using Care_Project.Methods.API_Post;
using Care_Project.Methods.CommonClass;
using Care_Project.Methods.DataBase;
using Care_Project.Models.Health;
using Newtonsoft.Json.Linq;
using static Care_Project.Models.Health.HealthComparisonModel;
using static Care_Project.Models.Health.HealthModel;

namespace Care_Project.Methods.Helper
{
    public class Health_Helper
    {
        #region[HealthRejistration]
        public static List<SumInsuredMaster> GetSumInsuredPriceList()
        {
            List<SumInsuredMaster> sumInsuredList = new List<SumInsuredMaster>();

            try
            {
                DataTable dtSumInsuredList = ConnectToDataBase.GetSumInsuredList();
                if (dtSumInsuredList != null && dtSumInsuredList.Rows.Count > 0)
                {
                    for (int i = 0; i < dtSumInsuredList.Rows.Count; i++)
                    {
                        SumInsuredMaster suminsured = new SumInsuredMaster();
                        suminsured.SumInsuredId = Convert.ToInt32(dtSumInsuredList.Rows[i]["SumInsuredId"].ToString());
                        suminsured.MinSumInsured = dtSumInsuredList.Rows[i]["MinSumInsured"].ToString();
                        suminsured.MaxSumInsured = dtSumInsuredList.Rows[i]["MaxSumInsured"].ToString();
                        suminsured.SumInsuredText = "₹ " + suminsured.MinSumInsured + " - ₹ " + suminsured.MaxSumInsured;
                        suminsured.SumInsuredValue = suminsured.MinSumInsured + "-" + suminsured.MaxSumInsured;
                        sumInsuredList.Add(suminsured);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return sumInsuredList;
        }
        public static PinCode_Master CheckPinCodeExist(string pincode)
        {
            PinCode_Master pinDetail = new PinCode_Master();
            try
            {
                DataTable dtPinList = ConnectToDataBase.GetPincodeDetail(pincode);
                if (dtPinList != null && dtPinList.Rows.Count > 0)
                {
                    pinDetail.PINCODE = dtPinList.Rows[0]["PINCODE"].ToString();
                    pinDetail.CITYCODE = dtPinList.Rows[0]["CITYCODE"].ToString();
                    pinDetail.CITYDESC = dtPinList.Rows[0]["CITYDESC"].ToString();
                    pinDetail.STATECODE = dtPinList.Rows[0]["STATECODE"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return pinDetail;
        }
        public static bool SaveQuoteDetails(HealthEnquiry quote)
        {
            string agencyId = quote.AgencyId;
            string agencyName = quote.AgencyName;
            string pType = quote.Policy_Type;
            string pTypeText = quote.Policy_Type_Text;
            string sumInsured = quote.Sum_Insured;
            string self = quote.Self.ToString();
            string selfAge = quote.SelfAge.ToString();
            string spouse = quote.Spouse.ToString();
            string spouseAge = quote.SpouseAge.ToString();
            string son = quote.Son.ToString();
            string son1Age = quote.Son1Age.ToString();
            string son2Age = quote.Son2Age.ToString();
            string son3Age = quote.Son3Age.ToString();
            string son4Age = quote.Son4Age.ToString();
            string daughter = quote.Daughter.ToString();
            string daughter1Age = quote.Daughter1Age.ToString();
            string daughter2Age = quote.Daughter2Age.ToString();
            string daughter3Age = quote.Daughter3Age.ToString();
            string daughter4Age = quote.Daughter4Age.ToString();
            string father = quote.Father.ToString();
            string fatherAge = quote.FatherAge.ToString();
            string mother = quote.Mother.ToString();
            string motherAge = quote.MotherAge.ToString();
            string pincode = quote.Pin_Code;
            string enquiryId = quote.Enquiry_Id;
            return ConnectToDataBase.InsertEnqueryDetail(agencyId, agencyName, pType, pTypeText, sumInsured, self, selfAge, spouse, spouseAge, son, son1Age, son2Age, son3Age, son4Age, daughter, daughter1Age, daughter2Age, daughter3Age, daughter4Age, father, fatherAge, mother, motherAge, pincode, enquiryId);
        }
        public static HealthEnquiry GetHealthEnquiryDetail(string enquiryid)
        {
            HealthEnquiry hEnquiry = new HealthEnquiry();
            try
            {
                DataTable dtEnquiry = ConnectToDataBase.GetHealthEnquiryDetail(enquiryid);
                if (dtEnquiry != null && dtEnquiry.Rows.Count > 0)
                {
                    hEnquiry.AgencyId = dtEnquiry.Rows[0]["AgencyId"].ToString();
                    hEnquiry.AgencyName = dtEnquiry.Rows[0]["AgencyName"].ToString();
                    hEnquiry.Policy_Type = dtEnquiry.Rows[0]["Policy_Type"].ToString();
                    hEnquiry.Policy_Type_Text = dtEnquiry.Rows[0]["Policy_Type_Text"].ToString();
                    hEnquiry.Sum_Insured = dtEnquiry.Rows[0]["Sum_Insured"].ToString();
                    hEnquiry.Enquiry_Id = dtEnquiry.Rows[0]["Enquiry_Id"].ToString();
                    hEnquiry.Self = Convert.ToBoolean(dtEnquiry.Rows[0]["Self"].ToString());
                    hEnquiry.SelfAge = Convert.ToInt32(dtEnquiry.Rows[0]["SelfAge"].ToString());
                    hEnquiry.Spouse = Convert.ToBoolean(dtEnquiry.Rows[0]["Spouse"].ToString());
                    hEnquiry.SpouseAge = Convert.ToInt32(dtEnquiry.Rows[0]["SpouseAge"].ToString());
                    hEnquiry.Son = Convert.ToBoolean(dtEnquiry.Rows[0]["Son"].ToString());
                    hEnquiry.Son1Age = Convert.ToInt32(dtEnquiry.Rows[0]["Son1Age"].ToString());
                    hEnquiry.Son2Age = Convert.ToInt32(dtEnquiry.Rows[0]["Son2Age"].ToString());
                    hEnquiry.Son3Age = Convert.ToInt32(dtEnquiry.Rows[0]["Son3Age"].ToString());
                    hEnquiry.Son4Age = Convert.ToInt32(dtEnquiry.Rows[0]["Son4Age"].ToString());
                    hEnquiry.Daughter = Convert.ToBoolean(dtEnquiry.Rows[0]["Daughter"].ToString());
                    hEnquiry.Daughter1Age = Convert.ToInt32(dtEnquiry.Rows[0]["Daughter1Age"].ToString());
                    hEnquiry.Daughter2Age = Convert.ToInt32(dtEnquiry.Rows[0]["Daughter2Age"].ToString());
                    hEnquiry.Daughter3Age = Convert.ToInt32(dtEnquiry.Rows[0]["Daughter3Age"].ToString());
                    hEnquiry.Daughter4Age = Convert.ToInt32(dtEnquiry.Rows[0]["Daughter4Age"].ToString());
                    hEnquiry.Father = Convert.ToBoolean(dtEnquiry.Rows[0]["Father"].ToString());
                    hEnquiry.FatherAge = Convert.ToInt32(dtEnquiry.Rows[0]["FatherAge"].ToString());
                    hEnquiry.Mother = Convert.ToBoolean(dtEnquiry.Rows[0]["Mother"].ToString());
                    hEnquiry.MotherAge = Convert.ToInt32(dtEnquiry.Rows[0]["MotherAge"].ToString());
                    hEnquiry.Pin_Code = dtEnquiry.Rows[0]["Pin_Code"].ToString();
                    hEnquiry.searchurl = dtEnquiry.Rows[0]["searchurl"] != null ? dtEnquiry.Rows[0]["searchurl"].ToString() : string.Empty;
                    hEnquiry.selectedproductjson = dtEnquiry.Rows[0]["selectedproductjson"] != null ? dtEnquiry.Rows[0]["selectedproductjson"].ToString() : string.Empty;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return hEnquiry;
        }
        public static bool InsertSearchUrlParaDetail(string enquiryid, string urlpara)
        {
            return ConnectToDataBase.UpdateSearchUrlDetail(enquiryid, urlpara);
        }
        public static bool InsertSelectedPlanDetail(string enquiryid, string selectedRespo)
        {
            return ConnectToDataBase.UpdateSelectedPlanDetail(enquiryid, selectedRespo);
        }
        #endregion

        #region [Health Listing Section]
        public static string GetHealthApiResponse(string agencyId, string enquiryId, string requestJson)
        {
            try
            {
                string healthCacheRespo = GetHealth_RespoFromCache();
                if (!string.IsNullOrEmpty(healthCacheRespo))
                {
                    return healthCacheRespo;
                }
                else
                {
                    string tokenKey = Post_Utility.GetrAuthenticateToken(agencyId);
                    if (!string.IsNullOrEmpty(tokenKey))
                    {
                        if (ConnectToDataBase.InsertHealthRequestData(enquiryId, requestJson))
                        {
                            string response = Post_Utility.GetHealthSearchListing(requestJson, agencyId, tokenKey);
                            if (!string.IsNullOrEmpty(response))
                            {
                                if (ConnectToDataBase.UpdateHealthResponseData(enquiryId, response))
                                {
                                    StoreHealth_RespoInCache(response);
                                }
                                return response;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static List<string> CompareInsuranceProducts(List<CompareRequest> compReqArray, string enqId, string agencyId)
        {
            List<string> result = new List<string>();
            try
            {
                string tokenKey = Post_Utility.GetrAuthenticateToken(agencyId);

                if (!string.IsNullOrEmpty(tokenKey))
                {
                    foreach (var item in compReqArray)
                    {
                        result.Add(GetCompareInsuranceProducts(item.companyId, item.productid, item.plan, item.sumInsured, agencyId, tokenKey));
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return result;
        }
        private static string GetCompareInsuranceProducts(int companyid, int productid, string plan, int sumInsured, string agencyId, string tokenKey)
        {
            string requestJson = "{\"companyId\":" + companyid + ", \"productid\":" + productid + ", \"plan\": \"" + plan + "\", \"sumInsured\":" + sumInsured + " }";

            return Post_Utility.PostHealthAPI("POST", Post_Utility.GetCommonUrl("Benefits/GetBenefits"), requestJson, "GetBenefits", agencyId, "", tokenKey);
        }
        #endregion

        #region [Proposal Section]
        public static HealthProposal GetHealthProposalDetail(string enquiryid)
        {
            HealthProposal hProposal = new HealthProposal();
            try
            {
                DataTable dtProposal = ConnectToDataBase.GetHealthProposalDetail(enquiryid);
                if (dtProposal != null && dtProposal.Rows.Count > 0)
                {
                    hProposal.proposalId = Convert.ToInt32(dtProposal.Rows[0]["proposalId"].ToString());
                    hProposal.enquiryid = dtProposal.Rows[0]["enquiryid"].ToString();
                    hProposal.productid = dtProposal.Rows[0]["productid"].ToString();
                    hProposal.companyid = dtProposal.Rows[0]["companyid"].ToString();
                    hProposal.suminsured_amount = dtProposal.Rows[0]["suminsured_amount"].ToString();
                    hProposal.period = dtProposal.Rows[0]["period"].ToString();
                    hProposal.selected_premium = dtProposal.Rows[0]["selected_premium"].ToString();
                    hProposal.p_title = dtProposal.Rows[0]["p_title"].ToString();
                    hProposal.p_gender = dtProposal.Rows[0]["p_gender"].ToString();
                    hProposal.p_firstname = dtProposal.Rows[0]["p_firstname"].ToString();
                    hProposal.p_lastname = dtProposal.Rows[0]["p_lastname"].ToString();
                    hProposal.p_mobile = dtProposal.Rows[0]["p_mobile"].ToString();
                    hProposal.p_dob = dtProposal.Rows[0]["p_dob"].ToString();
                    hProposal.p_email = dtProposal.Rows[0]["p_email"].ToString();
                    hProposal.p_address1 = dtProposal.Rows[0]["p_address1"].ToString();
                    hProposal.p_address2 = dtProposal.Rows[0]["p_address2"].ToString();
                    hProposal.p_landmark = dtProposal.Rows[0]["p_landmark"].ToString();
                    hProposal.p_pincode = dtProposal.Rows[0]["p_pincode"].ToString();
                    hProposal.p_cityname = dtProposal.Rows[0]["p_cityname"].ToString();
                    hProposal.p_statename = dtProposal.Rows[0]["p_statename"].ToString();
                    hProposal.p_pannumber = dtProposal.Rows[0]["p_pannumber"].ToString().ToUpper();
                    hProposal.p_aadharno = dtProposal.Rows[0]["p_aadharno"].ToString();
                    hProposal.p_annualincome = dtProposal.Rows[0]["p_annualincome"].ToString();
                    hProposal.p_isproposerinsured = !string.IsNullOrEmpty(dtProposal.Rows[0]["p_isproposerinsured"].ToString()) ? Convert.ToBoolean(dtProposal.Rows[0]["p_isproposerinsured"].ToString()) : false;
                    hProposal.n_title = dtProposal.Rows[0]["n_title"].ToString();
                    hProposal.n_firstname = dtProposal.Rows[0]["n_firstname"].ToString();
                    hProposal.n_lastname = dtProposal.Rows[0]["n_lastname"].ToString();
                    hProposal.n_dob = dtProposal.Rows[0]["n_dob"].ToString();
                    hProposal.n_age = dtProposal.Rows[0]["n_age"].ToString();
                    hProposal.n_relation = dtProposal.Rows[0]["n_relation"].ToString();
                    hProposal.n_relationid = dtProposal.Rows[0]["n_relationid"].ToString();
                    hProposal.row_premium = dtProposal.Rows[0]["row_premium"].ToString();
                    hProposal.row_basePremium = dtProposal.Rows[0]["row_basePremium"].ToString();
                    hProposal.row_serviceTax = dtProposal.Rows[0]["row_serviceTax"].ToString();
                    hProposal.row_totalPremium = dtProposal.Rows[0]["row_totalPremium"].ToString();
                    hProposal.row_discountPercent = dtProposal.Rows[0]["row_discountPercent"].ToString();
                    hProposal.row_insuranceCompany = dtProposal.Rows[0]["row_insuranceCompany"].ToString();
                    hProposal.row_policyType = dtProposal.Rows[0]["row_policyType"].ToString();
                    hProposal.row_planname = dtProposal.Rows[0]["row_planname"].ToString();
                    hProposal.row_policyTypeName = dtProposal.Rows[0]["row_policyTypeName"].ToString();
                    hProposal.row_policyName = dtProposal.Rows[0]["row_policyName"].ToString();
                    hProposal.policypdflink = dtProposal.Rows[0]["policypdflink"].ToString();
                    hProposal.referenceId = dtProposal.Rows[0]["referenceId"].ToString();
                    hProposal.proposalNumber = dtProposal.Rows[0]["proposalNumber"].ToString();
                    hProposal.policyNumber = dtProposal.Rows[0]["policyNumber"].ToString();
                    hProposal.paymenturl = dtProposal.Rows[0]["paymenturl"].ToString();
                    hProposal.paymentdate = dtProposal.Rows[0]["paymentdate"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return hProposal;
        }
        public static bool InsertBasicPlanDetail(HealthProposal proposal)
        {
            return ConnectToDataBase.InsertUpdateHealthProposal(proposal);
        }
        public static bool InsertPaymentDetail(HealthProposal proposal)
        {
            return ConnectToDataBase.InsertPaymentDetail(proposal);
        }
        public static DataTable GetAnnualIncomeDetails(string enquiryid, string companyid = "")
        {
            return ConnectToDataBase.GetAnnualIncomeDetails(enquiryid, companyid);
        }
        public static DataTable GetRelationShipDetail()
        {
            return ConnectToDataBase.GetRelationShipDetail();
        }
        public static List<HealthInsured> GetInsuredDetails(string enquiryid)
        {
            List<HealthInsured> insuredList = new List<HealthInsured>();
            try
            {
                DataTable dtInsured = ConnectToDataBase.GetInsuredDetails(enquiryid);
                if (dtInsured != null && dtInsured.Rows.Count > 0)
                {
                    for (int i = 0; i < dtInsured.Rows.Count; i++)
                    {
                        HealthInsured insured = new HealthInsured();
                        insured.id = Convert.ToInt32(dtInsured.Rows[i]["id"].ToString());
                        insured.enquiryid = dtInsured.Rows[i]["enquiryid"].ToString();
                        insured.productid = dtInsured.Rows[i]["productid"].ToString();
                        insured.companyid = dtInsured.Rows[i]["companyid"].ToString();
                        insured.relationid = dtInsured.Rows[i]["relationid"].ToString();
                        insured.relationname = dtInsured.Rows[i]["relationname"].ToString();
                        insured.title = dtInsured.Rows[i]["title"].ToString();
                        insured.firstname = dtInsured.Rows[i]["firstname"].ToString();
                        insured.lastname = dtInsured.Rows[i]["lastname"].ToString();
                        insured.dob = dtInsured.Rows[i]["dob"].ToString();
                        insured.height = dtInsured.Rows[i]["height"].ToString();
                        insured.weight = dtInsured.Rows[i]["weight"].ToString();
                        insured.occupationId = dtInsured.Rows[i]["occupationId"].ToString();
                        insured.occupation = dtInsured.Rows[i]["occupation"].ToString();
                        insured.illness = Convert.ToBoolean(dtInsured.Rows[i]["illness"].ToString());
                        insured.illnessdesc = dtInsured.Rows[i]["illnessdesc"].ToString();
                        insured.engageManualLabour = Convert.ToBoolean(dtInsured.Rows[i]["engageManualLabour"].ToString());
                        insured.engageManualLabourDesc = dtInsured.Rows[i]["engageManualLabourDesc"].ToString();
                        insured.engageWinterSports = Convert.ToBoolean(dtInsured.Rows[i]["engageWinterSports"].ToString());
                        insured.engageWinterSportDesc = dtInsured.Rows[i]["engageWinterSportDesc"].ToString();
                        insured.ispropinsured = Convert.ToBoolean(dtInsured.Rows[i]["ispropinsured"].ToString());
                        insured.medicalhistory = dtInsured.Rows[i]["medicalhistory"].ToString();
                        insuredList.Add(insured);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return insuredList;
        }
        public static HealthPayment GetHealthPaymentDetails(string enquiryid)
        {
            HealthPayment payment = new HealthPayment();
            try
            {
                DataTable dtPayment = ConnectToDataBase.GetHealthPaymentDetail(enquiryid);
                if (dtPayment != null && dtPayment.Rows.Count > 0)
                {
                    payment.enquiryid = dtPayment.Rows[0]["enquiryid"].ToString();
                    payment.companyid = Convert.ToInt32(dtPayment.Rows[0]["companyid"].ToString());
                    payment.companyname = dtPayment.Rows[0]["companyname"].ToString();
                    payment.premium = dtPayment.Rows[0]["premium"].ToString();
                    payment.serviceTax = dtPayment.Rows[0]["serviceTax"].ToString();
                    payment.totalPremium = dtPayment.Rows[0]["totalPremium"].ToString();
                    payment.proposalNum = dtPayment.Rows[0]["proposalNum"].ToString();
                    payment.productId = dtPayment.Rows[0]["productId"].ToString();
                    payment.paymenturl = dtPayment.Rows[0]["paymenturl"].ToString();
                    payment.referenceId = dtPayment.Rows[0]["referenceId"].ToString();
                    payment.cc_policyNumber = dtPayment.Rows[0]["cc_policyNumber"].ToString();
                    payment.cc_transactionRefNum = dtPayment.Rows[0]["cc_transactionRefNum"].ToString();
                    payment.cc_uwDecision = dtPayment.Rows[0]["cc_uwDecision"].ToString();
                    payment.cc_errorMsg = dtPayment.Rows[0]["cc_errorMsg"].ToString();
                    payment.status = dtPayment.Rows[0]["status"].ToString();
                    payment.paymentstatus = Convert.ToBoolean(dtPayment.Rows[0]["paymentstatus"].ToString());
                    payment.paymentdate = dtPayment.Rows[0]["paymentdate"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return payment;
        }
        public static DataTable GetNomineeDetails()
        {
            return ConnectToDataBase.GetNomineeDetails();
        }
        public static string GetOccupation(string occupationId = "")
        {
            DataTable occTable = ConnectToDataBase.GetOccupation();

            string occDdlList = string.Empty;

            if (occTable != null && occTable.Rows.Count > 0)
            {
                for (int i = 0; i < occTable.Rows.Count; i++)
                {
                    string occId = occTable.Rows[i]["OccupationId"].ToString();
                    string occupation = occTable.Rows[i]["Occupation"].ToString();
                    occDdlList = occDdlList + "<option value='" + occId + "' " + (occId == occupationId ? "selected" : string.Empty) + ">" + occupation + "</option>";
                }
            }
            return occDdlList;
        }
        public static bool InsertUpdate_InsuredDetail(HealthInsured insured)
        {
            return ConnectToDataBase.InsertUpdate_InsuredDetail(insured);
        }
        public static bool InsertUpdate_NomineeDetail(HealthProposal proposal)
        {
            return ConnectToDataBase.InsertUpdateHealthProposal(proposal);
        }
        public static string GetHealthQuestionaries(HealthProposal proposal, string agencyId)
        {
            string tokenKey = Post_Utility.GetrAuthenticateToken(agencyId);
            if (!string.IsNullOrEmpty(tokenKey))
            {
                string postUrl = Post_Utility.GetCommonUrl("Questionnaire/Questionnaire/" + proposal.companyid + "/" + proposal.productid);
                if (ConnectToDataBase.InsertHealthQuestionary(proposal.enquiryid, postUrl, "request"))
                {
                    string response = Post_Utility.PostHealtApiWithoutBody(postUrl, tokenKey);
                    if (!string.IsNullOrEmpty(response))
                    {
                        if (ConnectToDataBase.InsertHealthQuestionary(proposal.enquiryid, response, "response"))
                        {
                            return response;
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static bool InsertProposalRequestData(string prequest, string enquiryid, string type)
        {
            return ConnectToDataBase.InsertProposalRequestData(prequest, enquiryid, type);
        }
        public static string GenrateCareProposal(string request, string enquiryid, string agencyId)
        {
            string response = string.Empty;
            string tokenKey = Post_Utility.GetrAuthenticateToken(agencyId);
            if (!string.IsNullOrEmpty(tokenKey))
            {
                response = Post_Utility.GenrateCareProposal(request, agencyId, tokenKey);
            }
            return response;
        }
        public static bool UpdateHealthMedicalHistory(List<HealthPerposalQuestion> medhistory)
        {
            bool issuccess = false;

            try
            {
                if (medhistory != null && medhistory.Count > 0)
                {
                    foreach (var item in medhistory)
                    {
                        //string fieldsWithValue = "medicalhistory='" + item.questionjson.Replace("'", "''") + "'";
                        //string whereCondition = "enquiryid='" + item.enquiryid + "' and firstname='" + item.firstname + "' and lastname='" + item.lastnane + "'";

                        issuccess = ConnectToDataBase.UpdateHealthMedicalHistory(item.enquiryid, item.questionjson.Replace("'", "''"), item.firstname, item.lastnane);
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return issuccess;
        }
        public static bool UpdateProposalRespoData(string referenceId, string proposalNum, string enquiryid)
        {
            return ConnectToDataBase.UpdateProposalRespoData(referenceId, proposalNum, enquiryid);
        }
        public static bool UpdateStartEndPolicyDate(string enquiryid,string updateQuery)
        {
            return ConnectToDataBase.UpdateStartEndPolicyDate(enquiryid, updateQuery);
        }
        public static bool UpdatePaymentTabData(string referenceId, string proposalNum, string paymenturl, string premium, string serviceTax, string totalPremium, string enquiryid)
        {
            return ConnectToDataBase.UpdatePaymentTabData(referenceId, proposalNum, paymenturl, premium, serviceTax, totalPremium, enquiryid);
        }
        public static bool UpdateCarePaymentDetails(PaymentModel payment)
        {
            return ConnectToDataBase.UpdateCarePaymentDetails(payment.policyNumber, payment.transactionRefNum, payment.uwDecision, payment.errorFlag, payment.errorMsg, payment.Status, payment.enquiryid);
        }
        public static bool AddLessAddon(string[] addonlist)
        {
            return ConnectToDataBase.AddLessAddon(addonlist);
        }
        public static bool UpdateSelectedAddons(List<AddonChecked> addonlist)
        {
            return ConnectToDataBase.UpdateSelectedAddons(addonlist);
        }
        public static DataTable GetAddLessAddon(string enquiryid, string isselected = "")
        {
            return ConnectToDataBase.GetAddLessAddon(enquiryid, isselected);
        }
        public static List<AddonChecked> GetAddOneList(string enquiryid)
        {
            List<AddonChecked> addonlist = new List<AddonChecked>();
            try
            {
                DataTable dtAddon = GetAddLessAddon(enquiryid, "1");
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    for (int i = 0; i < dtAddon.Rows.Count; i++)
                    {
                        AddonChecked tempAddon = new AddonChecked();
                        tempAddon.addOnsId = dtAddon.Rows[i]["addOnsId"].ToString();
                        tempAddon.addOns = dtAddon.Rows[i]["addOns"].ToString();
                        tempAddon.code = dtAddon.Rows[i]["code"].ToString();
                        tempAddon.value = dtAddon.Rows[i]["value"].ToString();
                        tempAddon.calculation = dtAddon.Rows[i]["calculation"].ToString();
                        tempAddon.addOnValue = dtAddon.Rows[i]["addOnValue"].ToString();
                        tempAddon.inquiryid = dtAddon.Rows[i]["inquiryid"].ToString();
                        addonlist.Add(tempAddon);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return addonlist;
        }
        #endregion

        #region  [HealthProposalSummary Section]

        public static string GenerateHealth_PaymentUrl(string proposalNumber)
        {
            string url, response = string.Empty;
            string tokenKey = Post_Utility.GetrAuthenticateToken(string.Empty);
            if (!string.IsNullOrEmpty(tokenKey))
            {
                response = Post_Utility.PostHealthAPI("GET", Post_Utility.GetCommonUrl("Care/GetPaymentUrl/" + proposalNumber), string.Empty, string.Empty, string.Empty, string.Empty);
            }
            JObject obj = JObject.Parse(response);
            url = obj["response"].ToString();
            return url;
        }
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string pdfurl)
        {
            return ConnectToDataBase.UpdateHealthPolicyPdfLink(enquiryid, pdfurl);
        }
        #endregion

        #region [Set Health Response in Cache]
        private static string _key = "health_respo";
        private static readonly MemoryCache _cache = MemoryCache.Default;
        public static void StoreHealth_RespoInCache(string healthResponse)
        {
            CacheItemPolicy cacheItemPolicy = new CacheItemPolicy();
            cacheItemPolicy.AbsoluteExpiration = DateTime.Now.AddHours(1.0);

            _cache.Add(_key, healthResponse, cacheItemPolicy);
        }
        public static string GetHealth_RespoFromCache()
        {
            if (!_cache.Contains(_key))
            {
                RemoveHealth_RespoFromCache();
                return (string)_cache.Get(_key);
            }

            return _cache.Get(_key) as string;
        }
        public static void RemoveHealth_RespoFromCache()
        {
            if (string.IsNullOrEmpty(_key))
            {
                _cache.Dispose();
            }
            else
            {
                _cache.Remove(_key);
            }
        }
        #endregion    
        public static DataTable GetAddLessAddon(string enquiryid, string addOnsId, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID, string planAmount, string planPeriod)
        {
            return ConnectToDataBase.GetAddLessAddon(enquiryid, addOnsId, addOns, code, value, calculation, addOnValue, productDetailID, planAmount, planPeriod);
        }
    }
}