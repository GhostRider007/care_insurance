﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Care_Project.Methods.Helper;
using Care_Project.Methods.Utility;
using Care_Project.Models;

namespace Care_Project.Methods.Service
{
    public static class Administrator_Service
    {
        public static List<string> Agency_Login(string userid, string password)
        {
            List<string> result = new List<string>();
            try
            {
                string msg = string.Empty;
                DataTable dtAgency = Administrator_Helper.Agency_Login(userid, password);
                if (dtAgency != null && dtAgency.Rows.Count > 0)
                {
                    if (dtAgency.Columns.Contains("AgencyId"))
                    {
                        result.Add("success");
                        InitializeAgencySession(dtAgency);
                    }
                    else if (dtAgency.Columns.Contains("UID"))
                    {
                        result.Add("failed");
                        result.Add("Your UserID Seems to be Incorrect!");
                    }
                    else if (dtAgency.Columns.Contains("PWD"))
                    {
                        result.Add("failed");
                        result.Add("Your Password Seems to be Incorrect!");
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add("failed");
                result.Add(ex.Message);
            }
            return result;
        }
        private static void InitializeAgencySession(DataTable dtAgency, string type = "")
        {
            if (dtAgency != null && dtAgency.Rows.Count > 0)
            {
                Agency olu = new Agency();
                if (type == "auto")
                {
                    olu.UID = !string.IsNullOrEmpty(dtAgency.Rows[0]["User_Id"].ToString()) ? dtAgency.Rows[0]["User_Id"].ToString() : string.Empty;
                    olu.Password = !string.IsNullOrEmpty(dtAgency.Rows[0]["PWD"].ToString()) ? dtAgency.Rows[0]["PWD"].ToString() : string.Empty;
                    olu.UserType = !string.IsNullOrEmpty(dtAgency.Rows[0]["Status"].ToString()) ? dtAgency.Rows[0]["Status"].ToString() : string.Empty;
                    olu.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agency_Name"].ToString()) ? dtAgency.Rows[0]["Agency_Name"].ToString() : string.Empty;
                    olu.Agent_Type = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_Type"].ToString()) ? dtAgency.Rows[0]["Agent_Type"].ToString() : string.Empty;
                    olu.AgencyId = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyId"].ToString()) ? dtAgency.Rows[0]["AgencyId"].ToString() : string.Empty;
                    olu.Crd_Limit = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Limit"].ToString()) ? dtAgency.Rows[0]["Crd_Limit"].ToString() : string.Empty;
                }
                else
                {
                    olu.UID = !string.IsNullOrEmpty(dtAgency.Rows[0]["UID"].ToString()) ? dtAgency.Rows[0]["UID"].ToString() : string.Empty;
                    olu.Password = !string.IsNullOrEmpty(dtAgency.Rows[0]["Password"].ToString()) ? dtAgency.Rows[0]["Password"].ToString() : string.Empty;
                    olu.UserType = !string.IsNullOrEmpty(dtAgency.Rows[0]["UserType"].ToString()) ? dtAgency.Rows[0]["UserType"].ToString() : string.Empty;
                    olu.TypeID = !string.IsNullOrEmpty(dtAgency.Rows[0]["TypeID"].ToString()) ? dtAgency.Rows[0]["TypeID"].ToString() : string.Empty;
                    olu.AgencyName = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyName"].ToString()) ? dtAgency.Rows[0]["AgencyName"].ToString() : string.Empty;
                    olu.Name = !string.IsNullOrEmpty(dtAgency.Rows[0]["Name"].ToString()) ? dtAgency.Rows[0]["Name"].ToString() : string.Empty;
                    olu.IsCorp = !string.IsNullOrEmpty(dtAgency.Rows[0]["IsCorp"].ToString()) ? dtAgency.Rows[0]["IsCorp"].ToString() : string.Empty;
                    olu.DistrId = !string.IsNullOrEmpty(dtAgency.Rows[0]["DistrId"].ToString()) ? dtAgency.Rows[0]["DistrId"].ToString() : string.Empty;
                    olu.Agent_Type = !string.IsNullOrEmpty(dtAgency.Rows[0]["Agent_Type"].ToString()) ? dtAgency.Rows[0]["Agent_Type"].ToString() : string.Empty;
                    olu.AgencyId = !string.IsNullOrEmpty(dtAgency.Rows[0]["AgencyId"].ToString()) ? dtAgency.Rows[0]["AgencyId"].ToString() : string.Empty;
                    olu.Crd_Limit = !string.IsNullOrEmpty(dtAgency.Rows[0]["Crd_Limit"].ToString()) ? dtAgency.Rows[0]["Crd_Limit"].ToString() : string.Empty;
                }

                List<Agency> lu = new List<Agency>();
                lu.Add(olu);
                HttpContext.Current.Session["agency"] = lu;
            }
        }
        public static bool IsAgencyLogin(ref Agency lu)
        {
            List<Agency> objLoginUser = new List<Agency>();
            bool idsuccess = IsAgencyLoginSuccess(ref objLoginUser);
            if (objLoginUser != null && objLoginUser.Count > 0)
            {
                lu = objLoginUser[0];
            }
            return idsuccess;
        }
        public static bool IsAgencyLoginSuccess(ref List<Agency> loginUserList)
        {
            bool value = false;
            try
            {
                if (HttpContext.Current.Session["agency"] != null)
                {
                    loginUserList = (List<Agency>)HttpContext.Current.Session["agency"];
                    value = true;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return value;
        }
        public static bool LogoutAgency()
        {
            HttpContext.Current.Session.Remove("agency");
            return true;
        }
        public static List<string> GetAgencyById(string agencyid)
        {
            List<string> result = new List<string>();
            try
            {
                LogoutAgency();
                string dec_agencyid = UtilityClass.Decrypt(agencyid);
                if (!string.IsNullOrEmpty(dec_agencyid))
                {
                    DataTable dtAgency = Administrator_Helper.Get_AgencyDetail(dec_agencyid);
                    if (dtAgency != null && dtAgency.Rows.Count > 0)
                    {
                        string userid = !string.IsNullOrEmpty(dtAgency.Rows[0]["User_Id"].ToString()) ? dtAgency.Rows[0]["User_Id"].ToString() : string.Empty;
                        string password = !string.IsNullOrEmpty(dtAgency.Rows[0]["PWD"].ToString()) ? dtAgency.Rows[0]["PWD"].ToString() : string.Empty;
                        result = Agency_Login(userid, password);
                    }
                }
            }
            catch (Exception ex)
            {
                result.Add(ex.Message);
            }
            return result;
        }
    }
}