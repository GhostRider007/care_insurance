﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using Care_Project.Methods.CommonClass;
using Care_Project.Methods.Helper;
using Care_Project.Methods.Utility;
using Care_Project.Models;
using Care_Project.Models.Health;
using Newtonsoft.Json;
using static Care_Project.Models.Health.HealthComparisonModel;
using static Care_Project.Models.Health.HealthModel;

namespace Care_Project.Methods.Service
{
    public class Health_Service
    {
        #region[HealthRejistration]
        public static string EncodeGenrateEnquiryId()
        {
            //return UtilityClass.ApiBase64Encode("RWT" + UtilityClass.GenrateEnquiryId(5, "1234567890")).ToLower();
            return "RWT" + UtilityClass.GenrateEnquiryId(5, "1234567890").ToLower();
        }
        public static string DecodeGenrateEnquiryId(string enquiryid)
        {
            return UtilityClass.ApiBase64Decode(enquiryid);
        }
        public static List<SumInsuredMaster> GetSumInsuredPriceList()
        {
            return Health_Helper.GetSumInsuredPriceList();
        }
        public static string CheckPinCodeExist(string pincode)
        {
            string strPin = string.Empty;
            PinCode_Master pincodeDetail = Health_Helper.CheckPinCodeExist(pincode);
            if (pincodeDetail != null && !string.IsNullOrEmpty(pincodeDetail.PINCODE))
            {
                strPin = pincodeDetail.CITYCODE;
            }
            return strPin;
        }
        public static bool SaveQuoteDetails(HealthEnquiry quote)
        {
            Agency lu = new Agency();
            if (Administrator_Service.IsAgencyLogin(ref lu))
            {
                quote.AgencyName = lu.AgencyName;
                quote.AgencyId = lu.UID;
            }
            return Health_Helper.SaveQuoteDetails(quote);
        }
        public static bool InsertSearchUrlParaDetail(string enquiryid, string urlpara)
        {
            bool issuccess = false;
            if (Health_Helper.InsertSearchUrlParaDetail(enquiryid, urlpara))
            {
                string[] spliturlpara = urlpara.Split('&');
                string product = spliturlpara[1].Split('=')[1].Trim().ToLower();
                string companyid = spliturlpara[2].Split('=')[1].Trim().ToLower();
                string suminsuredid = spliturlpara[3].Split('=')[1].Trim().ToLower();
                string suminsured = spliturlpara[4].Split('=')[1].Trim().ToLower();
                string adult = spliturlpara[5].Split('=')[1].Trim().ToLower();
                string child = spliturlpara[6].Split('=')[1].Trim().ToLower();
                string plan = spliturlpara[7].Split('=')[1].Trim().ToLower();
                string totalamt = spliturlpara[8].Split('=')[1].Trim().ToLower();
                string tenure = spliturlpara[9].Split('=')[1].Trim().ToLower();

                string helth_Response = Health_Helper.GetHealth_RespoFromCache();
                if (!string.IsNullOrEmpty(helth_Response))
                {
                    StringBuilder sbJson = new StringBuilder();

                    HealthSearchList hRespo = JsonConvert.DeserializeObject<HealthSearchList>(helth_Response);
                    foreach (var item in hRespo.response.data)
                    {
                        if (item.product != null)
                        {
                            string productId = item.product.productDetailId > 0 ? item.product.productDetailId.ToString() : string.Empty;
                            if (productId == product && companyid == item.companyId.ToString() && suminsuredid == item.suminsuredId.ToString() && suminsured == item.sumInsuredValue.ToString() && plan == item.plan && totalamt == item.totalPremium && tenure == item.tenure.ToString())
                            {
                                sbJson.Append("[");
                                sbJson.Append("{");
                                sbJson.Append("\"productDetailID\": " + item.productDetailID + ",\"companyName\": \"" + item.companyName + "\",");
                                sbJson.Append("\"companyId\": " + item.companyId + ",\"productCode\": " + item.productCode + ",");
                                sbJson.Append("\"productName\": \"" + item.productName + "\",\"gstPercenatge\": " + item.gstPercenatge + ",");
                                sbJson.Append("\"period\": \"" + item.period + "\",\"tenure\": " + item.period + ",");
                                sbJson.Append("\"plan\": \"" + item.plan + "\",\"discountPercent\": " + item.discountPercent + ",\"basePremium\": \"" + item.basePremium + "\",");
                                sbJson.Append("\"policytype\": \"" + item.policytype + "\",\"premium\": \"" + item.premium + "\",\"schemaId\": " + item.schemaId + ",");
                                sbJson.Append("\"schemeId\": " + item.schemeId + ",\"serviceTax\": \"" + item.serviceTax + "\",\"totalPremium\": \"" + item.totalPremium + "\",\"inquiry_id\": \"" + item.inquiry_id + "\",");

                                sbJson.Append("\"product\": {");
                                sbJson.Append("\"policyTypeName\": \"" + item.product.policyTypeName + "\",\"policyName\": \"" + item.product.policyName + "\",");
                                sbJson.Append("\"period\": \"" + item.product.period + "\",");
                                if (item.product != null)
                                {
                                    sbJson.Append("\"insureds\": [");
                                    string insureds = string.Empty;
                                    foreach (var insd in item.product.insureds)
                                    {
                                        insureds = !string.IsNullOrEmpty(insureds) ? (insureds + ",{\"dob\": " + insd.dob + ",\"buyBackPED\": " + insd.buyBackPED + "}") : "{\"dob\": " + insd.dob + ",\"buyBackPED\": " + insd.buyBackPED + "}";
                                    }
                                    sbJson.Append(insureds);
                                    sbJson.Append("],");
                                }
                                sbJson.Append("\"productDetailId\": " + item.productDetailID + "");
                                sbJson.Append("},");

                                sbJson.Append("\"addons\": [");
                                if (item.addons != null)
                                {
                                    int loopcount = 1;
                                    foreach (var addon in item.addons)
                                    {
                                        if (loopcount == item.addons.Count)
                                        {
                                            sbJson.Append("{\"addOns\": \"" + addon.addOns.Trim() + "\",\"value\": \"" + addon.value.Trim() + "\"}");
                                        }
                                        else
                                        {
                                            sbJson.Append("{\"addOns\": \"" + addon.addOns.Trim() + "\",\"value\": \"" + addon.value.Trim() + "\"},");
                                        }
                                        loopcount = loopcount + 1;
                                    }
                                }
                                sbJson.Append("],");

                                sbJson.Append("\"ageBracket\": \"" + item.ageBracket + "\",\"scheme\": " + item.scheme + ",\"sumInsuredValue\": " + item.sumInsuredValue + ",");
                                sbJson.Append("\"suminsuredId\": " + item.suminsuredId + ",\"sumInsuredCode\": " + item.sumInsuredCode + ",\"logoUrl\": \"" + item.logoUrl + "\"");
                                sbJson.Append("}");
                                sbJson.Append("]");
                                break;
                            }
                        }
                    }

                    if (Health_Helper.InsertSelectedPlanDetail(enquiryid, sbJson.ToString()))
                    {
                        issuccess = true;
                    }
                }
            }
            return issuccess;
        }
        #endregion

        #region [Health Listing Section]

        public static void RemoveHealth_RespoFromCache()
        {
            Health_Helper.RemoveHealth_RespoFromCache();
        }
        public static List<string> GetHealthApiResponse(string enquiryid, string agencyId)
        {
            List<string> result = new List<string>();
            try
            {
                HealthEnquiry health = new HealthEnquiry();
                health = Health_Helper.GetHealthEnquiryDetail(enquiryid);
                if (health != null && !string.IsNullOrEmpty(health.Enquiry_Id))
                {
                    List<string> adultChild = AdultChildCount(health);
                    string requestJson = GenrateHealthRequestRowData(health);
                    string response = Health_Helper.GetHealthApiResponse(agencyId, health.Enquiry_Id, requestJson);
                    //result = MakeSearchListDynmic(response, adultChild);

                    HealthSearchList hRespo = JsonConvert.DeserializeObject<HealthSearchList>(response);
                    result = MakeListingDataDynamic(hRespo, adultChild);

                    if (result.Count > 0)
                    {
                        string filterside = "<p style='line-height: 20px;;'>Policy Type : <b><label id='lblPolicyType'>" + health.Policy_Type_Text + "</label></b></p>"
                            + "<p style='line-height: 20px;;'>Pincode  : <b><label id='lblPinCode'>" + health.Pin_Code + "</label></b></p>"
                            + "<p style='line-height: 20px;;'>Product Count  : <b><label id='lblProductCount' style='color:#e71820;'>" + result[5] + " Records</label></b></p>";
                        result.Add(filterside);
                    }
                    else
                    {
                        string filterside = "<p style='line-height: 20px;;'>Policy Type : <b><label id='lblPolicyType'>" + health.Policy_Type_Text + "</label></b></p>"
                               + "<p style='line-height: 20px;;'>Pincode  : <b><label id='lblPinCode'>" + health.Pin_Code + "</label></b></p>"
                               + "<p style='line-height: 20px;;'>Product Count  : <b><label id='lblProductCount' style='color:#e71820;'>0 Records</label></b></p>";
                        result.Add(filterside);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }

        public static List<string> GetFilterHealthApiResponse(FilterSection filter)
        {
            List<string> result = new List<string>();
            try
            {
                string response = string.Empty;
                HealthEnquiry health = new HealthEnquiry();
                health = Health_Helper.GetHealthEnquiryDetail(filter.EnquiryId);
                if (health != null && !string.IsNullOrEmpty(health.Enquiry_Id))
                {
                    List<string> adultChild = AdultChildCount(health);
                    string requestJson = GenrateHealthRequestRowData(health);
                    response = Health_Helper.GetHealthApiResponse(string.Empty, health.Enquiry_Id, requestJson);
                    if (!string.IsNullOrEmpty(response))
                    {
                        HealthSearchList hRespo = JsonConvert.DeserializeObject<HealthSearchList>(response);

                        bool isFilter = false;
                        List<Health_Datum> dataList = hRespo.response.data;

                        //if (filter.MinPrice > 0)
                        //{
                        //    isFilter = true;
                        //    dataList = dataList.Where(p => Convert.ToDecimal(p.totalPremium) >= Convert.ToDecimal(filter.MinPrice)).ToList();
                        //}
                        //if (filter.MaxPrice > 0)
                        //{
                        //    isFilter = true;
                        //    dataList = dataList.Where(p => Convert.ToDecimal(p.totalPremium) <= Convert.ToDecimal(filter.MaxPrice)).ToList();
                        //}
                        if (filter.MinPrice > 0 && filter.MaxPrice > 0)
                        {
                            isFilter = true;
                            dataList = dataList.Where(p => Convert.ToDecimal(p.totalPremium) >= Convert.ToDecimal(filter.MinPrice) && Convert.ToDecimal(p.totalPremium) <= Convert.ToDecimal(filter.MaxPrice)).ToList();
                        }
                        if (!string.IsNullOrEmpty(filter.Tenure))
                        {
                            isFilter = true;
                            dataList = dataList.Where(p => p.product.period.ToString().Contains(filter.Tenure)).ToList();
                        }
                        if (!string.IsNullOrEmpty(filter.SumInsured))
                        {
                            isFilter = true;
                            dataList = dataList.Where(p => p.sumInsuredValue.ToString().Contains(filter.SumInsured)).ToList();
                        }

                        if (isFilter)
                        {
                            hRespo.response.data = null;
                            hRespo.response.data = dataList;
                        }

                        result = MakeListingDataDynamic(hRespo, adultChild);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        private static List<string> AdultChildCount(HealthEnquiry health)
        {
            List<string> ac_count = new List<string>();

            int adult = 0; int child = 0;
            if (health != null && !string.IsNullOrEmpty(health.Enquiry_Id))
            {
                if (health.Self) { adult = adult + 1; }
                if (health.Spouse) { adult = adult + 1; }
                if (health.Father) { adult = adult + 1; }
                if (health.Mother) { adult = adult + 1; }
                if (health.Son)
                {
                    if (Convert.ToInt32(health.Son1Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Son2Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Son3Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Son4Age) > 0) { child = child + 1; }
                }
                if (health.Daughter)
                {
                    if (Convert.ToInt32(health.Daughter1Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Daughter2Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Daughter3Age) > 0) { child = child + 1; }
                    if (Convert.ToInt32(health.Daughter4Age) > 0) { child = child + 1; }
                }
                ac_count.Add(adult.ToString());
                ac_count.Add(child.ToString());
            }

            return ac_count;
        }
        //private static List<string> MakeSearchListDynmic(string response, List<string> adultChild)
        //{
        //    List<string> result = new List<string>();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(response))
        //        {
        //            HealthSearchList hRespo = JsonConvert.DeserializeObject<HealthSearchList>(response);
        //            result = MakeListingDataDynamic(hRespo, adultChild);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ex.ToString();
        //    }
        //    return result;
        //}
        private static List<string> MakeListingDataDynamic(HealthSearchList hRespo, List<string> adultChild)
        {
            List<string> result = new List<string>();

            try
            {
                StringBuilder sbResult = new StringBuilder();
                if (hRespo != null && hRespo.success == true)
                {
                    List<decimal> priceOrder = new List<decimal>();
                    List<object> tenureCount = new List<object>();
                    List<object> sumInsuCount = new List<object>();
                    List<object> providerCount = new List<object>();

                    int totalcount = 1;
                    if (hRespo.response.data.Count > 0)
                    {
                        foreach (var item in hRespo.response.data)
                        {
                            sbResult.Append("<div class='col-sm-12 listbox' id='listitem_" + totalcount + "'>");
                            sbResult.Append("<div class='row'>");
                            //sbResult.Append("<span class='label label-large arrowed-in-right my_badge'> <i></i> COVID-19 Covered</span>");
                            sbResult.Append("<div class='col-sm-2' style='border-right: 1px solid #ccc;'>");
                            sbResult.Append("<img src='/Content/images/care.jpg' class='img-responsive img-center margin_top_15 company_logo' style='margin-top:2px;width: 100%;'>");
                            sbResult.Append("<p class='text-center'>Care</p>");
                            sbResult.Append("</div>");
                            sbResult.Append("<div class='col-sm-8'>");
                            sbResult.Append("<div class='row' style='border-bottom: 1px solid #ccc;padding: 3px;'>");
                            sbResult.Append("<div class='col-sm-4'>");
                            sbResult.Append("<div class='row'>");
                            sbResult.Append("<div class='col-sm-2'><p><span class='fa fa-home fonticon'></span></p></div>");
                            sbResult.Append("<div class='col-sm-10'><p class='fontdetails'>Room Rent</p>");
                            //sbResult.Append("<p class='fontdetails'> Shared Accommodation</p>");
                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                            sbResult.Append("</div></div>");
                            sbResult.Append("</div>");
                            sbResult.Append("<div class='col-sm-4'>");
                            sbResult.Append("<div class='row'>");
                            sbResult.Append("<div class='col-sm-2'><p><span class='fa fa-heartbeat fonticon '></span></p></div>");
                            sbResult.Append("<div class='col-sm-10'><p class='fontdetails'>Pre-Existing Disease</p>");
                            //sbResult.Append("<p class='fontdetails'>  48 months</p>");
                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                            sbResult.Append("</div></div>");
                            sbResult.Append("</div>");
                            sbResult.Append("<div class='col-sm-4'>");
                            sbResult.Append("<div class='row'>");
                            sbResult.Append("<div class='col-sm-2'><p><span class='fa fa-hospital fonticon'></span></p></div>");
                            sbResult.Append("<div class='col-sm-10'><p class='fontdetails'>Network Hospitals</p>");
                            //sbResult.Append("<p class='fontdetails'>  4000+</p>");
                            sbResult.Append("<p class='fontdetails text-danger'> Not Available</p>");
                            sbResult.Append("</div></div>");
                            sbResult.Append("</div>");
                            sbResult.Append("</div>");
                            sbResult.Append("<div class='row' style='margin-top:20px;'>");
                            //sbResult.Append("<div class='col-sm-3'></div>"); //<a href='' data-toggle='modal' data-target='#productDetailshealth' style='font-size:17px;color:green'><span class='fa fa-life-ring'></span>&nbsp; View Features</a>
                            sbResult.Append("<div class='col-sm-12 text-center'>" + item.product.policyName + "</div>");
                            //sbResult.Append("<div class='col-sm-5'>");

                            //sbResult.Append("<h6 class='checkbox-inline' style=' padding: 1px 17px 5px; width: 70%;float:right'><input type='checkbox' class='submenuclass healthInsuranceProducts' data-identity='hip" + totalcount + "' id='compareCheckBox_"+ totalcount + "' onchange='CompareCheck(this," + totalcount + ")'  name='compareCheckBox_" + totalcount + "'>&nbsp;Add to compare<span class='hidden' id='compData" + totalcount + "' data-sum_insured='" + item.sumInsuredValue + "' data-plan='" + item.plan + "' data-companyid='" + item.companyId + "' data-productid='" + item.product.productDetailId + "'></span></h6>");//span class='checkmark'></span>

                            //sbResult.Append("</div>");
                            sbResult.Append("</div>");
                            sbResult.Append("</div>");

                            sbResult.Append("<div class='col-sm-2' style='text-transform: capitalize;'>");
                            sbResult.Append("<div class='row' style='border-left:1px solid #ccc'>");

                            //string finalAmount = Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString();
                            string finalAmount = item.totalPremium;
                            priceOrder.Add(!string.IsNullOrEmpty(finalAmount) ? Math.Ceiling(Convert.ToDecimal(finalAmount)) : 0);
                            if (!tenureCount.Contains(item.tenure)) { tenureCount.Add(item.tenure); }
                            if (!sumInsuCount.Contains(Convert.ToDecimal(item.sumInsuredValue))) { sumInsuCount.Add(Convert.ToDecimal(item.sumInsuredValue)); }

                            string INRAmount = UtilityClass.IndianMoneyFormat(finalAmount.Replace("INR", ""));
                            sbResult.Append("<div class='col-sm-12'><p style='text-align: center;margin-top: -6px;font-size: 12px;font-weight: bold;'> Cover:&nbsp;<span id='coverCount" + totalcount + "'><span class='fa fa-rupee'></span>" + UtilityClass.IndianMoneyFormat(item.sumInsuredValue.ToString()) + " / " + item.tenure + " Year</span></p><button class='btn theme-btn btn-hover btnselectedplan' style='cursor: pointer;margin-left: 27px;' id='btnPremiumPlan" + totalcount + "' data-thiscount='" + totalcount + "' data-suminsuredid='" + item.suminsuredId + "' data-productid='" + item.productDetailID + "' data-suminsured='" + item.sumInsuredValue + "' data-totalamt='" + item.totalPremium + "' data-planname='" + item.plan + "' data-tenure='" + item.tenure + "' data-companyid='" + item.companyId + "' data-adult='" + adultChild[0] + "' data-child='" + adultChild[1] + "' data-inramount='" + INRAmount + "'>" + INRAmount + "</button><p style='text-align: center;font-size: 12px;'>All Inclusive</p>" + GetPerDayAmount(item.totalPremium, item.tenure) + "</div>");
                            sbResult.Append("</div>");
                            sbResult.Append("</div>");
                            sbResult.Append("</div>");
                            sbResult.Append("</div>");
                            totalcount = totalcount + 1;
                        }

                        result.Add(sbResult.ToString());

                        priceOrder.Sort();
                        result.Add((Math.Ceiling(priceOrder[0]) - 100).ToString());
                        result.Add((Math.Ceiling(priceOrder[priceOrder.Count() - 1]) + 100).ToString());

                        tenureCount.Sort();
                        result.Add(tenureCount[0] + "-" + tenureCount[tenureCount.Count() - 1]);

                        sumInsuCount.Sort();
                        string sumInsuStr = "<option value=''>Select</option>";
                        for (int s = 0; s < sumInsuCount.Count(); s++)
                        {
                            sumInsuStr += "<option value='" + sumInsuCount[s].ToString() + "'>" + UtilityClass.IndianMoneyFormat(sumInsuCount[s].ToString()) + " </option>";
                        }
                        result.Add(sumInsuStr);
                        result.Add((totalcount - 1).ToString());
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        private static string GetPerDayAmount(string totalPremium, int tenure)
        {
            if (!string.IsNullOrEmpty(totalPremium))
            {
                decimal amount = Convert.ToDecimal(totalPremium);
                if (amount > 0)
                {
                    int totalDaysInAYear = DateTime.IsLeapYear(DateTime.Now.Year) ? 366 : 365;
                    string perDayAmt = (Math.Ceiling((Math.Ceiling(amount / totalDaysInAYear)) / tenure)).ToString();
                    return "<p style='text-align:center;font-weight: bold;font-size: 14px;'><span class='fa fa-rupee'></span>&nbsp;" + perDayAmt + " / Day</p>";
                }
            }

            return string.Empty;
        }
        private static string GenrateHealthRequestRowData(HealthEnquiry enqDetail)
        {
            StringBuilder requestJson = new StringBuilder();

            try
            {
                requestJson.Append("{\"inquiry_id\": \"" + enqDetail.Enquiry_Id + "\",");
                requestJson.Append("\"selfAge\":" + enqDetail.SelfAge);
                requestJson.Append(",\"spouseAge\":" + enqDetail.SpouseAge);

                if (enqDetail.Son)
                {
                    requestJson.Append(",\"sonAge\":[");
                    if (enqDetail.Son1Age > 0)
                    {
                        requestJson.Append(enqDetail.Son1Age);
                    }
                    if (enqDetail.Son2Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son2Age);
                    }
                    if (enqDetail.Son3Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son3Age);
                    }
                    if (enqDetail.Son4Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Son4Age);
                    }
                    requestJson.Append("]");
                }
                else
                {
                    requestJson.Append(",\"sonAge\":[");
                    requestJson.Append(enqDetail.Son1Age);
                    requestJson.Append("]");
                }
                if (enqDetail.Daughter)
                {
                    requestJson.Append(",\"daughterAge\":[");
                    if (enqDetail.Daughter1Age > 0)
                    {
                        requestJson.Append(enqDetail.Daughter1Age);
                    }
                    if (enqDetail.Daughter2Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter2Age);
                    }
                    if (enqDetail.Daughter3Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter3Age);
                    }
                    if (enqDetail.Daughter4Age > 0)
                    {
                        requestJson.Append("," + enqDetail.Daughter4Age);
                    }
                    requestJson.Append("]");
                }
                else
                {
                    requestJson.Append(",\"daughterAge\":[");
                    requestJson.Append(enqDetail.Daughter1Age);
                    requestJson.Append("]");
                }
                requestJson.Append(",\"fatherAge\":" + enqDetail.FatherAge);
                requestJson.Append(",\"motherAge\":" + enqDetail.MotherAge);
                requestJson.Append(",\"suminsured\":\"" + enqDetail.Sum_Insured + "\"");
                requestJson.Append(",\"policytype\":\"" + enqDetail.Policy_Type.ToUpper() + "\"");
                requestJson.Append(",\"pincode\":" + enqDetail.Pin_Code);
                requestJson.Append("}");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return requestJson.ToString();
        }
        public static string CompareInsuranceProducts(List<CompareRequest> compReqArray, string enqId, string agencyId)
        {
            string strResult = string.Empty;
            try
            {
                StringBuilder sbStr = new StringBuilder();
                List<string> result = Health_Helper.CompareInsuranceProducts(compReqArray, enqId, agencyId);

                if (result != null && result.Count > 0)
                {
                    sbStr.Append("<div class='row'>");
                    sbStr.Append("<div class='col-sm-12 no-padding'>");
                    sbStr.Append("<div class='table-responsive'>");
                    sbStr.Append("<table class='table no-bottom-space text-center compare_table'>");
                    sbStr.Append("<tbody>");

                    List<BenefitsRoot> respoList = new List<BenefitsRoot>();
                    foreach (var item in result)
                    {
                        BenefitsRoot respo = JsonConvert.DeserializeObject<BenefitsRoot>(item);
                        respoList.Add(respo);
                    }
                    int respoCount = respoList.Count;
                    int CategoryCount = 0;

                    sbStr.Append("<tr>");
                    sbStr.Append("<td style='width: 200px;'> <span id='img_quote' style='cursor:pointer' class='img_quote'> <label><i class='fa fa-envelope' aria-hidden='true'></i></label> <span img_id='#health_product_comparison' id='btn_Email' page='Mediclaim Product Comparison' class='btn_send_quote_with_img'>Email</span> </span> </br><span class='mailform hidden'><input type='text' class='form-control' id='mailaddress' placeholder='email id..' style='height: 30px; '><span class='btn btn-danger btn-sm sendbtn' id='compareemai' style='margin-top: 15px;'>Send mail</span> <span class='btn btn-danger btn-sm temsend hidden' style='margin-top: 15px;'> Sending mail.. <i class='fa fa-spinner fa-pulse'></i></span></span> </td>");
                    for (int i = 0; i < respoCount; i++)
                    {
                        sbStr.Append("<td class='compare_" + (i + 1) + "'> <img src='/Content/images/care.jpg' class='img-responsive img-center img-thumbnail compare_" + (i + 1) + "_product_image' height='80' width='80' alt=''> <h5 class='no-margin padding-top-20 compare_" + i + "_product_name'>" + (!string.IsNullOrEmpty(respoList[i].response.productName) ? (respoList[i].response.productName + (!string.IsNullOrEmpty(respoList[i].response.plan) ? " - " + respoList[i].response.plan : string.Empty)) : string.Empty) + "</h5><h6>Cover : " + compReqArray[i].cover + "</h6><h6>Premium : " + compReqArray[i].premium + "/-</h6></td>");
                    }
                    sbStr.Append("</tr>");

                    List<List<string>> FullMenuAndSubMenuList = new List<List<string>>();

                    for (int i = 0; i < respoCount; i++)
                    {
                        if (respoList[i].message.ToLower() == "success")
                        {
                            CategoryCount = respoList[i].response.categories.Count;
                            for (int j = 0; j < respoList[i].response.categories.Count; j++)
                            {
                                int MenuAndSubMenuCount = respoList[i].response.categories[j].menus.Count + 1;
                                List<string> MenuAndSubMenuList = new List<string>();
                                MenuAndSubMenuList.Add(respoList[i].response.categories[j].categoryName);
                                for (int k = 0; k < MenuAndSubMenuCount - 1; k++)
                                {
                                    MenuAndSubMenuList.Add(respoList[i].response.categories[j].menus[k].menuName);
                                }
                                FullMenuAndSubMenuList.Add(MenuAndSubMenuList);
                            }
                            break;
                        }
                    }

                    for (int i = 0; i < CategoryCount; i++)
                    {
                        int rCount = 0;
                        sbStr.Append("<tr>");
                        sbStr.Append("<td colspan='4' class='compare_feature_heading font-bold' style='background-color: #1f77b4;color: #fff;'>" + FullMenuAndSubMenuList[i][0] + "</td>");
                        sbStr.Append("</tr>");
                        for (int j = 1; j < FullMenuAndSubMenuList[i].Count; j++)
                        {
                            sbStr.Append("<tr>");

                            sbStr.Append("<td>" + FullMenuAndSubMenuList[i][j] + "</td>");

                            for (int k = 0; k < respoCount; k++)
                            {
                                sbStr.Append("<td title='" + respoList[rCount].response.categories[i].menus[j - 1].destriptions + "'>" + respoList[k].response.categories[i].menus[j - 1].details + "</td>");
                            }
                            sbStr.Append("</tr>");
                        }
                        sbStr.Append("</tr>");
                        rCount++;
                    }

                    sbStr.Append("</tr>");



                    sbStr.Append("</tbody>");
                    sbStr.Append("</table>");
                    sbStr.Append("</div>");
                    sbStr.Append("</div>");
                    sbStr.Append("</div>");

                    strResult = sbStr.ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
                strResult = "<h3>Couldnot Fetch Benefits</h6>";
                strResult = strResult + "<br/>" + "<p>Error : " + ex.ToString() + "</p>";
            }

            return strResult;
        }
        #endregion

        #region [Proposal Section]
        public static HealthProposal GetHealthProposalDetail(string enquiryid)
        {
            return Health_Helper.GetHealthProposalDetail(enquiryid);
        }
        public static List<string> GetSelectedPlanDetails(string enquiryid)
        {
            List<string> result = new List<string>();
            HealthEnquiry health = new HealthEnquiry();
            try
            {
                health = Health_Helper.GetHealthEnquiryDetail(enquiryid);
                if (!string.IsNullOrEmpty(health.selectedproductjson))
                {
                    List<string> adultChild = AdultChildCount(health);

                    string[] spliturlpara = health.searchurl.Split('&');
                    string product = spliturlpara[1].Split('=')[1].Trim().ToLower();
                    string companyid = spliturlpara[2].Split('=')[1].Trim().ToLower();
                    string suminsuredid = spliturlpara[3].Split('=')[1].Trim().ToLower();
                    string suminsured = spliturlpara[4].Split('=')[1].Trim().ToLower();
                    string adult = spliturlpara[5].Split('=')[1].Trim().ToLower();
                    string child = spliturlpara[6].Split('=')[1].Trim().ToLower();
                    string plan = spliturlpara[7].Split('=')[1].Trim().ToLower();
                    string totalamt = spliturlpara[8].Split('=')[1].Trim().ToLower();
                    string tenure = spliturlpara[9].Split('=')[1].Trim().ToLower();

                    List<Health_Datum> hData = JsonConvert.DeserializeObject<List<Health_Datum>>(health.selectedproductjson);
                    //Bind Select Plan
                    result.Add(BindSelectPlanHtml(hData));
                    result.Add(BindSelectAddonHtml(hData));

                    foreach (var item in hData)
                    {
                        string productId = item.product.productDetailId > 0 ? item.product.productDetailId.ToString() : string.Empty;
                        if (productId == product && companyid == item.companyId.ToString() && suminsuredid == item.suminsuredId.ToString() && suminsured == item.sumInsuredValue.ToString() && plan == item.plan && totalamt == item.totalPremium && tenure == item.tenure.ToString())
                        {
                            result.Add(BindProduct_FareBreakUpHtml(item, adultChild));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        private static string BindSelectAddonHtml(List<Health_Datum> hData)
        {
            StringBuilder strAddon = new StringBuilder();

            if (hData != null)
            {
                Health_Datum single_hData = hData[0];
                List<Addon> lstAddon = single_hData.addons;
                if (lstAddon != null && lstAddon.Count > 0)
                {
                    int chkaddonid = 1;
                    //lstAddon = lstAddon.Where(p => !BlockAddon.Contains(p.value)).ToList();
                    foreach (var addon in lstAddon)
                    {
                        strAddon.Append(AddonsHtml(0, chkaddonid, addon.addOns.Trim(), addon.value.Trim(), "", "", "", single_hData.productDetailID.ToString(), single_hData.inquiry_id, single_hData.basePremium, single_hData.premium, single_hData.discountPercent, single_hData.serviceTax, single_hData.totalPremium, single_hData.tenure));
                        chkaddonid = chkaddonid + 1;
                    }
                }
            }

            return strAddon.ToString();
        }

        private static List<string> BlockAddon = new List<string>(new List<string> { "OPDEXP8002", "PAH8053" });
        private static string AddonsHtml(int addOnsId, int chkaddonid, string addOns, string code, string value, string calculation, string addOnValue, string productDetailID, string inquiryid, string basePremium, string premium, double discountPercent, string serviceTax, string totalPremium, int period)
        {
            StringBuilder sbStr = new StringBuilder();

            try
            {
                string showaddOnValue = !string.IsNullOrEmpty(addOnValue) ? addOnValue : "0";

                bool isSelected = false;
                DataTable dtAddon = Health_Helper.GetAddLessAddon(inquiryid, addOnsId.ToString(), addOns, code, value, calculation, addOnValue, productDetailID, totalPremium, period.ToString());
                if (dtAddon != null && dtAddon.Rows.Count > 0)
                {
                    isSelected = true;
                }

                if (value.Trim() == "percentage")
                {
                    showaddOnValue = ((Convert.ToDecimal(basePremium) * Convert.ToDecimal(addOnValue)) / 100).ToString();
                }

                sbStr.Append("<div class='col-sm-3'>");
                sbStr.Append("<h6 class='checkbox-inline' style='margin-top: 8px;'>");
                sbStr.Append("<label class='checkboxcust' style='font-weight: bold !important;color: #455757 !important;'>");

                if (isSelected)
                {
                    sbStr.Append("<input style='width: 20px;height: 18px;' type='checkbox' class='submenuclass chkaddon chkAddon_Year" + period + "' id='chkAddon_" + chkaddonid + "' name='chkAddon_" + code + "' data-addonsid='" + addOnsId + "' data-addons='" + addOns + "' data-code='" + code + "' data-value='" + value + "' data-calculation='" + calculation.Trim() + "' data-addonvalue='" + addOnValue + "' data-productdetailid='" + productDetailID + "' data-inquiryid='" + inquiryid + "' data-basepremium='" + basePremium + "' data-premium='" + premium + "' data-discountpercent='" + discountPercent.ToString() + "' data-serviceTax='" + serviceTax + "' data-totalpremium='" + totalPremium + "' data-period='" + period + "' data-chkaddonid='" + chkaddonid + "' checked='checked' onclick='AddLessAddon(\"" + chkaddonid + "\");' />");

                }
                else
                {
                    sbStr.Append("<input style='width: 20px;height: 18px;' type='checkbox' class='submenuclass chkaddon chkAddon_Year" + period + "' id='chkAddon_" + chkaddonid + "' name='chkAddon_" + code + "' data-addonsid='" + addOnsId + "' data-addons='" + addOns + "' data-code='" + code + "' data-value='" + value + "' data-calculation='" + calculation.Trim() + "' data-addonvalue='" + addOnValue + "' data-productdetailid='" + productDetailID + "' data-inquiryid='" + inquiryid + "' data-basepremium='" + basePremium + "' data-premium='" + premium + "' data-discountpercent='" + discountPercent.ToString() + "' data-serviceTax='" + serviceTax + "' data-totalpremium='" + totalPremium + "' data-period='" + period + "' data-chkaddonid='" + chkaddonid + "' onclick='AddLessAddon(\"" + chkaddonid + "\");' />");
                }
                sbStr.Append("&nbsp;<span style='vertical-align: super;'>" + addOns.Trim() + "</span>");
                //sbStr.Append("<p style='margin-left: 30px; margin-top: 1px; font-weight: 100; color: #8a8a8a;'>" + UtilityClass.IndianMoneyFormat(showaddOnValue) + "</p>");
                sbStr.Append("</h6></div>");
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return sbStr.ToString();
        }
        private static string BindSelectPlanHtml(List<Health_Datum> hData)
        {
            StringBuilder sbPlan = new StringBuilder();
            bool isChecked = false;

            foreach (var item in hData)
            {
                sbPlan.Append("<div class='col-sm-4'>");
                sbPlan.Append("<div class='panel panel-default text-center rounded' style='border: 1px solid #ddd;'>");

                sbPlan.Append("<div class='panel-heading' style='background:#e3e3e3;padding: 10px;'>");
                sbPlan.Append("<h3 class='panel-title' style='margin-bottom: 0px; font-size: 19px;'>" + item.tenure + " Year Premium</h3>");
                sbPlan.Append("<p class='no-margin-bottom color-purple font-bold'>Discount : " + item.discountPercent + "%</p>");
                sbPlan.Append("</div>");

                sbPlan.Append("<div class='panel-body no-padding'>");
                sbPlan.Append("<h3 class='color-orange'>");
                //string finalAmount = Math.Ceiling(Convert.ToDecimal(item.totalPremium)).ToString();
                string INRAmount = UtilityClass.IndianMoneyFormat(item.totalPremium.Replace("INR", ""));
                if (item.tenure == 1 && isChecked == false)
                {
                    sbPlan.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + INRAmount + "' data-basepremium='" + item.premium + "' data-premium='" + item.totalPremium + "' data-periodfor='" + item.tenure + "' data-servicetax='" + item.serviceTax + "' data-discount='" + item.discountPercent + "' checked='checked'><br><strong>" + INRAmount + "</strong>");
                    isChecked = true;
                }
                else
                {
                    sbPlan.Append("<input type='radio' name='term' class='help_trigger chkpremiumamt' value='" + INRAmount + "' data-basepremium='" + item.premium + "' data-premium='" + item.totalPremium + "' data-periodfor='" + item.tenure + "' data-servicetax='" + item.serviceTax + "' data-discount='" + item.discountPercent + "'><br><strong>" + INRAmount + "</strong>");
                }
                sbPlan.Append("</h3>");
                sbPlan.Append("</div>");

                sbPlan.Append("</div>");
                sbPlan.Append("</div>");
            }

            return sbPlan.ToString();
        }
        private static string BindProduct_FareBreakUpHtml(Health_Datum item, List<string> adultChild, string premium = "", string serviceTax = "", string totalPremium = "", bool isaddon = false)
        {
            StringBuilder sbPFBrakeUp = new StringBuilder();

            sbPFBrakeUp.Append("<div class='col-sm-12'>");
            sbPFBrakeUp.Append("<img src='/Content/images/care.jpg' style='margin-left: auto;margin-right: auto;display: block;width: 60%;'>");
            sbPFBrakeUp.Append("</div>");
            sbPFBrakeUp.Append("<div class='col-sm-12' style='margin-top: 10%;'>");

            sbPFBrakeUp.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Product  Name</label></div><div class='col-sm-6'><label>" + item.productName + "</label></div>");
            sbPFBrakeUp.Append("</div>");

            sbPFBrakeUp.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Plan Type</label></div><div class='col-sm-6'><label>" + item.plan + "</label></div>");
            sbPFBrakeUp.Append("</div>");

            sbPFBrakeUp.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Policy Type</label></div><div class='col-sm-6'><label>" + item.policytype + "</label></div>");
            sbPFBrakeUp.Append("</div>");

            sbPFBrakeUp.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Member</label></div><div class='col-sm-6'><label>Adult- " + adultChild[0] + ", Child- " + adultChild[1] + "</label></div>");
            sbPFBrakeUp.Append("</div>");

            sbPFBrakeUp.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>SumInsred</label></div><div class='col-sm-6'><label>" + UtilityClass.IndianMoneyFormat(item.sumInsuredValue.ToString().Replace("INR", "")) + "</label></div>");
            sbPFBrakeUp.Append("</div>");

            sbPFBrakeUp.Append("<div class='col-sm-12' style='margin-top: 2%;margin-bottom: 2%;font-size: 20px;text-align: center;color: #09a9e5;'>");
            sbPFBrakeUp.Append("<b class='text-center'>Premium Breakup</b></div>");

            sbPFBrakeUp.Append("<div id='divPremiumBreakup' class='row'>");

            sbPFBrakeUp.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='row'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Basic</label></div>");
            sbPFBrakeUp.Append("<div class='col-sm-6' style='text-align: right;'><label>" + (!string.IsNullOrEmpty(premium) ? UtilityClass.IndianMoneyFormat(premium) : UtilityClass.IndianMoneyFormat(item.basePremium.Replace("INR", ""))) + "</label></div>");
            sbPFBrakeUp.Append("</div></div>");

            sbPFBrakeUp.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='row'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>Discount (" + (item.discountPercent > 0 ? item.discountPercent.ToString() : "0") + " %)</label></div>");
            sbPFBrakeUp.Append("<div class='col-sm-6' style='text-align: right;'><label>₹ 0.00</label></div>");
            sbPFBrakeUp.Append("</div></div>");

            sbPFBrakeUp.Append("<div class='col-sm-12' style='border-bottom: 1px solid #ccc;line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='row'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>GST " + item.gstPercenatge + "%</label></div>");
            sbPFBrakeUp.Append("<div class='col-sm-6' style='text-align: right;'><label>" + (!string.IsNullOrEmpty(serviceTax) ? UtilityClass.IndianMoneyFormat(serviceTax) : UtilityClass.IndianMoneyFormat(item.serviceTax.Replace("INR", ""))) + "</label></div>");
            sbPFBrakeUp.Append("</div></div>");

            sbPFBrakeUp.Append("<div class='col-sm-12' style='line-height: 1.5rem;'>");
            sbPFBrakeUp.Append("<div class='row'>");
            sbPFBrakeUp.Append("<div class='col-sm-6'><label>You'll Pay</label></div>");
            sbPFBrakeUp.Append("<div class='col-sm-6' style='text-align: right;'><label>" + (!string.IsNullOrEmpty(totalPremium) ? UtilityClass.IndianMoneyFormat(totalPremium) : UtilityClass.IndianMoneyFormat(item.totalPremium.Replace("INR", ""))) + "</label></div>");
            sbPFBrakeUp.Append("</div></div>");

            if (isaddon)
            {
                sbPFBrakeUp.Append("<div class='col-sm-12' style='line-height: 1.5rem;'>");
                sbPFBrakeUp.Append("<div class='row'>");
                sbPFBrakeUp.Append("<div class='col-sm-12'><label class='text-success' style='font-size: 11px;font-style: italic;'>* The above amount is exclusive of any addons you have selected.</label></div>");
                sbPFBrakeUp.Append("</div></div>");
            }

            sbPFBrakeUp.Append("</div></div>");

            return sbPFBrakeUp.ToString();
        } // USED IN HEALTHPROPOSALSUMMARY SECTION TOO
        public static HealthProposal InsertSelectedPlanDetail(HealthProposal proposal, List<AddonChecked> addonlist)
        {
            HealthProposal hProposal = new HealthProposal();
            try
            {
                HealthEnquiry health = new HealthEnquiry();
                health = Health_Helper.GetHealthEnquiryDetail(proposal.enquiryid);
                if (!string.IsNullOrEmpty(health.selectedproductjson))
                {
                    //List<string> adultChild = AdultChildCount(health);

                    string[] spliturlpara = health.searchurl.Split('&');
                    string product = spliturlpara[1].Split('=')[1].Trim().ToLower();
                    string companyid = spliturlpara[2].Split('=')[1].Trim().ToLower();
                    string suminsuredid = spliturlpara[3].Split('=')[1].Trim().ToLower();
                    string suminsured = spliturlpara[4].Split('=')[1].Trim().ToLower();
                    string adult = spliturlpara[5].Split('=')[1].Trim().ToLower();
                    string child = spliturlpara[6].Split('=')[1].Trim().ToLower();
                    string plan = spliturlpara[7].Split('=')[1].Trim().ToLower();
                    string totalamt = spliturlpara[8].Split('=')[1].Trim().ToLower();
                    string tenure = spliturlpara[9].Split('=')[1].Trim().ToLower();

                    proposal.productid = product;
                    proposal.companyid = companyid;
                    proposal.suminsured_amount = suminsured;

                    List<Health_Datum> hData = JsonConvert.DeserializeObject<List<Health_Datum>>(health.selectedproductjson);
                    foreach (var item in hData)
                    {
                        string productId = item.product.productDetailId > 0 ? item.product.productDetailId.ToString() : string.Empty;
                        if (productId == product && companyid == item.companyId.ToString() && suminsuredid == item.suminsuredId.ToString() && suminsured == item.sumInsuredValue.ToString() && plan == item.plan && totalamt == item.totalPremium && tenure == item.tenure.ToString())
                        {
                            proposal.row_premium = item.premium;
                            proposal.row_basePremium = item.basePremium;
                            proposal.row_serviceTax = item.serviceTax;
                            proposal.row_totalPremium = item.totalPremium;
                            proposal.row_discountPercent = item.discountPercent.ToString();
                            proposal.row_insuranceCompany = item.companyName;
                            proposal.row_policyType = item.policytype;
                            proposal.row_planname = item.plan;
                            proposal.row_policyTypeName = item.product.policyTypeName;
                            proposal.row_policyName = item.product.policyName;
                            proposal.actionType = "baseplan";
                            if (Health_Helper.InsertBasicPlanDetail(proposal))
                            {
                                bool isAddonUpdated = Health_Service.UpdateSelectedAddons(addonlist);

                                if (Health_Helper.InsertPaymentDetail(proposal))
                                {
                                    hProposal = GetHealthProposalDetail(proposal.enquiryid);
                                    if (string.IsNullOrEmpty(hProposal.p_pincode))
                                    {
                                        hProposal.p_pincode = health.Pin_Code;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return hProposal;
        }
        public static string GetAnnualIncomeDetails(string enquiryid)
        {
            string strIncome = string.Empty;

            DataTable dtAnnualIncome = Health_Helper.GetAnnualIncomeDetails(enquiryid);
            if (dtAnnualIncome != null && dtAnnualIncome.Rows.Count > 0)
            {
                strIncome = "<option value=''>Select Annual Income</option>";
                for (int i = 0; i < dtAnnualIncome.Rows.Count; i++)
                {
                    strIncome = strIncome + "<option value='" + dtAnnualIncome.Rows[i]["ValueCode"].ToString() + "'>" + dtAnnualIncome.Rows[i]["ValueAmount"].ToString() + "</option>";
                }
            }

            return strIncome;
        }
        public static string InsertProposerDetails(HealthProposal proposal)
        {
            string strResult = string.Empty;
            try
            {
                proposal.actionType = "proposal";
                if (Health_Helper.InsertBasicPlanDetail(proposal))
                {
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return strResult;
        }
        public static List<string> GetInsuredDetails(HealthProposal proposal)
        {
            List<string> result = new List<string>();
            try
            {
                HealthEnquiry health = Health_Helper.GetHealthEnquiryDetail(proposal.enquiryid);
                HealthProposal hProposal = GetHealthProposalDetail(proposal.enquiryid);
                List<HealthInsured> insuredList = Health_Helper.GetInsuredDetails(proposal.enquiryid);
                List<string> adultChild = AdultChildCount(health);
                result = BindInsuredHtml(health, hProposal, insuredList, adultChild);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        private static List<string> BindInsuredHtml(HealthEnquiry health, HealthProposal hProposal, List<HealthInsured> insuredList, List<string> adultChild)
        {
            List<string> result = new List<string>();
            try
            {
                int total_Adults = Convert.ToInt32(adultChild[0]);
                int total_Childs = Convert.ToInt32(adultChild[1]);
                int totalPerson = Convert.ToInt32(adultChild[0]) + Convert.ToInt32(adultChild[1]);
                StringBuilder sbStr = new StringBuilder();
                DataTable dtRelation = Health_Helper.GetRelationShipDetail();

                sbStr.Append("<div class='col-sm-12'>");
                sbStr.Append("<h5>Insured Details</h6 >");////<h6>Insured Details</ h6 >
                int i = 1;
                for (i = 1; i <= total_Adults; i++)
                {
                    HealthInsured tempInsured = new HealthInsured();
                    if (insuredList != null && insuredList.Count > 0)
                    {
                        tempInsured = insuredList[i - 1];
                    }

                    sbStr.Append("<div class='row form-group' " + (totalPerson > 1 ? "style='padding:10px; border: 1px dotted #ccc;border-radius: 5px;'" : "style='padding:10px;'") + ">");
                    if (!string.IsNullOrEmpty(tempInsured.id.ToString()))
                    {
                        sbStr.Append("<input type='hidden' id='hdnInsuredId_" + i + "' value='" + tempInsured.id + "' />");
                    }
                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6'><div class='row'>");
                    sbStr.Append("<div class='col-sm-7 form-validation'><select class='form-control formcontlbord' name='ddlRelation_" + i + "' id='ddlRelation_" + i + "' onchange='InsuredTitleChange(" + i + ");'>");
                    sbStr.Append("<option value=''>Relation</option>");
                    for (int r = 0; r < dtRelation.Rows.Count; r++)
                    {
                        if (tempInsured.id > 0)
                        {
                            sbStr.Append("<option value='" + dtRelation.Rows[r]["RelationshipId"].ToString() + "'" + (tempInsured.relationid == dtRelation.Rows[r]["RelationshipId"].ToString() ? "selected" : "") + ">" + dtRelation.Rows[r]["RelationshipName"].ToString() + "</option>");
                        }
                        else
                        {
                            if (hProposal.p_isproposerinsured && i == 1)
                            {
                                if (dtRelation.Rows[r]["RelationshipName"].ToString().ToLower().Contains("self"))
                                {
                                    sbStr.Append("<option value='" + dtRelation.Rows[r]["RelationshipId"].ToString() + "'" + (dtRelation.Rows[r]["RelationshipName"].ToString().ToLower().Contains("self") ? "selected" : "") + " > " + dtRelation.Rows[r]["RelationshipName"].ToString() + " </ option > ");
                                }
                                else
                                {
                                    sbStr.Append("<option value='" + dtRelation.Rows[r]["RelationshipId"].ToString() + "'>" + dtRelation.Rows[r]["RelationshipName"].ToString() + "</option>");
                                }
                            }
                            else
                            {
                                sbStr.Append("<option value='" + dtRelation.Rows[r]["RelationshipId"].ToString() + "'>" + dtRelation.Rows[r]["RelationshipName"].ToString() + "</option>");
                            }
                        }

                        //sbStr.Append("<option value='" + item.RelationshipId + "'>" + item.RelationshipName + "</option>");
                    }
                    sbStr.Append("</select></div>");
                    sbStr.Append("<div class='col-sm-5 form-validation'><select name='ddlTitle_" + i + "' id='ddlTitle_" + i + "' class='form-control formcontlbord'><option value=''>Title</option>");

                    if (hProposal.p_isproposerinsured && i == 1)
                    {
                        if (!string.IsNullOrEmpty(tempInsured.title))
                        {
                            sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                        }
                        else
                        {
                            sbStr.Append("<option value='Mr.' " + (hProposal.p_title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (hProposal.p_title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (hProposal.p_title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");

                            //sbStr.Append("<option value='Mr.' selected='selected'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tempInsured.title))
                        {
                            sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                        }
                        else
                        {
                            sbStr.Append("<option value='Mr.'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                        }
                    }

                    sbStr.Append("</select></div>");
                    sbStr.Append("</div></div>");

                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6 form-validation'>");

                    if (hProposal.p_isproposerinsured)
                    {
                        if (i == 1)
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name'  value='" + hProposal.p_firstname + "' onkeypress='return IsAlphabet(event)' />");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.firstname))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' value='" + tempInsured.firstname + "' onkeypress='return IsAlphabet(event)'/>");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' />");
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tempInsured.firstname))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' value='" + tempInsured.firstname + "' onkeypress='return IsAlphabet(event)'/>");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + i + "' placeholder='first name' onkeypress='return IsAlphabet(event)'/>");
                        }
                    }

                    sbStr.Append("</div><div class='col-sm-6 form-validation'>");

                    if (hProposal.p_isproposerinsured)
                    {
                        if (i == 1)
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' value='" + hProposal.p_lastname + "' onkeypress='return IsAlphabet(event)'/>");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.firstname))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name'  value='" + tempInsured.lastname + "' onkeypress='return IsAlphabet(event)'/>");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)'/>");
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tempInsured.firstname))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name'  value='" + tempInsured.lastname + "' onkeypress='return IsAlphabet(event)'/>");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + i + "' placeholder='last name' onkeypress='return IsAlphabet(event)' />");
                        }
                    }

                    sbStr.Append("</div></div></div></div></div>");

                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-4 form-validation'>");

                    if (hProposal.p_isproposerinsured)
                    {
                        if (i == 1)
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + hProposal.p_dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(tempInsured.dob))
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                            }
                            else
                            {
                                sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(tempInsured.dob))
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        }
                        else
                        {
                            sbStr.Append("<input type='text' class='form-control formcontlbord DynamicCommanDateWithYr' id='txtInDOB_" + i + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + i + "\",\"notndobvalid" + i + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notndobvalid" + i + "'></span></div>");
                        }
                    }
                    if (!string.IsNullOrEmpty(tempInsured.height))
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberOnlyKeyNoDotNoSpace(event);' onkeyup='return CheckHeight(" + i + ")' type='text' class='form-control formcontlbord' id='txtInHeight_" + i + "' placeholder='height (cm)' maxlength='3' minlength='1' value='" + tempInsured.height + "' /><span class='cmposion'>CM</span><span class='notheightvalid" + i + "'></span></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberOnlyKeyNoDotNoSpace(event);' onkeyup='return CheckHeight(" + i + ")'  type='text' class='form-control formcontlbord' id='txtInHeight_" + i + "' placeholder='height (cm)' maxlength='3' minlength='1' /><span class='cmposion'>CM</span><span class='notheightvalid" + i + "'></span></div>");
                    }

                    if (!string.IsNullOrEmpty(tempInsured.weight))
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberOnlyKeyNoDotNoSpace(event);' onkeyup='return CheckWeight(" + i + ")' name='txtInWeight_" + i + "' id='txtInWeight_" + i + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' value='" + tempInsured.weight + "'><span class='cmposion'>KG</span><span class='notweightvalid" + i + "'></span></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberOnlyKeyNoDotNoSpace(event);' onkeyup='return CheckWeight(" + i + ")' name='txtInWeight_" + i + "' id='txtInWeight_" + i + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3'><span class='cmposion'>KG</span><span class='notweightvalid" + i + "'></span></div>");
                    }

                    sbStr.Append("</div></div>");

                    sbStr.Append("<div class='col-sm-12'>");
                    sbStr.Append("<div class='row' style='margin-top: 12px;'>");
                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlOccupation_" + i + "' id='ddlOccupation_" + i + "'>");
                    sbStr.Append("<option value=''>Select Occupation</option>");

                    string occupationStr = Health_Helper.GetOccupation(insuredList.Count > 0 ? insuredList[i - 1].occupationId : string.Empty);
                    sbStr.Append(occupationStr);

                    sbStr.Append("</select></div>");

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlIllness_" + i + "' id='ddlIllness_" + i + "' onchange='return FillExistingIllness(" + i + ");'>");
                    sbStr.Append("<option value=''>Existing Illness</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[i - 1].illness == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[i - 1].illness == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");

                    if (insuredList.Count > 0 && insuredList[i - 1].illness == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation ExistingIllness_" + i + "'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + i + "' placeholder='Describe Existing Illness' value='" + insuredList[i - 1].illnessdesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow ExistingIllness_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + i + "' placeholder='Describe Existing Illness' onkeypress='return IsAlphabet(event)'></div>");
                    }

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageManualLabour_" + i + "' id='ddlEngageManualLabour_" + i + "' onchange='return FillEngageManualLabour(" + i + ");'>");
                    sbStr.Append("<option value=''>Is Engage Manual Labour</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[i - 1].engageManualLabour == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[i - 1].engageManualLabour == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");

                    if (insuredList.Count > 0 && insuredList[i - 1].engageManualLabour == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation EngageManualLabour_" + i + "'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour' value='" + insuredList[i - 1].engageManualLabourDesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow EngageManualLabour_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour' onkeypress='return IsAlphabet(event)'></div>");
                    }

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageWinterSports_" + i + "' id='ddlEngageWinterSports_" + i + "' onchange='return FillEngageWinterSports(" + i + ");'>");
                    sbStr.Append("<option value=''>Is Engage Winter Sports</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[i - 1].engageWinterSports == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[i - 1].engageWinterSports == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");
                    if (insuredList.Count > 0 && insuredList[i - 1].engageWinterSports == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation EngageWinterSports_" + i + "'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' value='" + insuredList[i - 1].engageWinterSportDesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow EngageWinterSports_" + i + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    sbStr.Append("</div></div>");

                    sbStr.Append("</div>");
                }

                for (int j = i; j <= totalPerson; j++)
                {
                    HealthInsured tempInsured = new HealthInsured();
                    if (insuredList != null && insuredList.Count > 0)
                    {
                        tempInsured = insuredList[j - 1];
                    }

                    sbStr.Append("<div class='row form-group' " + (totalPerson > 1 ? "style='padding:10px; border: 1px dotted #ccc;border-radius: 5px;'" : "style='padding:10px;'") + ">");
                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6'><div class='row'>");
                    sbStr.Append("<div class='col-sm-7 form-validation'><select class='form-control formcontlbord' name='ddlRelation_" + j + "' id='ddlRelation_" + j + "' onchange='InsuredTitleChange(" + j + ");'>");
                    sbStr.Append("<option value=''>Relation</option>");
                    for (int rl = 0; rl < dtRelation.Rows.Count; rl++)
                    {
                        if (tempInsured.id > 0)
                        {
                            sbStr.Append("<option value='" + dtRelation.Rows[rl]["RelationshipId"].ToString() + "'" + (tempInsured.relationid == dtRelation.Rows[rl]["RelationshipId"].ToString() ? "selected" : "") + ">" + dtRelation.Rows[rl]["RelationshipName"].ToString() + "</option>");
                        }
                        else
                        {
                            sbStr.Append("<option value='" + dtRelation.Rows[rl]["RelationshipId"].ToString() + "'>" + dtRelation.Rows[rl]["RelationshipName"].ToString() + "</option>");
                        }
                    }
                    sbStr.Append("</select></div>");
                    sbStr.Append("<div class='col-sm-5 form-validation'><select name='ddlTitle_" + j + "' id='ddlTitle_" + j + "' class='form-control formcontlbord'><option value=''>Title</option>");

                    if (!string.IsNullOrEmpty(tempInsured.title))
                    {
                        sbStr.Append("<option value='Mr.' " + (tempInsured.title.ToLower() == "mr." ? "selected" : string.Empty) + ">Mr.</option><option value='Mrs.' " + (tempInsured.title.ToLower() == "mrs." ? "selected" : string.Empty) + ">Mrs.</option></option><option value='Ms.' " + (tempInsured.title.ToLower() == "ms." ? "selected" : string.Empty) + ">Ms.</option>");
                    }
                    else
                    {
                        sbStr.Append("<option value='Mr.'>Mr.</option><option value='Mrs.'>Mrs.</option></option><option value='Ms.'>Ms.</option>");
                    }

                    sbStr.Append("</select></div>");
                    sbStr.Append("</div></div>");

                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-6 form-validation'>");

                    if (!string.IsNullOrEmpty(tempInsured.firstname))
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + j + "' placeholder='first name' value='" + tempInsured.firstname + "' onkeypress='return IsAlphabet(event)' />");
                    }
                    else
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInFirstName_" + j + "' placeholder='first name' onkeypress='return IsAlphabet(event)' />");
                    }

                    sbStr.Append("</div><div class='col-sm-6 form-validation'>");
                    if (!string.IsNullOrEmpty(tempInsured.lastname))
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + j + "' placeholder='last name'  value='" + tempInsured.lastname + "' onkeypress='return IsAlphabet(event)' />");
                    }
                    else
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord' id='txtInLastName_" + j + "' placeholder='last name' onkeypress='return IsAlphabet(event)' />");
                    }

                    sbStr.Append("</div></div></div></div></div>");

                    sbStr.Append("<div class='col-sm-6'><div class='row'><div class='col-sm-4 form-validation'>");
                    if (!string.IsNullOrEmpty(tempInsured.dob))
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord DynamicChildCommanDateWithYr' id='txtInDOB_" + j + "' readonly autocomplete='off' placeholder='Date Of Birth' value='" + tempInsured.dob + "' onchange='return CalculateNomineeAge(\"txtInDOB_" + j + "\",\"notchilddobvalid" + j + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notchilddobvalid" + j + "'></span></div>");
                    }
                    else
                    {
                        sbStr.Append("<input type='text' class='form-control formcontlbord DynamicChildCommanDateWithYr' id='txtInDOB_" + j + "' readonly autocomplete='off' placeholder='Date Of Birth' onchange='return CalculateNomineeAge(\"txtInDOB_" + j + "\",\"notchilddobvalid" + j + "\",\"position: absolute;top: 37px;right: 15px;font-size: 10px;\")' /><span class='notchilddobvalid" + j + "'></span></div>");
                    }

                    if (!string.IsNullOrEmpty(tempInsured.height))
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' type='text' class='form-control formcontlbord' id='txtInHeight_" + j + "' placeholder='height (cm)' maxlength='3' minlength='1' value='" + tempInsured.height + "' /><span class='cmposion'>CM</span></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' type='text' class='form-control formcontlbord' id='txtInHeight_" + j + "' placeholder='height (cm)' maxlength='3' minlength='1' /><span class='cmposion'>CM</span></div>");
                    }

                    if (!string.IsNullOrEmpty(tempInsured.weight))
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' name='txtInWeight_" + j + "' id='txtInWeight_" + j + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3' value='" + tempInsured.weight + "'><span class='cmposion'>KG</span></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-4 form-validation'><input onkeypress='return isNumberValidationPrevent(event);' name='txtInWeight_" + j + "' id='txtInWeight_" + j + "' type='text' class='form-control formcontlbord' minlength='1' placeholder='weight (kg)' maxlength='3'><span class='cmposion'>KG</span></div>");
                    }

                    sbStr.Append("</div></div>");

                    sbStr.Append("<div class='col-sm-12'>");
                    sbStr.Append("<div class='row' style='margin-top: 12px;'>");
                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlOccupation_" + j + "' id='ddlOccupation_" + j + "'>");
                    sbStr.Append("<option value=''>Select Occupation</option>");

                    string occupationStr = Health_Helper.GetOccupation(insuredList.Count > 0 ? insuredList[j - 1].occupationId : string.Empty);
                    sbStr.Append(occupationStr);

                    sbStr.Append("</select></div>");

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlIllness_" + j + "' id='ddlIllness_" + j + "' onchange='return FillExistingIllness(" + j + ");'>");
                    sbStr.Append("<option value=''>Existing Illness</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[j - 1].illness == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[j - 1].illness == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");

                    if (insuredList.Count > 0 && insuredList[j - 1].illness == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation ExistingIllness_" + j + "'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + j + "' placeholder='Describe Existing Illness' value='" + insuredList[j - 1].illnessdesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow ExistingIllness_" + j + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtExistingIllness_" + j + "' placeholder='Describe Existing Illness' onkeypress='return IsAlphabet(event)'></div>");
                    }

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageManualLabour_" + j + "' id='ddlEngageManualLabour_" + j + "' onchange='return FillExistingIllness(" + j + ");'>");
                    sbStr.Append("<option value=''>Is Engage Manual Labour</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[j - 1].engageManualLabour == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[j - 1].engageManualLabour == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");

                    if (insuredList.Count > 0 && insuredList[j - 1].engageManualLabour == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation EngageManualLabour_" + j + "'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + i + "' placeholder='Describe Engage Manual Labour' value='" + insuredList[j - 1].engageManualLabourDesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow EngageManualLabour_" + j + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageManualLabour_" + j + "' placeholder='Describe Engage Manual Labour' onkeypress='return IsAlphabet(event)'></div>");
                    }

                    sbStr.Append("<div class='col-sm-3 form-validation'>");
                    sbStr.Append("<select class='form-control formcontlbord' name='ddlEngageWinterSports_" + j + "' id='ddlEngageWinterSports_" + j + "' onchange='return FillExistingIllness(" + j + ");'>");
                    sbStr.Append("<option value=''>Is Engage Winter Sports</option>");
                    sbStr.Append("<option value='true' " + (insuredList.Count > 0 ? (insuredList[j - 1].engageWinterSports == true ? "selected" : string.Empty) : string.Empty) + ">Yes</option><option value='false' " + (insuredList.Count > 0 ? (insuredList[j - 1].engageWinterSports == false ? "selected" : string.Empty) : string.Empty) + ">No</option>");
                    sbStr.Append("</select></div>");
                    if (insuredList.Count > 0 && insuredList[j - 1].engageWinterSports == true)
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation EngageWinterSports_" + j + "'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' value='" + insuredList[j - 1].engageWinterSportDesc + "' onkeypress='return IsAlphabet(event)'></div>");
                    }
                    else
                    {
                        sbStr.Append("<div class='col-sm-6 form-validation notshow EngageWinterSports_" + j + "' style='display:none;'><input type='text' class='form-control formcontlbord' id='txtEngageWinterSports_" + i + "' placeholder='Describe Engage Winter Sports' onkeypress='return IsAlphabet(event)'></div>");
                    }

                    sbStr.Append("</div></div>");

                    sbStr.Append("</div>");
                }

                sbStr.Append("</div>");

                //Nominee Section
                sbStr.Append("<div class=\"col-sm-12\" style=\"border-top: 1px solid #ccc;\">");
                sbStr.Append("<div class=\"row form-group\" style=\"padding:10px;\">");

                sbStr.Append("<div class=\"col-sm-12\"><h6>Nominee Details</h6></div>");
                sbStr.Append("<div class=\"col-sm-3 form-validation\">");
                sbStr.Append("<select class=\"form-control formcontlbord\" name=\"ddlnRelation\" id=\"ddlnRelation\">");

                DataTable dtNominee = Health_Helper.GetNomineeDetails();
                sbStr.Append("<option value = \"\" >Relation </option >");
                if (dtNominee != null && dtNominee.Rows.Count > 0)
                {
                    for (int n = 0; n < dtNominee.Rows.Count; n++)
                    {
                        sbStr.Append("<option value = \"" + dtNominee.Rows[n]["RelationshipCode"].ToString() + "\" " + (hProposal.n_relation == "" + dtNominee.Rows[n]["RelationshipCode"].ToString() + "" ? "selected" : "") + " data-title='" + dtNominee.Rows[n]["Title"].ToString().Trim() + "'> " + dtNominee.Rows[n]["Relationship"].ToString() + " </option >");
                    }
                    //sbStr.Append("<option value = \"Mother\" " + (proposalDetail.nRelation == "Mother" ? "selected" : "") + "> Mother </option >");
                    //sbStr.Append("<option value = \"Brother\" " + (proposalDetail.nRelation == "Brother" ? "selected" : "") + "> Brother </option >");
                    //sbStr.Append("<option value = \"Sister\" " + (proposalDetail.nRelation == "Sister" ? "selected" : "") + "> Sister </option >");
                    //sbStr.Append("<option value = \"Grandfather\" " + (proposalDetail.nRelation == "Grandfather" ? "selected" : "") + "> Grandfather </option >");
                    //sbStr.Append("<option value = \"GrandMother\" " + (proposalDetail.nRelation == "GrandMother" ? "selected" : "") + "> GrandMother </option >");
                }
                sbStr.Append("</select >");
                //sbStr.Append("</select >");
                sbStr.Append("</div>");
                sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                sbStr.Append("<select name = \"ddlnTitle\" id=\"ddlnTitle\" class=\"form-control formcontlbord\">");
                sbStr.Append("<option value =''>Title</option>");
                sbStr.Append("<option value = \"Mr.\" " + (hProposal.n_title.ToLower() == "mr." ? "selected" : "") + ">Mr.</option>");
                sbStr.Append("<option value = \"Mrs.\" " + (hProposal.n_title.ToLower() == "mrs." ? "selected" : "") + "> Mrs.</option>");
                sbStr.Append("<option value=\"Ms.\" " + (hProposal.n_title.ToLower() == "ms." ? "selected" : "") + ">Ms.</option>");
                sbStr.Append("</select>");
                sbStr.Append("</div>");

                sbStr.Append("<div class=\"col-sm-3 form-validation\">");
                sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nFirstName\" placeholder=\"First Name\" value=\"" + hProposal.n_firstname + "\"  onkeypress='return IsAlphabet(event)'/>");
                sbStr.Append("</div>");
                sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord\" id=\"nLastName\" placeholder=\"Last Name\" value=\"" + hProposal.n_lastname + "\"  onkeypress='return IsAlphabet(event)'/>");
                sbStr.Append("</div>");
                sbStr.Append("<div class=\"col-sm-2 form-validation\">");
                sbStr.Append("<input type = \"text\" class=\"form-control formcontlbord DynamicCommanDateWithYr\" id=\"nDOB\" maxlength='10' readonly autocomplete=\"off\" placeholder=\"Date Of Birth\" onchange=\"return CalculateNomineeAge('nDOB','ndobvalid','position: absolute;top: 37px;right: 15px;font-size: 10px;')\" value=\"" + hProposal.n_dob + "\"><span class=\"ndobvalid\"></span>");
                sbStr.Append("</div>");

                sbStr.Append("</div>");
                sbStr.Append("</div>");
                //Nominee Section End


                sbStr.Append("<div class='col-sm-12'>");
                sbStr.Append("<button class='btn theme-btn btn-hover' onclick='return Backform2Section2();' style='padding: 10px 50px;float:left;background: #000000;border-color: #000000;' id='btnInsuredPrev' >&lt; Previous</button>");
                sbStr.Append("<button class='btn theme-btn btn-hover' style='padding: 10px 50px; float:right' id='btnInsuredNext' onclick='return GoToMedicalHisSection();'>NEXT &gt;</button>");
                sbStr.Append("</div>");
                sbStr.Append("</div>");

                result.Add(sbStr.ToString());
                result.Add(totalPerson.ToString());
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static string InsertInsured_NomineeDetail(HealthProposal proposal)
        {
            try
            {
                bool issuccess = false;
                if (proposal.InsuredList != null && proposal.InsuredList.Count > 0)
                {
                    HealthProposal hProposal = GetHealthProposalDetail(proposal.enquiryid);
                    foreach (var insured in proposal.InsuredList)
                    {
                        insured.productid = hProposal.productid;
                        insured.companyid = hProposal.productid;
                        issuccess = Health_Helper.InsertUpdate_InsuredDetail(insured);
                    }

                    proposal.actionType = "nominee";
                    if (Health_Helper.InsertUpdate_NomineeDetail(proposal))
                    {
                        return GetMedicalHistoryDetail(hProposal, string.Empty);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return string.Empty;
        }
        private static string GetMedicalHistoryDetail(HealthProposal proposal, string agencyId)
        {
            StringBuilder sbStr = new StringBuilder();
            try
            {
                string response = Health_Helper.GetHealthQuestionaries(proposal, agencyId);
                if (!string.IsNullOrEmpty(response))
                {
                    HealthQuestionaries hQuestion = JsonConvert.DeserializeObject<HealthQuestionaries>(response);
                    if (hQuestion.success)
                    {
                        if (hQuestion.response.Count > 0)
                        {
                            List<HealthInsured> insuredDel = Health_Helper.GetInsuredDetails(proposal.enquiryid);

                            sbStr.Append("<div class='col-sm-12'>");
                            sbStr.Append("<div class='tabmedi row'>");
                            int insuredloop = 0;
                            foreach (var insured in insuredDel)
                            {
                                sbStr.Append("<button class='tablinks butntab " + (insuredloop == 0 ? "active" : "") + "' id='tab" + (insured.firstname + "_" + insured.lastname) + "' onclick='openCity(event, \"" + (insured.firstname + "_" + insured.lastname).Trim() + "\")'>" + insured.firstname + " " + insured.lastname + "</button>");
                                insuredloop = insuredloop + 1;
                            }
                            sbStr.Append("</div>");
                            sbStr.Append("</div>");

                            sbStr.Append("<input type=\"hidden\" id=\"hdnQuestionCount\" value=\"" + hQuestion.response.Count + "\" />");
                            sbStr.Append("<input type=\"hidden\" id=\"hdnInsuredCount\" value=\"" + insuredDel.Count + "\" />");
                            insuredloop = 0;
                            foreach (var insured in insuredDel)
                            {
                                sbStr.Append("<input type=\"hidden\" id=\"hdnInsuredName_" + insuredloop + "\" value=\"" + insured.firstname + "," + insured.lastname + "\" />");
                                sbStr.Append("<div id='" + insured.firstname + "_" + insured.lastname + "' class='tabcontent " + (insuredloop == 0 ? "active" : "") + "' " + (insuredloop == 0 ? "style='display:block;'" : "style='display:none;'") + ">");
                                sbStr.Append("<div class='col-sm-12 text-center' style='padding: 10px;text-align: left;'>");

                                foreach (var item in hQuestion.response)
                                {
                                    if (!string.IsNullOrEmpty(item.response))
                                    {
                                        item.questionCode = item.questionCode.ToString().Trim();

                                        bool issubquestionexist = item.subQuestion.Count > 0 ? true : false;
                                        bool lagaobeta = false;
                                        sbStr.Append("<div class='row' style='border-bottom: 1px solid #ccc;padding: 5px;text-align: left;'>");
                                        sbStr.Append("<div class='col-sm-10' id='divMainQuestion_" + insuredloop + "_" + item.questionCode + "' data-subquestionexist='" + issubquestionexist + "' >");
                                        sbStr.Append(item.questionDescription);
                                        sbStr.Append("</div>");
                                        sbStr.Append("<div class='col-sm-2'>");
                                        if (item.response.ToLower() == "boolean")
                                        {
                                            sbStr.Append("<input type='checkbox' class='mainquestionar' data-insuredcount='" + insuredloop + "' id='chkQues_" + insuredloop + "_" + item.questionCode + "' name='chkQues_" + insuredloop + "_" + item.questionCode + "' data-repotype='" + item.response + "' data-isrequired='" + item.required + "' data-quessetcode='" + item.questionSetCode.Replace(" ", "_") + "' data-quescode='" + item.questionCode + "' style='width: 20px; height: 18px; float: right;'/>");
                                        }

                                        sbStr.Append("</div>");

                                        foreach (var subitem in item.subQuestion)
                                        {
                                            if (!string.IsNullOrEmpty(subitem.response))
                                            {
                                                bool issub2questionexist = subitem.subQuestion.Count > 0 ? true : false;
                                                sbStr.Append("<div class='col-sm-12 DivSubQuestionSection_" + insuredloop + "_" + item.questionCode + "' style='padding-top: 5px;display:none;' " + (lagaobeta == false ? "style='padding-top: 10px;'" : "") + ">");
                                                sbStr.Append("<div class='row'>");
                                                sbStr.Append("<div class='col-sm-10'>" + subitem.questionDescription + "</div>");
                                                sbStr.Append("<div class='col-sm-2'>");


                                                if (subitem.response.ToLower() == "boolean")
                                                {
                                                    sbStr.Append("<input type='checkbox' id='" + insuredloop + "_" + item.questionCode + "_" + subitem.questionCode.Trim() + "' class='submenuclass subquestionfield_" + insuredloop + "_" + item.questionCode + "' data-subquestioncode='" + subitem.questionCode.Trim() + "' data-repotype='" + subitem.response + "' data-sub2exist='" + issub2questionexist + "' data-subdesc='" + subitem.questionDescription + "' data-subquestionsetcode='" + subitem.questionSetCode + "' data-isrequired='" + subitem.required + "' style='width: 20px; height: 18px; float: right;' />");
                                                }

                                                sbStr.Append("</div>");
                                                sbStr.Append("</div>");
                                                sbStr.Append("</div>");
                                                lagaobeta = true;
                                            }
                                        }

                                        sbStr.Append("</div>");
                                    }
                                }

                                sbStr.Append("</div>");
                                sbStr.Append("</div>");
                                insuredloop = insuredloop + 1;
                            }
                        }
                        else
                        {
                            sbStr.Append("<input type=\"hidden\" id=\"hdnQuestionCount\" value=\"" + hQuestion.response.Count + "\" />");
                            sbStr.Append("<p style=\"color: red\">No medical details available!</p>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return sbStr.ToString(); ;
        }
        public static Dictionary<string, string> GenrateCareProposal(string enquiryid, string agencyId, List<HealthPerposalQuestion> medhistory)
        {
            Dictionary<string, string> strResult = new Dictionary<string, string>();
            try
            {
                if (Health_Helper.UpdateHealthMedicalHistory(medhistory))
                {
                    string policyStartEndDetail = string.Empty;
                    string request = CreateProposalRequestHtml(enquiryid, ref policyStartEndDetail);
                    if (Health_Helper.InsertProposalRequestData(request.Replace("'", "''"), enquiryid, "request"))
                    {
                        string response = Health_Helper.GenrateCareProposal(request, enquiryid, agencyId);
                        if (!string.IsNullOrEmpty(response))
                        {
                            if (Health_Helper.InsertProposalRequestData(response, enquiryid, "response"))
                            {
                                HealthPerposalResponse result = JsonConvert.DeserializeObject<HealthPerposalResponse>(response);
                                if (result != null)
                                {
                                    if (result.success)
                                    {
                                        if (result.response.data != null)
                                        {
                                            string referenceId = result.response.data.referenceId != null ? result.response.data.referenceId.ToString() : string.Empty;
                                            string proposalNum = result.response.data.proposalNum != null ? result.response.data.proposalNum.ToString() : string.Empty;
                                            string totalPremium = result.response.data.totalPremium != null ? result.response.data.totalPremium.ToString() : string.Empty;
                                            string paymenturl = result.response.urls != null ? result.response.urls.ToString() + "/" + enquiryid : string.Empty;
                                            string premium = result.response.data.premium != null ? result.response.data.premium.ToString() : string.Empty;
                                            string serviceTax = result.response.data.serviceTax != null ? result.response.data.serviceTax.ToString() : string.Empty;

                                            bool is_startenddateupdated = Health_Helper.UpdateStartEndPolicyDate(enquiryid, policyStartEndDetail);

                                            if (Health_Helper.UpdateProposalRespoData(referenceId, proposalNum, enquiryid))
                                            {
                                                if (Health_Helper.UpdatePaymentTabData(referenceId, proposalNum, paymenturl, premium, serviceTax, totalPremium, enquiryid))
                                                {
                                                    strResult.Add("status", result.success.ToString());
                                                    strResult.Add("EnquiryId", enquiryid);
                                                    strResult.Add("Premium", UtilityClass.IndianMoneyFormat(result.response.data.totalPremium.ToString()) + "/- (Inclusive of all Tax)");
                                                    strResult.Add("ProposalNo", proposalNum);
                                                }
                                            }
                                        }
                                        else
                                        {
                                            strResult.Add("status", "error");
                                            strResult.Add("message", result.response.message);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return strResult;
        }
        private static string CreateProposalRequestHtml(string enquiryid, ref string policyStartEnd)
        {
            StringBuilder rowData = new StringBuilder();
            try
            {
                HealthEnquiry health = Health_Helper.GetHealthEnquiryDetail(enquiryid);
                if (!string.IsNullOrEmpty(health.selectedproductjson))
                {
                    string[] spliturlpara = health.searchurl.Split('&');
                    string product = spliturlpara[1].Split('=')[1].Trim().ToLower();
                    string companyid = spliturlpara[2].Split('=')[1].Trim().ToLower();
                    string suminsuredid = spliturlpara[3].Split('=')[1].Trim().ToLower();
                    string suminsured = spliturlpara[4].Split('=')[1].Trim().ToLower();
                    string adult = spliturlpara[5].Split('=')[1].Trim().ToLower();
                    string child = spliturlpara[6].Split('=')[1].Trim().ToLower();
                    string plan = spliturlpara[7].Split('=')[1].Trim().ToLower();
                    string totalamt = spliturlpara[8].Split('=')[1].Trim().ToLower();
                    string tenure = spliturlpara[9].Split('=')[1].Trim().ToLower();

                    HealthProposal hProposal = GetHealthProposalDetail(enquiryid);
                    List<HealthInsured> insuredList = Health_Helper.GetInsuredDetails(enquiryid);
                    List<Health_Datum> hData = JsonConvert.DeserializeObject<List<Health_Datum>>(health.selectedproductjson);
                    List<AddonChecked> addOneList = Health_Helper.GetAddOneList(enquiryid);

                    foreach (var item in hData)
                    {
                        string productId = item.product.productDetailId > 0 ? item.product.productDetailId.ToString() : string.Empty;
                        if (productId == product && companyid == item.companyId.ToString() && suminsuredid == item.suminsuredId.ToString() && suminsured == item.sumInsuredValue.ToString() && plan == item.plan && totalamt == item.totalPremium && tenure == item.tenure.ToString())
                        {
                            rowData.Append("{");

                            //rowData.Append("\"id\": 1,");
                            rowData.Append("\"productDetailId\": " + item.productDetailID + ",");
                            rowData.Append("\"quoteId\": \"\",");

                            rowData.Append("\"proposerDetails\": {");
                            rowData.Append("\"firstName\": \"" + hProposal.p_firstname + "\",\"lastName\": \"" + hProposal.p_lastname + "\",\"middleName\": \"\",\"gender\": \"" + hProposal.p_gender + "\",");
                            rowData.Append("\"mobileNumber\": \"" + hProposal.p_mobile + "\",\"email\": \"" + hProposal.p_email + "\",\"addLine1\": \"" + hProposal.p_address1 + "\",\"addLine2\": \"" + hProposal.p_address2 + "\",");
                            rowData.Append("\"landmark\": \"" + hProposal.p_landmark + "\",\"postalCode\": " + hProposal.p_pincode + ",\"zoneCd\": \"\",\"state\": \"" + hProposal.p_statename + "\",\"city\": \"" + hProposal.p_cityname + "\",");
                            rowData.Append("\"proposerDob\": \"" + UtilityClass.ReverseDateFormate(hProposal.p_dob) + "\",\"panNumber\": \"" + hProposal.p_pannumber + "\",\"aadharNumber\": \"" + hProposal.p_aadharno + "\",");
                            rowData.Append("\"previousMedicalInsurance\": \"\",\"annualIncome\": \"" + hProposal.p_annualincome + "\",\"criticalIllness\": \"false\"");
                            rowData.Append("},");

                            policyStartEnd = "starton='" + DateTime.Now.AddDays(2).ToString("dd-MM-yyyy") + "',endon='" + DateTime.Now.AddDays(Convert.ToInt32(item.product.period) * 365).ToString("dd-MM-yyyy") + "'";

                            rowData.Append("\"starton\": \"" + DateTime.Now.AddDays(2).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ") + "\",\"endon\": \"" + DateTime.Now.AddDays(365 * Convert.ToInt32(item.product.period)).ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ") + "\",");

                            rowData.Append("\"nomineeDetails\": {\"nomineeFirstName\": \"" + hProposal.n_firstname + "\",\"nomineeLastName\": \"" + hProposal.n_lastname + "\",\"nomineeRelation\": \"" + hProposal.n_relation.Trim() + "\",\"nomineeAge\": \"" + hProposal.n_age + "\"},");

                            rowData.Append("\"insureds\": [");

                            int insurloop = 1;
                            foreach (var insured in insuredList)
                            {
                                string kkMedHistory = string.Empty;
                                if (!string.IsNullOrEmpty(insured.medicalhistory))
                                {
                                    kkMedHistory = insured.medicalhistory.Replace("\n", "").Replace("\r", "");
                                }

                                string gender = insured.title.ToLower() == "mr." ? "MALE" : "FEMALE";
                                rowData.Append("{\"firstName\": \"" + insured.firstname + "\",\"lastName\": \"" + insured.lastname + "\",\"dob\": \"" + UtilityClass.ReverseDateFormate(insured.dob) + "\",\"gender\": \"" + gender + "\",\"illness\": \"" + (insured.illness == true ? insured.illnessdesc : insured.illness.ToString()) + "\",\"relationshipId\": " + insured.relationid + ",\"occupationId\": " + insured.occupationId + ",\"ispersonalAccidentApplicable\": \"" + (insurloop == 1 ? "true" : "false") + "\",\"engageManualLabour\": \"" + (insured.engageManualLabour == true ? insured.engageManualLabourDesc : insured.engageManualLabour.ToString()) + "\",\"engageWinterSports\": \"" + (insured.engageWinterSports == true ? insured.engageWinterSportDesc : insured.engageWinterSports.ToString()) + "\",\"height\": " + insured.height + ",\"weight\": " + insured.weight + ",\"buyBackPED\": 0,\"questionsList\": " + (!string.IsNullOrEmpty(kkMedHistory) ? kkMedHistory : "[]") + "}");

                                if (insurloop != insuredList.Count)
                                {
                                    rowData.Append(",");
                                }
                                insurloop = insurloop + 1;
                            }
                            rowData.Append("],");

                            rowData.Append("\"discountPercent\": \"" + item.discountPercent + "\",");
                            rowData.Append("\"premium\": \"" + item.premium + "\",");
                            rowData.Append("\"serviceTax\": \"" + item.serviceTax + "\",");
                            rowData.Append("\"totalPremium\": \"" + item.totalPremium + "\",");
                            rowData.Append("\"schemaId\": " + item.schemaId + ",");
                            rowData.Append("\"insuranceCompanyId\": " + item.companyId + ",");
                            rowData.Append("\"policyType\": \"" + item.policytype + "\",");
                            rowData.Append("\"suminsured\": " + item.sumInsuredValue + ",");
                            rowData.Append("\"suminsuredId\": " + item.suminsuredId + ",");

                            rowData.Append("\"products\": {\"policyTypeName\": \"" + item.product.policyTypeName + "\",\"policyName\": \"" + item.product.policyName + "\",\"period\": \"" + item.product.period + "\"},");

                            rowData.Append("\"addOns\":\"");

                            if (addOneList != null && addOneList.Count > 0)
                            {
                                int addonloop = 1;
                                foreach (var addon in addOneList)
                                {
                                    if (addonloop == addOneList.Count())
                                    {
                                        rowData.Append(addon.code);
                                    }
                                    else
                                    {
                                        rowData.Append(addon.code + ",");
                                    }
                                    addonloop = addonloop + 1;
                                }
                            }

                            rowData.Append("\",");

                            rowData.Append("\"productTerms\": {},");
                            rowData.Append("\"inquiry_id\": \"" + item.inquiry_id + "\"");

                            rowData.Append("}");
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return rowData.ToString();
        }
        public static bool AddLessAddon(string[] addonlist)
        {
            return Health_Helper.AddLessAddon(addonlist);
        }
        public static bool UpdateSelectedAddons(List<AddonChecked> addonlist)
        {
            return Health_Helper.UpdateSelectedAddons(addonlist);
        }
        #endregion

        #region  [HealthProposalSummary Section]

        public static string GetHealthCarePaymentUrl(string enquiryid)
        {
            string paymenturl = string.Empty;
            HealthPayment payment = Health_Helper.GetHealthPaymentDetails(enquiryid);
            if (payment.paymentstatus)
            {
                paymenturl = "paid";
            }
            else
            {
                HealthProposal hProposal = GetHealthProposalDetail(enquiryid);
                paymenturl = hProposal.paymenturl;//"http://148.66.135.80/CarePayment/" + payment.proposalNum; //payment.paymenturl;
            }
            return paymenturl;
        }
        public static string CheckHealthPaymentDetails(string enquiryid)
        {
            string result = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(enquiryid))
                {
                    HealthPayment payment = Health_Helper.GetHealthPaymentDetails(enquiryid);
                    if (payment.paymentstatus)
                    {
                        HealthProposal hProposal = GetHealthProposalDetail(enquiryid);
                        if (!string.IsNullOrEmpty(hProposal.proposalNumber) || !string.IsNullOrEmpty(hProposal.policyNumber))
                        {
                            result = "<h5 class='text-success'>Proposal Number : " + (!string.IsNullOrEmpty(hProposal.proposalNumber) ? hProposal.proposalNumber : "- - -") + "</h5><h5 class='text-success'>Policy Number : " + (!string.IsNullOrEmpty(hProposal.policyNumber) ? hProposal.policyNumber : "- - -") + "</h5>";
                        }
                        else
                        {
                            result = "failed";
                        }
                    }
                    else
                    {
                        result = "notpay";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        public static List<string> GetHealthPdfDocument(string enquiryid)
        {
            List<string> details = new List<string>();
            if (!string.IsNullOrEmpty(enquiryid))
            {
                HealthPayment payment = Health_Helper.GetHealthPaymentDetails(enquiryid);
                if (payment.paymentstatus)
                {
                    if (!string.IsNullOrEmpty(payment.cc_policyNumber) && !string.IsNullOrEmpty(payment.enquiryid))
                    {
                        details.Add(payment.cc_policyNumber);
                        details.Add(payment.enquiryid);
                    }
                }
                else
                {
                    details.Add("notpay");
                }
            }
            return details;
        }
        public static bool UpdateHealthPolicyPdfLink(string enquiryid, string pdfurl)
        {
            return Health_Helper.UpdateHealthPolicyPdfLink(enquiryid, pdfurl);
        }
        public static List<string> BindProposalSummaryDetails(string enquiryid)
        {
            List<string> result = new List<string>();
            try
            {
                HealthEnquiry health = Health_Helper.GetHealthEnquiryDetail(enquiryid);
                if (!string.IsNullOrEmpty(health.selectedproductjson))
                {
                    List<Health_Datum> hData = JsonConvert.DeserializeObject<List<Health_Datum>>(health.selectedproductjson);
                    HealthProposal hProposal = GetHealthProposalDetail(enquiryid);
                    List<HealthInsured> insuredList = Health_Helper.GetInsuredDetails(enquiryid);
                    HealthPayment payment = Health_Helper.GetHealthPaymentDetails(enquiryid);
                    List<AddonChecked> addOneList = Health_Helper.GetAddOneList(enquiryid);
                    List<string> adultChild = AdultChildCount(health);

                    string[] spliturlpara = health.searchurl.Split('&');
                    string product = spliturlpara[1].Split('=')[1].Trim().ToLower();
                    string companyid = spliturlpara[2].Split('=')[1].Trim().ToLower();
                    string suminsuredid = spliturlpara[3].Split('=')[1].Trim().ToLower();
                    string suminsured = spliturlpara[4].Split('=')[1].Trim().ToLower();
                    string adult = spliturlpara[5].Split('=')[1].Trim().ToLower();
                    string child = spliturlpara[6].Split('=')[1].Trim().ToLower();
                    string plan = spliturlpara[7].Split('=')[1].Trim().ToLower();
                    string totalamt = spliturlpara[8].Split('=')[1].Trim().ToLower();
                    string tenure = spliturlpara[9].Split('=')[1].Trim().ToLower();

                    foreach (var item in hData)
                    {
                        string productId = item.product.productDetailId > 0 ? item.product.productDetailId.ToString() : string.Empty;
                        if (productId == product && companyid == item.companyId.ToString() && suminsuredid == item.suminsuredId.ToString() && suminsured == item.sumInsuredValue.ToString() && plan == item.plan && totalamt == item.totalPremium && tenure == item.tenure.ToString())
                        {
                            bool isaddon = addOneList.Count > 0 ? true : false;
                            result.Add(BindProduct_FareBreakUpHtml(item, adultChild, payment.premium, payment.serviceTax, payment.totalPremium, isaddon));
                        }
                    }

                    result.Add(BindProposalSummary_ProposerDetails(hProposal));
                    result.Add(BindProposalSummary_InsuredDetails(insuredList));
                    result.Add(BindProposalSummary_MedicalDetails(insuredList));
                    result.Add(BindProposalSummary_NomineeDetails(hProposal));
                    result.Add(BindProposalSummary_AddonDetails(addOneList));
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return result;
        }
        //private static string BindProposalSummary_PrePremiumBreakup()
        //{
        //    StringBuilder sbPremium = new StringBuilder();

        //    sbPremium.Append("<div class='col-sm-12'><img src='/Content/images/care.jpg' style='margin-left: auto;margin-right: auto;display: block;'></div>");
        //    sbPremium.Append("<div class='col-sm-12' style='margin-top: 20px;'>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Product  Name</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Medi Classic</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Plan Type</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>SILVER</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Policy Type</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>INDIVIDUAL</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Policy Period</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>1 year(s)</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Member</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Adult-1, Child-0</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Sum Insred</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>₹ 3,00,000.00</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='col-sm-12' style='margin-top: 2%;margin-bottom: 2%;font-size: 20px;text-align: center;color: #19aee6;'>");
        //    sbPremium.Append("<b class='text-center'>Premium Breakup</b></div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>Basic Breakup</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>₹ 3,00,000.00</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row' style='border-bottom: 1px solid #ccc;line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>GST 18%</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>₹ 3,00,000.00</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("<div class='row ' style='line-height: 25px;'>");
        //    sbPremium.Append("<div class='col-sm-6'><label>You'll Pay</label></div>");
        //    sbPremium.Append("<div class='col-sm-6'><label>₹ 3,00,000.00</label></div>");
        //    sbPremium.Append("</div>");

        //    sbPremium.Append("</div>");
        //    sbPremium.Append("</div>");

        //    return sbPremium.ToString();
        //}
        private static string BindProposalSummary_ProposerDetails(HealthProposal hProposal)
        {
            StringBuilder sbProInsured = new StringBuilder();

            sbProInsured.Append("<input type='text' class='hidden' id='propNoValue' readonly value='" + hProposal.proposalNumber + "'>");
            sbProInsured.Append("<table class='table table-striped summary_data_table'>");
            sbProInsured.Append("<tbody>");
            sbProInsured.Append("<tr> <td>Gender</td> <td><strong>:</strong> " + hProposal.p_gender + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Name</td> <td><strong>:</strong> " + hProposal.p_title + " " + hProposal.p_firstname + " " + hProposal.p_lastname + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>D.O.B</td> <td><strong>:</strong> " + hProposal.p_dob + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Mobile</td> <td><strong>:</strong> " + hProposal.p_mobile + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Email</td> <td><strong>:</strong> " + hProposal.p_email + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Pan No</td> <td><strong>:</strong> " + (!string.IsNullOrEmpty(hProposal.p_pannumber) ? hProposal.p_pannumber.ToUpper() : "<span class='text-danger'>N/A</span >") + "<br/></td> </tr>");
            sbProInsured.Append("<tr> <td>Aadhar No</td> <td><strong>:</strong> " + (!string.IsNullOrEmpty(hProposal.p_aadharno) ? hProposal.p_aadharno : "<span class='text-danger'>N/A</span >") + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Annual Income</td> <td><strong>:</strong> " + UtilityClass.IndianMoneyFormat(hProposal.p_annualincome.Replace("INR", "")) + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Address</td> <td><strong>:</strong> " + (hProposal.p_address1 + ", " + hProposal.p_address2 + ", " + hProposal.p_landmark + ", " + hProposal.p_pincode) + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>Pincode</td> <td><strong>:</strong> " + hProposal.p_pincode + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>State</td> <td><strong>:</strong> " + hProposal.p_statename + "<br></td> </tr>");
            sbProInsured.Append("<tr> <td>City</td> <td><strong>:</strong> " + hProposal.p_cityname + "<br></td> </tr>");
            sbProInsured.Append("</tbody>");
            sbProInsured.Append("</table>");

            return sbProInsured.ToString();
        }
        private static string BindProposalSummary_InsuredDetails(List<HealthInsured> insuredList)
        {
            StringBuilder sbProInsured = new StringBuilder();

            sbProInsured.Append("<table class='table table-striped summary_data_table'>");
            sbProInsured.Append("<tbody>");
            sbProInsured.Append("<tr style='background-color: #f9f9f9'>");
            sbProInsured.Append("<th>Relation</th><th>Name</th><th>Gender</th><th>Occupation</th><th>Date of Birth</th><th>Height</th><th>Weight</th>");
            sbProInsured.Append("</tr>");
            if (insuredList != null && insuredList.Count > 0)
            {
                foreach (var item in insuredList)
                {
                    sbProInsured.Append("<tr>");
                    sbProInsured.Append("<td>  " + item.relationname + "   </td>");
                    sbProInsured.Append("<td> " + (item.title + " " + item.firstname + " " + item.lastname) + " </td>");
                    sbProInsured.Append("<td> " + (item.title.ToLower().Trim() == "mr." ? "MALE" : "FEMALE") + " </td>");
                    sbProInsured.Append("<td> " + item.occupation + "  </td>");
                    sbProInsured.Append("<td> " + item.dob + "  </td>");
                    sbProInsured.Append("<td> " + item.height + "  </td>");
                    sbProInsured.Append("<td> " + item.weight + "  </td>");
                    sbProInsured.Append("</tr>");
                }
            }
            else
            {
            }
            sbProInsured.Append("</tbody>");
            sbProInsured.Append("</table>");

            return sbProInsured.ToString();
        }
        private static string BindProposalSummary_MedicalDetails(List<HealthInsured> insuredList)
        {
            StringBuilder sbProInsured = new StringBuilder();

            if (insuredList.Count > 0)
            {

                sbProInsured.Append("<div style='margin-bottom: 20px;'>");
                //sbProInsured.Append("<label style=\"font-size: 20px;\">Medical Details of : </label>");

                sbProInsured.Append("<ul style='display: flex;'>");
                for (int i = 0; i < insuredList.Count; i++)
                {
                    if (i == 0)
                    {
                        sbProInsured.Append("<li class=\"tabcommon ui-tabs-tab ui-corner-top ui-state-default ui-tab ui-tabs-active ui-state-active\" data-insuredno='" + (i + 1) + "' id=\"insuredMedicalPerson_" + (i + 1) + "\" onclick='ChangeInsuredMedicalData(" + (i + 1) + "," + (insuredList.Count) + ")'>" + insuredList[i].firstname + " " + insuredList[i].lastname + "</li>");
                    }
                    else
                    {
                        sbProInsured.Append("<li class=\"tabcommon ui-tabs-tab ui-corner-top ui-state-default ui-tab\" data-insuredno='" + (i + 1) + "' id=\"insuredMedicalPerson_" + (i + 1) + "\" onclick='ChangeInsuredMedicalData(" + (i + 1) + "," + (insuredList.Count) + ")'>" + insuredList[i].firstname + " " + insuredList[i].lastname + "</li>");
                    }

                }
                sbProInsured.Append("</ul>");
                sbProInsured.Append("</div>");


                sbProInsured.Append("<table class='table table-striped table-bordered summary_data_table' style=\"border: 1px black solid;\">");

                for (int i = 0; i < insuredList.Count; i++)
                {
                    if (i == 0)
                    {
                        sbProInsured.Append("<tbody data-insuredno='" + (i + 1) + "' id=\"insuredMedicalData_" + (i + 1) + "\" style=\"text-align: justify;\">");
                    }
                    else
                    {
                        sbProInsured.Append("<tbody class='hidden' data-insuredno='" + (i + 1) + "' id=\"insuredMedicalData_" + (i + 1) + "\" style=\"text-align: justify;\">");
                    }


                    if (!string.IsNullOrEmpty(insuredList[i].medicalhistory))
                    {
                        List<Questionary_Response> questionaries = JsonConvert.DeserializeObject<List<Questionary_Response>>(insuredList[i].medicalhistory);

                        foreach (var ques in questionaries)
                        {
                            string mainRespo = ques.response;

                            sbProInsured.Append("<tr>");
                            sbProInsured.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\">" + ques.questionDescription + "</td>");
                            if (mainRespo == "true")
                            {
                                sbProInsured.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\" class=\"text-center text-success\"><b> YES</b> </td>");
                            }
                            else
                            {
                                sbProInsured.Append("<td style=\"border-top: 1px solid black;border-bottom: 1px solid black;\" class=\"text-center text-danger\"><b> NO</b> </td>");
                            }
                            sbProInsured.Append("</tr>");

                            foreach (var subques in ques.subQuestion)
                            {
                                sbProInsured.Append("<tr>");
                                sbProInsured.Append("<td > " + subques.questionDescription + "</td>");
                                sbProInsured.Append("<td >" + subques.response + "</td>");
                                sbProInsured.Append("</tr>");
                            }
                        }
                    }
                    else
                    {
                        sbProInsured.Append("<tr>");
                        sbProInsured.Append("<td style='text-align: center;color: red;border-top: none;'>No medical record found!</td>");
                        sbProInsured.Append("</tr>");
                    }

                    sbProInsured.Append("</tbody>");
                }

                sbProInsured.Append("</table>");
            }
            else
            {
                sbProInsured.Append("<table class='table table-striped table-bordered summary_data_table' style=\"border: 1px black solid;\">");
                sbProInsured.Append("<tr>");
                sbProInsured.Append("<td style='text-align: center;color: red;border-top: none;'>No Insured record found!</td>");
                sbProInsured.Append("</tr>");
                sbProInsured.Append("</tbody>");
                sbProInsured.Append("</table>");
            }

            return sbProInsured.ToString();
        }
        private static string BindProposalSummary_NomineeDetails(HealthProposal hProposal)
        {
            StringBuilder sbProInsured = new StringBuilder();

            sbProInsured.Append("<table class='table table-striped summary_data_table'>");
            sbProInsured.Append("<tbody>");
            sbProInsured.Append("<tr><td>Nominee Name</td><td><strong>:</strong> " + (hProposal.n_title + " " + hProposal.n_firstname + " " + hProposal.n_lastname) + "</td></tr>");
            sbProInsured.Append("<tr><td>Nominee Relation</td><td><strong>:</strong> " + hProposal.n_relation + "</td></tr>");
            sbProInsured.Append("<tr><td>Nominee DOB</td><td><strong>:</strong> " + hProposal.n_dob + "</td></tr>");
            sbProInsured.Append("</tbody>");
            sbProInsured.Append("</table>");

            return sbProInsured.ToString();
        }
        private static string BindProposalSummary_AddonDetails(List<AddonChecked> addOneList)
        {
            StringBuilder sbProInsured = new StringBuilder();

            sbProInsured.Append("<table class='table table-striped summary_data_table'>");
            sbProInsured.Append("<tbody>");
            if (addOneList != null && addOneList.Count > 0)
            {
                foreach (var addon in addOneList)
                {
                    sbProInsured.Append("<tr><td>" + addon.addOns + "</td><td><strong>:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class='text-success'><i class='fa fa-check-circle' aria-hidden='true'></i>&nbsp;Selected</span></td></tr>");
                }
            }
            sbProInsured.Append("</tbody>");
            sbProInsured.Append("</table>");

            return sbProInsured.ToString();
        }
        private static string GenerateHealth_PaymentUrl(string enquiryid)
        {
            string paymentStr = string.Empty;
            try
            {
                HealthProposal hProposal = GetHealthProposalDetail(enquiryid);
                paymentStr = Health_Helper.GenerateHealth_PaymentUrl(hProposal.proposalNumber);
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return paymentStr;
        }
        public static bool UpdateCarePaymentDetails(PaymentModel payment)
        {
            return Health_Helper.UpdateCarePaymentDetails(payment);
        }
        public static PaymentModel GetPaymentSuccessPageData(PaymentModel payment)
        {
            HealthPayment paymentTabDetail = Health_Helper.GetHealthPaymentDetails(payment.enquiryid);
            payment.imgurl = "/Content/images/care.jpg";
            payment.proposalNum = paymentTabDetail.proposalNum;
            payment.premium = UtilityClass.IndianMoneyFormat(paymentTabDetail.totalPremium);
            payment.paymentdate = paymentTabDetail.paymentdate;
            return payment;
        }
        #endregion
    }
}