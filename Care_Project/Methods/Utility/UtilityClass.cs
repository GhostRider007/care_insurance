﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace Care_Project.Methods.Utility
{
    public class UtilityClass
    {
        public static string GenrateTrackId()
        {
            return GenrateRandomTransactionId("RWT", 7, null);
        }
        public static string GenrateEnquiryId(int sizelimit, string charcombo)
        {
            DateTime dtCurr = DateTime.Now;
            string todatdate = dtCurr.Year.ToString() + dtCurr.Month.ToString() + dtCurr.Day.ToString();
            return GenrateRandomTransactionId(todatdate, sizelimit, charcombo);
        }
        public static string GenrateRandomTransactionId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
        public static string ConvertDateToISOFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                DateTime dtCDate = Convert.ToDateTime(rutDate);
                rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }

        public static string ReverseDateFormate(string date)
        {
            string rutDate = date;
            if (!string.IsNullOrEmpty(date))
            {
                string[] splitDate = date.Split('/');
                rutDate = splitDate[2] + "-" + splitDate[1] + "-" + splitDate[0];

                //DateTime dtCDate = Convert.ToDateTime(rutDate);
                //rutDate = dtCDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.sssZ");
            }
            return rutDate;
        }
        public static string FormatURL(string value)
        {
            return value.Replace("//", "/").Replace(":/", "://").ToLower().Trim();
        }
        public static string IndianMoneyFormat(string fare)
        {
            //decimal parsed = decimal.Parse((Math.Ceiling(Convert.ToDecimal(fare)).ToString()), CultureInfo.InvariantCulture);
            decimal parsed = decimal.Parse(fare, CultureInfo.InvariantCulture);
            CultureInfo hindi = new CultureInfo("hi-IN");
            return string.Format(hindi, "{0:c}", parsed).Replace("₹", "₹ ");
        }
        public static string ApiBase64Encode(string data)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(data);
            return System.Convert.ToBase64String(plainTextBytes);
        }
        public static string ApiBase64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
        public static string GetLocalIPAddress()
        {
            string result = string.Empty;

            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    result = ip.ToString();
                }
            }

            return result;
        }
        public static string EncryptString(string key, string plainText)
        {
            byte[] iv = new byte[16];
            byte[] array;

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;

                ICryptoTransform encryptor = aes.CreateEncryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream())
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter streamWriter = new StreamWriter((Stream)cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        array = memoryStream.ToArray();
                    }
                }
            }

            return Convert.ToBase64String(array);
        }
        public static string DecryptString(string key, string cipherText)
        {
            byte[] iv = new byte[16];
            byte[] buffer = Convert.FromBase64String(cipherText);

            using (Aes aes = Aes.Create())
            {
                aes.Key = Encoding.UTF8.GetBytes(key);
                aes.IV = iv;
                ICryptoTransform decryptor = aes.CreateDecryptor(aes.Key, aes.IV);

                using (MemoryStream memoryStream = new MemoryStream(buffer))
                {
                    using (CryptoStream cryptoStream = new CryptoStream((Stream)memoryStream, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader streamReader = new StreamReader((Stream)cryptoStream))
                        {
                            return streamReader.ReadToEnd();
                        }
                    }
                }
            }
        }
        public static string Encrypt(string input)
        {
            string ToReturn = string.Empty;
            try
            {
                string publickey = "shrikant";
                string secretkey = "software";
                byte[] secretkeyByte = { };
                secretkeyByte = System.Text.Encoding.UTF8.GetBytes(secretkey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = System.Text.Encoding.UTF8.GetBytes(input);
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateEncryptor(publickeybyte, secretkeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    ToReturn = Convert.ToBase64String(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }
        public static string Decrypt(string input)
        {
            string ToReturn = string.Empty;
            try
            {
                string publickey = "shrikant";
                string privatekey = "software";
                byte[] privatekeyByte = { };
                privatekeyByte = System.Text.Encoding.UTF8.GetBytes(privatekey);
                byte[] publickeybyte = { };
                publickeybyte = System.Text.Encoding.UTF8.GetBytes(publickey);
                MemoryStream ms = null;
                CryptoStream cs = null;
                byte[] inputbyteArray = new byte[input.Replace(" ", "+").Length];
                inputbyteArray = Convert.FromBase64String(input.Replace(" ", "+"));
                using (DESCryptoServiceProvider des = new DESCryptoServiceProvider())
                {
                    ms = new MemoryStream();
                    cs = new CryptoStream(ms, des.CreateDecryptor(publickeybyte, privatekeyByte), CryptoStreamMode.Write);
                    cs.Write(inputbyteArray, 0, inputbyteArray.Length);
                    cs.FlushFinalBlock();
                    Encoding encoding = Encoding.UTF8;
                    ToReturn = encoding.GetString(ms.ToArray());
                }
                return ToReturn;
            }
            catch (Exception ae)
            {
                throw new Exception(ae.Message, ae.InnerException);
            }
        }
    }
}