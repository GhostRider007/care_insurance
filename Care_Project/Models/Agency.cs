﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models
{
    public class Agency
    {
        public string UID { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string TypeID { get; set; }
        public string AgencyName { get; set; }
        public string Name { get; set; }
        public string IsCorp { get; set; }
        public string DistrId { get; set; }
        public string Agent_Type { get; set; }
        public string AgencyId { get; set; }
        public string Crd_Limit { get; set; }
    }
}