﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models.Health
{
    public class HealthComparisonModel
    {
        //Comparison Request for different insurance products
        public class CompareRequest
        {
            public int companyId { get; set; }
            public int productid { get; set; }
            public string plan { get; set; }
            public int sumInsured { get; set; }
            public string cover { get; set; }
            public string premium { get; set; }
        }

        //response class for comparison
        #region[Comparison response class]

        // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
        public class Menu
        {
            public string menuName { get; set; }
            public string details { get; set; }
            public int menuID { get; set; }
            public string destriptions { get; set; }
        }

        public class Category
        {
            public string categoryName { get; set; }
            public int categoryID { get; set; }
            public List<Menu> menus { get; set; }
        }

        public class Response
        {
            public List<Category> categories { get; set; }
            public string logoUrl { get; set; }
            public string productName { get; set; }
            public string insuranceCompany { get; set; }
            public int companyId { get; set; }
            public int productid { get; set; }
            public string plan { get; set; }
            public int sumInsured { get; set; }
        }

        public class BenefitsRoot
        {
            public bool success { get; set; }
            public string message { get; set; }
            public Response response { get; set; }
            public int error_code { get; set; }
        }

        #endregion
    }
}