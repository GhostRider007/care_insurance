﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models.Health
{
    public class HealthModel
    {
        public class PinCode_Master
        {
            public string PINCODE { get; set; }
            public string CITYCODE { get; set; }
            public string CITYDESC { get; set; }
            public string STATECODE { get; set; }
        }
        public class HealthEnquiry
        {
            public string AgencyId { get; set; }
            public string AgencyName { get; set; }
            public string Policy_Type { get; set; }
            public string Policy_Type_Text { get; set; }
            public string Sum_Insured { get; set; }
            public string Enquiry_Id { get; set; }
            public bool Self { get; set; }
            public int SelfAge { get; set; }
            public bool Spouse { get; set; }
            public int SpouseAge { get; set; }
            public bool Son { get; set; }
            public int Son1Age { get; set; }
            public int Son2Age { get; set; }
            public int Son3Age { get; set; }
            public int Son4Age { get; set; }
            public bool Daughter { get; set; }
            public int Daughter1Age { get; set; }
            public int Daughter2Age { get; set; }
            public int Daughter3Age { get; set; }
            public int Daughter4Age { get; set; }
            public bool Father { get; set; }
            public int FatherAge { get; set; }
            public bool Mother { get; set; }
            public int MotherAge { get; set; }
            public string Pin_Code { get; set; }
            public string searchurl { get; set; }
            public string selectedproductjson { get; set; }
        }
        public class SumInsuredMaster
        {
            public int SumInsuredId { get; set; }
            public string MinSumInsured { get; set; }
            public string MaxSumInsured { get; set; }
            public string SumInsuredText { get; set; }
            public string SumInsuredValue { get; set; }
        }

        public class PaymentModel
        {
            public string csrf { get; set; }
            public string policyNumber { get; set; }
            public string transactionRefNum { get; set; }
            public string uwDecision { get; set; }
            public string errorFlag { get; set; }
            public string errorMsg { get; set; }
            public string enquiryid { get; set; }
            public string companyName { get; set; }
            public string Status { get; set; }
            public string imgurl { get; set; }
            public string companyid { get; set; }
            public string premium { get; set; }
            public string proposalNum { get; set; }     
            public string paymentdate { get; set; }
        }
    }

    public class FilterSection
    {
        public int MinPrice { get; set; }
        public int MaxPrice { get; set; }
        public string EnquiryId { get; set; }
        public string Tenure { get; set; }
        public string SumInsured { get; set; }
    }
}