﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models.Health
{
    public class HealthInsured
    {
        public int id { get; set; }
        public string enquiryid { get; set; }
        public string productid { get; set; }
        public string companyid { get; set; }
        public string relationid { get; set; }
        public string relationname { get; set; }
        public string title { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string dob { get; set; }
        public string height { get; set; }
        public string weight { get; set; }
        public string occupationId { get; set; }
        public string occupation { get; set; }
        public bool illness { get; set; }
        public string illnessdesc { get; set; }
        public bool engageManualLabour { get; set; }
        public string engageManualLabourDesc { get; set; }
        public bool engageWinterSports { get; set; }
        public string engageWinterSportDesc { get; set; }
        public bool ispropinsured { get; set; }
        public string medicalhistory { get; set; }
        public int insuredcount { get; set; }
    }
    public class HealthProposal
    {
        public int proposalId { get; set; }
        public string enquiryid { get; set; }
        public string productid { get; set; }
        public string companyid { get; set; }
        public string suminsured_amount { get; set; }
        public string period { get; set; }
        public string selected_premium { get; set; }
        public string p_title { get; set; }
        public string p_gender { get; set; }
        public string p_firstname { get; set; }
        public string p_lastname { get; set; }
        public string p_mobile { get; set; }
        public string p_dob { get; set; }
        public string p_email { get; set; }
        public string p_address1 { get; set; }
        public string p_address2 { get; set; }
        public string p_landmark { get; set; }
        public string p_pincode { get; set; }
        public string p_cityname { get; set; }
        public string p_statename { get; set; }
        public string p_pannumber { get; set; }
        public string p_aadharno { get; set; }
        public string p_annualincome { get; set; }
        public bool p_isproposerinsured { get; set; }
        public string n_title { get; set; }
        public string n_firstname { get; set; }
        public string n_lastname { get; set; }
        public string n_dob { get; set; }
        public string n_age { get; set; }
        public string n_relation { get; set; }
        public string n_relationid { get; set; }
        public string row_premium { get; set; }
        public string row_basePremium { get; set; }
        public string row_serviceTax { get; set; }
        public string row_totalPremium { get; set; }
        public string row_discountPercent { get; set; }
        public string row_insuranceCompany { get; set; }
        public string row_policyType { get; set; }
        public string row_planname { get; set; }
        public string row_policyTypeName { get; set; }
        public string row_policyName { get; set; }
        public string policypdflink { get; set; }
        public string referenceId { get; set; }
        public string proposalNumber { get; set; }
        public string policyNumber { get; set; }
        public string paymenturl { get; set; }
        public string paymentdate { get; set; }
        public string actionType { get; set; }
        public List<HealthInsured> InsuredList { get; set; }
    }
    public class HealthPayment
    {
        public string enquiryid { get; set; }
        public int companyid { get; set; }
        public string companyname { get; set; }
        public string premium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public string proposalNum { get; set; }
        public string productId { get; set; }
        public string paymenturl { get; set; }
        public string referenceId { get; set; }
        public string cc_policyNumber { get; set; }
        public string cc_transactionRefNum { get; set; }
        public string cc_uwDecision { get; set; }
        public string cc_errorMsg { get; set; }
        public string status { get; set; }
        public bool paymentstatus { get; set; }
            public string paymentdate { get; set; }
    }
    public class HealthPerposalQuestion
    {
        public int id { get; set; }
        public string enquiryid { get; set; }
        public string firstname { get; set; }
        public string lastnane { get; set; }
        public string questionjson { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class CreateProposalData
    {
        public object referenceId { get; set; }
        public string premium { get; set; }
        public object serviceTax { get; set; }
        public object totalPremium { get; set; }
        public string proposalNum { get; set; }
        public int productDetailId { get; set; }
        public int companyId { get; set; }
    }
    public class CreateProposalResponse
    {
        public CreateProposalData data { get; set; }
        public string urls { get; set; }
        public string message { get; set; }
    }
    public class HealthPerposalResponse
    {
        public bool success { get; set; }
        public string message { get; set; }
        public CreateProposalResponse response { get; set; }
        public int error_code { get; set; }
        public int status { get; set; }
        public string title { get; set; }
    }
}