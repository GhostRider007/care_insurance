﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models.Health
{
    public class HealthQuestionaries
    {
        public bool success { get; set; }
        public string message { get; set; }
        public List<Questionary_Response> response { get; set; }
        public int error_code { get; set; }
    }
    public class Questionary_Response
    {
        public string questionSetCode { get; set; }
        public string questionCode { get; set; }
        public string questionDescription { get; set; }
        public string response { get; set; }
        public string required { get; set; }
        public List<Questionary_SubQuestion> subQuestion { get; set; }
        public List<Questionary_DropDownData> dropDownData { get; set; }
    }
    public class Questionary_DropDownData
    {
        public string text { get; set; }
        public string value { get; set; }
    }
    public class Questionary_SubQuestion
    {
        public string questionSetCode { get; set; }
        public string questionCode { get; set; }
        public string questionDescription { get; set; }
        public string response { get; set; }
        public string required { get; set; }
        public List<object> subQuestion { get; set; }
        public List<Questionary_DropDownData> dropDownData { get; set; }
    }
}