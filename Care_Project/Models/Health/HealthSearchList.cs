﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Care_Project.Models.Health
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Health_Insured
    {
        public int dob { get; set; }
        public int buyBackPED { get; set; }
    }

    public class Health_Product
    {
        public string policyTypeName { get; set; }
        public string policyName { get; set; }
        public string period { get; set; }
        public List<Health_Insured> insureds { get; set; }
        public int productDetailId { get; set; }
    }
    public class Addon
    {
        public string addOns { get; set; }
        public string value { get; set; }
    }

    public class Health_Datum
    {
        public string inquiry_id { get; set; }
        public int productDetailID { get; set; }
        public double discountPercent { get; set; }
        public string basePremium { get; set; }
        public string premium { get; set; }
        public string serviceTax { get; set; }
        public string totalPremium { get; set; }
        public int gstPercenatge { get; set; }
        public string policytype { get; set; }
        public int schemaId { get; set; }
        public string companyName { get; set; }
        public int companyId { get; set; }
        public int productCode { get; set; }
        public string productName { get; set; }
        public string period { get; set; }
        public int tenure { get; set; }
        public string plan { get; set; }
        public int schemeId { get; set; }
        public Health_Product product { get; set; }
        public List<Addon> addons { get; set; }
        public string ageBracket { get; set; }
        public object scheme { get; set; }
        public int sumInsuredValue { get; set; }
        public int suminsuredId { get; set; }
        public int sumInsuredCode { get; set; }
        public string logoUrl { get; set; }
    }

    public class Health_SchemesMaster
    {
        public int schemeId { get; set; }
        public string schemes { get; set; }
    }

    public class Health_Response
    {
        public List<Health_Datum> data { get; set; }
        public object urls { get; set; }
        public List<Health_SchemesMaster> schemesMaster { get; set; }
    }

    public class HealthSearchList
    {
        public bool success { get; set; }
        public string message { get; set; }
        public Health_Response response { get; set; }
        public int error_code { get; set; }
    }

    public class Auth_Response
    {
        public string tokenvalue { get; set; }
    }

    public class Health_Auth
    {
        public bool success { get; set; }
        public string message { get; set; }
        public Auth_Response response { get; set; }
        public int error_code { get; set; }
    }

    public class AddonChecked
    {
        public string addOnsId { get; set; }
        public string addOns { get; set; }
        public string code { get; set; }
        public string value { get; set; }
        public string calculation { get; set; }
        public string addOnValue { get; set; }
        public string BasePremium { get; set; }
        public string PlanAmount { get; set; }
        public string PlanPeriod { get; set; }
        public string inquiryid { get; set; }
    }
}