﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    GetSelectedPlanDetails(GetEnquiryId());
});

GetEnquiryId = () => {
    let enquiryid = GetParameterValues('enquiryid');
    if (enquiryid != "" && enquiryid != undefined) {
        if (enquiryid.length == 15) {
            return enquiryid;
        }
        else {
            window.location.href = "/health/healthrejistration";
        }
    }
    else {
        window.location.href = "/health/healthrejistration";
    }
}

GetSelectedPlanDetails = (enquiryid) => {
    $("#NextSelectedPlan").css("display", "none");
    $.ajax({
        type: "Post",
        url: "/Health/GetSelectedPlanDetails",
        data: '{enquiryid: ' + JSON.stringify(enquiryid) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null && response != "") {
                $("#divPlanSection").html("").html(response[0]);
                $("#divProductDetails").html("").html(response[1]);
                $("#NextSelectedPlan").css("display", "block");
            }
        }
    });
}

GoToNextInsuredSection = () => {
    let enquiryid = GetEnquiryId();
    if (CheckFocusDropDownBlankValidation("ddlPTitle")) return !1;
    if (CheckFocusBlankValidation("txtPFirstName")) return !1;
    if (CheckFocusBlankValidation("txtPLastName")) return !1;
    if (CheckFocusBlankValidation("txtPMobile")) return !1;
    if (CheckFocusBlankValidation("txtPDOB")) return !1;
    if ($("#txtPDOB").hasClass("errorpage")) { return false; }
    if (CheckFocusBlankValidation("txtPEmail")) return !1;
    if (CheckEmailValidatoin("txtPEmail")) return !1;
    if (CheckFocusBlankValidation("txtPAddress1")) return !1;
    if (CheckFocusBlankValidation("txtPAddress2")) return !1;
    if (CheckFocusBlankValidation("txtPPincode")) return !1;
    if (CheckFocusBlankValidation("txtPCity")) return !1;
    if (CheckFocusBlankValidation("txtPState")) return !1;

    let selectedamount = 0;
    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            selectedamount = $(this).data("premium");
        }
    });
    if (parseFloat(selectedamount) > 49000) { if (CheckFocusBlankValidation("txtPPan")) return !1; }
    if (CheckFocusDropDownBlankValidation("ddlAnualIncome")) return !1;
    let proposal = {};

    proposal.enquiryid = enquiryid;
    proposal.p_title = $("#ddlPTitle option:selected").val();
    proposal.p_gender = proposal.p_title == "Mr." ? "Male" : "FeMale";
    proposal.p_firstname = $("#txtPFirstName").val();
    proposal.p_lastname = $("#txtPLastName").val();
    proposal.p_mobile = $("#txtPMobile").val();
    proposal.p_dob = $("#txtPDOB").val();
    proposal.p_email = $("#txtPEmail").val();
    proposal.p_address1 = $("#txtPAddress1").val();
    proposal.p_address2 = $("#txtPAddress2").val();
    proposal.p_landmark = $("#txtPLandmark").val();
    proposal.p_pincode = $("#txtPPincode").val();
    proposal.p_cityname = $("#txtPCity").val();
    proposal.p_statename = $("#txtPState").val();
    proposal.p_pannumber = $("#txtPPan").val();
    proposal.p_aadharno = $("#txtPAadhar").val();
    proposal.p_annualincome = $("#ddlAnualIncome option:selected").val();
    proposal.p_isproposerinsured = ($("#chkIsProposerInsured").prop("checked") == true ? 1 : 0);

    $("#btnGoToNextInsured").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/InsertProposerDetails",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != null) {
                $("#fdselectedplansection").css("display", "none");
                $("#fdproposerdetails").css("display", "none");
                $("#fdInsuredDetails").css("display", "block");
                $(".linext3").addClass("active");
                BindInsuredDetails(proposal.isproposerinsured);
            }
        }
    });
}

function BindInsuredDetails(isproposerinsured) {
    let proposal = {};
    proposal.enquiryid = GetEnquiryId();
    proposal.isproposerinsured = isproposerinsured;

    $.ajax({
        type: "Post",
        url: "/Health/BindInsuredDetails",
        async: false,
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            $("#fdInsuredDetails").html(response[0]);
            let totalperson = response[1];

            $('.DynamicCommanDateWithYr').datepicker({
                dateFormat: "dd/mm/yy",
                showStatus: true,
                showWeeks: true,
                currentText: 'Now',
                changeMonth: true,
                changeYear: true,
                autoSize: true,
                maxDate: -0,
                gotoCurrent: true,
                showAnim: 'blind',
                highlightWeek: true,
                //yearRange: '' + ((new Date).getFullYear() - 50) + ':' + (new Date).getFullYear()
                yearRange: '1950:' + (new Date).getFullYear()
            });

            $("#btnGoToNextInsured").html("NEXT &gt;");

            CalculateNomineeAge($("#nDOB"), 'ndobvalid');
        }
    });
}

FillExistingIllness = (illnesspos) => {
    let existingillnes = $("#ddlIllness_" + illnesspos + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".ExistingIllness_" + illnesspos).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".ExistingIllness_" + illnesspos).addClass("notshow").css("display", "none");
        $("#txtExistingIllness_" + illnesspos).val("");
    }
}

FillEngageManualLabour = (mlabour) => {
    let existingillnes = $("#ddlEngageManualLabour_" + mlabour + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageManualLabour_" + mlabour).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageManualLabour_" + mlabour).addClass("notshow").css("display", "none");
        $("#txtEngageManualLabour_" + mlabour).val("");
    }
}

FillEngageWinterSports = (wsport) => {
    let existingillnes = $("#ddlEngageWinterSports_" + wsport + " option:selected").val();

    if (existingillnes != "" && existingillnes == "true") {
        $(".EngageWinterSports_" + wsport).removeClass("notshow").removeAttr("style");
    }
    else {
        $(".EngageWinterSports_" + wsport).addClass("notshow").css("display", "none");
        $("#txtEngageWinterSports_" + wsport).val("");
    }
}

CalculateNomineeAge = (fid, errorclass, errorstyle) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $("." + errorclass).html("<span style='" + errorstyle + "'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            //$("#btnNextProposerDetails").removeAttr('disabled');
            //$("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $("." + errorclass).html("<span class='text-danger pull-right' style='" + errorstyle + "'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            //$("#btnNextProposerDetails").attr('disabled');
            //$("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

function NextSelectedPlan() {
    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiryid');
    $(".chkpremiumamt").each(function (index, element) {
        if ($(this).prop("checked") == true) {
            proposal.period = $(this).data("periodfor");
            proposal.selected_premium = $(this).data("premium");
            //proposal.discount = $(this).data("discount");
        }
    });

    $("#btnGoToNextProposal").html("Processing... <i class='fa fa-spinner fa-pulse'></i>");

    $.ajax({
        type: "Post",
        url: "/Health/InsertSelectedPlanDetail",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            setTimeout(function () {
                GetAnnualIncomeDetails(response);
            }, 200);
        }
    });
}

GetAnnualIncomeDetails = (response) => {
    if (response.enquiryid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetAnnualIncomeDetails",
            data: '{enquiryid:' + JSON.stringify(response.enquiryid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $("#ddlAnualIncome").html(data);
                }
                $("#ddlPTitle").val(response.p_title);
                $("#txtPFirstName").val(response.p_firstname);
                $("#txtPLastName").val(response.p_lastname);
                $("#txtPMobile").val(response.p_mobile);
                $("#txtPEmail").val(response.p_email);
                $("#txtPPincode").val(response.p_pincode);
                $("#txtPDOB").val(response.p_dob);
                $("#txtPAddress1").val(response.p_address1);
                $("#txtPAddress2").val(response.p_address2);
                $("#txtPLandmark").val(response.p_landmark);
                //GetCityListFromStarHealth(response.pincode, response.cityid, response.areaid);
                CheckAndGetDetails(response.p_pincode);
                $("#txtPState").val(response.p_statename);
                $("#txtPCity").val(response.p_cityname);

                $("#txtPPan").val(response.p_pannumber);
                $("#txtPAadhar").val(response.p_aadharno);
                //$("#ddlAnualIncome option[value=" + response.annualincome + "]").attr("selected", "selected");  //response.annualincome
                $("#ddlAnualIncome").val(response.p_annualincome);
                if (response.p_isproposerinsured == true) { $("#chkIsProposerInsured").prop("checked", true); } else { $("#chkIsProposerInsured").prop("checked", false); }
                //if (response.criticalillness) { $("#chkHavingCriticalIllness").prop("checked", true); } else { $("#chkHavingCriticalIllness").prop("checked", false); }
                //if (response.ispropnominee) { $("#chkIsPropNominee").prop("checked", true); } else { $("#chkIsPropNominee").prop("checked", false); }

                $(".section1").addClass("hidden");
                $(".section2").removeClass("hidden");
                $(".linext2").addClass("active");
                $("#btnGoToNextProposal").html("Next");

                CalculateAge('txtPDOB');
            }
        });
    }
}

CheckAndGetDetails = (pincode) => {
    $(".notpincodevalid").addClass("hidden").html("");

    if (pincode.length != 6) {
        $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("provide valid pincode");
        $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
    }
    else {
        $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("verifying <i class='fa fa-spinner fa-pulse'></i>");
        $.ajax({
            type: "Post",
            url: "/Health/CheckPincode",
            data: '{pincode: ' + JSON.stringify(pincode) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response != null && response.length > 0) {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#00a300").html("<i class='fa fa-check-circle'></i> " + response);
                    $("#hdnState").val(response[0]);
                    $("#txtPPincode").addClass("successpincode").removeClass("errorpincode");
                }
                else {
                    $(".notpincodevalid").removeClass("hidden").css("color", "#ff00008c").html("Wrong Pincode");
                    $("#txtPPincode").removeClass("successpincode").addClass("errorpincode");
                }
            }
        });
    }
}

CalculateAge = (fid) => {
    let startdate = $("#" + fid).val();
    if (startdate != "") {
        let splitstartdate = startdate.split('/');
        var start = new Date(splitstartdate[2] + "-" + splitstartdate[1] + "-" + splitstartdate[0]);
        let age = Math.floor((new Date() - start) / 31557600000);

        if (age >= 18) {
            $("#" + fid).removeClass("errorpage");
            $(".notpdobvalid").html("<span style='position: absolute;top: 14px;right: 20px;font-size: 10px;'><i class='fa fa-check-circle'></i> " + age + " Years</span>").css("color", "rgb(0, 163, 0)");

            $("#btnNextProposerDetails").removeAttr('disabled');
            $("#btnNextProposerDetails").css('opacity', 1);
        }
        else {
            $("#" + fid).addClass("errorpage");
            $(".notpdobvalid").html("<span class='text-danger pull-right' style='position: absolute;top: 14px;right: 20px;font-size: 10px;'><i class='fa fa-exclamation-triangle'></i> Age must be 18+ Y&nbsp;</span>");

            $("#btnNextProposerDetails").attr('disabled');
            $("#btnNextProposerDetails").css('opacity', 0.3);
        }
    }
}

function NextInsuredDetails() {

    for (var i = 1; i <= parseInt(totalperson); i++) {
        if (CheckFocusBlankValidation("ddlRelation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlTitle_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInFirstName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInLastName_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInDOB_" + i)) return !1;
        if ($("#txtInDOB_" + i).hasClass("errorpage")) { return false; }
        if (CheckFocusBlankValidation("txtInHeight_" + i)) return !1;
        if (CheckFocusBlankValidation("txtInWeight_" + i)) return !1;

        if (CheckFocusBlankValidation("ddlOccupation_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlIllness_" + i)) return !1;

        let existingillnes = $("#ddlIllness_" + i + " option:selected").val();
        if (existingillnes == "true") { if (CheckFocusBlankValidation("txtExistingIllness_" + i)) return !1; }

        let ismlabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        if (ismlabour == "true") { if (CheckFocusBlankValidation("txtEngageManualLabour_" + i)) return !1; }

        let iswsport = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        if (iswsport == "true") { if (CheckFocusBlankValidation("txtEngageWinterSports_" + i)) return !1; }

        if (CheckFocusBlankValidation("ddlEngageManualLabour_" + i)) return !1;
        if (CheckFocusBlankValidation("ddlEngageWinterSports_" + i)) return !1;
    }

    if (CheckFocusDropDownBlankValidation("ddlnRelation")) return !1;
    if (CheckFocusDropDownBlankValidation("ddlnTitle")) return !1;
    if (CheckFocusBlankValidation("nFirstName")) return !1;
    if (CheckFocusBlankValidation("nLastName")) return !1;
    if (CheckFocusBlankValidation("nDOB")) return !1;
    if ($("#nDOB").hasClass("errorpage")) { return false; }

    let proposal = {};
    proposal.enquiryid = GetParameterValues('enquiry_id');

    var insuredlist = [];

    for (var i = 1; i <= parseInt(totalperson); i++) {
        let insured = {};

        insured.id = $("#hdnInsuredId_" + i).val();
        insured.relationid = $("#ddlRelation_" + i + " option:selected").val();
        insured.relationname = $("#ddlRelation_" + i + " option:selected").text();
        insured.title = $("#ddlTitle_" + i + " option:selected").val();
        insured.firstname = $("#txtInFirstName_" + i).val();
        insured.lastname = $("#txtInLastName_" + i).val();
        insured.dob = $("#txtInDOB_" + i).val();
        insured.height = $("#txtInHeight_" + i).val();
        insured.weight = $("#txtInWeight_" + i).val();

        insured.occupationid = $("#ddlOccupation_" + i + " option:selected").val();
        insured.occupation = $("#ddlOccupation_" + i + " option:selected").text();
        insured.isillness = $("#ddlIllness_" + i + " option:selected").val();
        insured.illnessdesc = $("#txtExistingIllness_" + i).val();

        insured.isengagemanuallabour = $("#ddlEngageManualLabour_" + i + " option:selected").val();
        insured.engageManualLabourDesc = $("#txtEngageManualLabour_" + i).val();

        insured.isengagewintersports = $("#ddlEngageWinterSports_" + i + " option:selected").val();
        insured.engageWinterSportDesc = $("#txtEngageWinterSports_" + i).val();

        insured.isinsured = (i == 1 ? true : false);

        insuredlist.push(insured);
    }

    proposal.InsuredDetails = insuredlist;
    proposal.nRelation = $("#ddlnRelation option:selected").val();
    proposal.nTitle = $("#ddlnTitle option:selected").val();
    proposal.nFirstName = $("#nFirstName").val();
    proposal.nLastName = $("#nLastName").val();
    proposal.nDOB = $("#nDOB").val();

    let nBdate = proposal.nDOB.split('/');
    nBdate = new Date(nBdate[2] + "-" + nBdate[1] + "-" + nBdate[0])

    proposal.nAge = Math.floor((new Date() - nBdate) / 31557600000);


    $("#btnInsuredNext").html("processing... <i class='fa fa-spinner fa-pulse'></i>");
    $.ajax({
        type: "Post",
        url: "/Health/GetNSaveInsuredDetail",
        data: '{proposal:' + JSON.stringify(proposal) + '}',
        contentType: "application/json; charset=utf-8",
        datatype: "json",
        success: function (response) {
            if (response != "") {
                $("#divMedicalHistoryDetail").html(response);
            }
            else {
                $("#divMedicalHistoryDetail").html("No medical history found!");
            }
            $("#fdselectedplansection").css("display", "none");
            $("#fdproposerdetails").css("display", "none");
            $("#fdInsuredDetails").css("display", "none");
            $("#fdMedicalHistory").css("display", "block");
            $(".linext4").addClass("active");
            $("#btnInsuredNext").html("NEXT");
        }
    });
}



























//function checkSection1RegForm() {
//    $(".section1").addClass("hidden");
//    $(".section2").removeClass("hidden");
//    $(".linext2").addClass("active");
//}
function Backform() {
    $(".section2").addClass("hidden");
    $(".linext2").removeClass("active");
    $(".section1").removeClass("hidden");
}