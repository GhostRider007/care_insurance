﻿function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () { FetchHealthListing(GetParameterValues('enquiryid')); });

FetchHealthListing = (enqid) => {
    if (enqid != "") {
        $.ajax({
            type: "Post",
            url: "/Health/GetHealthSearchList",
            data: '{enquiryid: ' + JSON.stringify(enqid) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                $("#divHealth_SearchList").html("").html(response);
            }
        });
    }
}

$(document.body).on('click', ".btnselectedplan", function (e) {
    let productid = $(this).data("productid");
    let suminsured = $(this).data("suminsured");
    let suminsuredid = $(this).data("suminsuredid");
    let companyid = $(this).data("companyid");
    let adult = $(this).data("adult");
    let child = $(this).data("child");
    let planname = $(this).data("planname");
    let totalamt = $(this).data("totalamt");
    let thiscount = $(this).data("thiscount");
    let tenure = $(this).data("tenure");
    let inramount = $(this).data("inramount");
    if (thiscount != "") {
        $(".btnselectedplan").each(function () { $(this).attr("disabled", true); });
        $("#btnPremiumPlan" + thiscount).html("Wait... <i class='fa fa-spinner fa-pulse'></i>").css({ "background": "#262566" });
        let urlpara = "enquiry_id=" + GetParameterValues('enquiryid') + "&product=" + productid + "&companyid=" + companyid + "&suminsuredid=" + suminsuredid + "&suminsured=" + suminsured + "&adult=" + adult + "&child=" + child + "&planname=" + planname + "&totalamt=" + totalamt + "&tenure=" + tenure;
        $.ajax({
            type: "Post",
            url: "/Health/InsertSearchUrlParaDetail",
            data: '{enquiryid: ' + JSON.stringify(GetParameterValues('enquiryid')) + ',urlpara:' + JSON.stringify(urlpara) + '}',
            contentType: "application/json; charset=utf-8",
            datatype: "json",
            success: function (response) {
                if (response == true) {
                    window.location.href = "/health/healthproposal?enquiryid=" + GetParameterValues('enquiryid');
                }
                else {
                    $("#btnPremiumPlan" + thiscount).html(inramount).css({ "background": "#09a9e5" });
                    $(".btnselectedplan").each(function () { $(this).attr("disabled", false); });
                }
            }
        });
    }
});